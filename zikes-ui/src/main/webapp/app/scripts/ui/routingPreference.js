/**
 * scripts/ui/routingPreference.js
 *
 * This prepares and manages the Routing Preference UI panel
 */
'use strict';

var when     = require("when");
var mustache = require("mustache");
var map      = require("./map.js");
var zikesApi = require("../lib/zikesRouter.js");
               require("../external/bootstrap-slider.js");

var routingPreference = (function() {

    var sliderTemplate = when.defer();
    $.get("templates/tools.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#routingPreferenceSlider_tmpl").html()
        sliderTemplate.resolve(html);
    });
    var toolAccordionElemTemplate = when.defer();
    $.get("templates/tools.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#routingPreferenceAccordionElem_tmpl").html()
        toolAccordionElemTemplate.resolve(html);
    });
    var trafficRestrictionsTemplate = when.defer();
    $.get("templates/tools.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#routingPreferenceTrafficRestrictions_tmpl").html()
        trafficRestrictionsTemplate.resolve(html);
    });

    function showHelp(hoverDiv, helpContents) {
        var helpIcon     = hoverDiv.find(".helpIcon");
        var mouseIn      = false;
        var popoverShown = false;
        var iconShown    = false;
        var hideTimer    = undefined;

        hoverDiv.mouseenter(function() {
            if (hideTimer) {
                clearTimeout(hideTimer);
            }
            mouseIn = true;
            setTimeout(function() {
                if (!iconShown && mouseIn) {
                    helpIcon.fadeIn(400);
                    iconShown = true;
                }
                hideTimer = setTimeout(function() {
                    if (!popoverShown) {
                        helpIcon.fadeOut(400);
                        iconShown = false;
                    }
                }, 3000);
            }, 400);
        });
        hoverDiv.mouseleave(function() {
            mouseIn = false;
        });
        var popover = helpIcon.popover({
            trigger: 'manual',
            html:    true,
            title:   helpContents.title + '<button type="button" id="popoverClose" class="close" style="margin:-3px;">&times;</button>',
            content: helpContents.content
        });
        helpIcon.click(function() {
            popover.popover("show");
        });

        popover.on("show.bs.popover", function(e){
            popover.data("bs.popover").tip().css({"min-width": helpContents.width || "300px"});
            $(".helpIcon").each(function() {
                if ($(this)[0] != popover[0]) {
                    $(this).popover("hide");
                }
            });
        })
        .on('shown.bs.popover', function() {
            popoverShown = true;
            hoverDiv.find("#popoverClose").click(function() {
                popover.popover("hide");
            });
        })
        .on('hidden.bs.popover', function() {
            helpIcon.fadeOut(400);
            popoverShown = false;
        });
        return popover;
    }

    function RoutingPreference(updateCb, jsonDocument) {
        this.mUpdatedCb      = updateCb;
        this.mJSON           = jsonDocument;
        this.mCurrentProfile = undefined;
        this.mSliders        = []
        this.mSliders.push(new Slider(
            this,
            require("../lib/highways.js").sliderGenerator,
            {
                help : {
                    content : '<span class="small">Prefer or avoid specific types of ways/roads.<br>'
                             +'Note that the coloured spectrum underneath the route profile corresponds to the road type colours:<br>'
                             +'<p style="text-align:center;"><img src="https://dl.dropboxusercontent.com/u/11723457/Zikes/spectrum.png" height="180"></p>'
                             +'The road types naming by the <a class="zikesAnchor" href="http://wiki.openstreetmap.org/wiki/Key:highway#Values" target="_blank">Open Street Maps</a></span>',
                    width: 320
                }
            }
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/climb.js").sliderGenerator,
            {
                help : {
                    title : '<span><b>prefer or avoid climbs.</b></span>',
                    content : '<span class="small">This slider pays attention to the elevation gain <u>rate</u> (inclination) and not elevation gain alone. </span>'
                             +'<p style="text-align:center;"><img src="https://dl.dropboxusercontent.com/u/11723457/Zikes/zikesClimb.gif" align="middle" height="140"></p>'
                             +'<span class="small">Descends are rewarded/punished similarly (albeit more conservativelly) as ascends. This means one cannot reward descends alone - this would result in an undesirable economy of them crediting ascends.</span>',
                    width: 400
                }
            }

        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/turns.js").sliderGenerator,
            {classs: "routingLeftPreference"}
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/cycleways.js").sliderGenerator,
            {
                classs: "routingRightPreference",
                help : {
                    title   : '<span><b>turns & cycleways</b></span>',
                    content : '<span><b>fewer turns</b><br></span>'
                             +'<span class="small">Suppose you set your preference to avoid the smoke and traffic of big city arteries and suppose there is one that cannot be avoided. '
                             +'Would you like to be taken away from it for only 50m of peace? I wouldn\'t, but perhaps you would.<br>'
                             +'<p style="text-align:center;"><img src="https://dl.dropboxusercontent.com/u/11723457/Zikes/fewerTurns.gif" align="middle" height="200"></p></span>'
                             +'<span><b>more cyleways</b><br></span>'
                             +'<span class="small">Increase your preference for any kind of track dedicated to bicycles - lanes (shared or otherwise), cycleways, parts of regional or (inter-) national bicycle networks, etc. This is different and less specific to the <i>cycleway</i> in the <i>roads</i> preferences, '
                             +'where it denotes a kind of way <a class="zikesAnchor" href="http://wiki.openstreetmap.org/wiki/Tag:highway%3Dcycleway" target="_blank">exclusivelly dedicated to bicycles</a>.</span>',
                    width: "500px"
                }
            }
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/urbanRural.js").sliderGenerator,
            {
                help : {
                    title : '<span><b>urban or rural?</b></span>',
                    content : '<span class="small">This slider pays attention to the density of the road network. Denser networks, it concludes, indicate urban character.</span>'
                }
            }
        ));
        this.mSliders.push(new Slider(
            this,
            require("../lib/popularity.js").sliderGenerator,
            {
                help : {
                    content : '<span class="small">Would you like to be taken through the roads and tracks other people (local people) took before you? Chances are, they knew what they were doing.</span>'
                }
            }
        ));
        this.render($("#toolsAccordion"));
    }

    var profilesTypeaheadMatcher = function(profileNames) {
        var KMaxListLength = 10;
        return function findMatches(q, cb) {

            if (q == "" || profileNames.length <= KMaxListLength) {
                cb(profileNames.slice(
                    0, Math.min(KMaxListLength, profileNames.length)
                ));
                return;
            }

            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    function resetTypeahead(profiles, currentProfile) {
        var input = $("#presetProfilesInput");
        input.typeahead("destroy");
        input.typeahead({
            minLength: 0,
            highlight: true},
            {
                source: profilesTypeaheadMatcher(Object.keys(profiles))
            }
        ).typeahead('val', currentProfile);
    }

    RoutingPreference.prototype.render = function(panel) {
        var self     = this;
        var slidersRendered = [];
        for (var i = 0; i < self.mSliders.length; i++) {
            slidersRendered.push(
                self.mSliders[i].render(panel)
            );
        }

        return when.all(slidersRendered.concat([zikesApi.meta()]))
        .then(function() {
            var profiles = {};
            var preferedProfileName = undefined;
            function loadPreferedProfile(profileName) {
                if (!self.mHasBeenFiddledWith && profileName in profiles) {
                    self.loadProfile(profiles[profileName]);
                    resetTypeahead(profiles, profileName);
                }
            }
            zikesApi.getPreferences(function(preferences) {
                //it is guaranteed this will be called before cb to getProfiles
                preferedProfileName = preferences.currentProfile;
            });
            zikesApi.getProfiles(function(newProfiles) {
                profiles = newProfiles;
                loadPreferedProfile(preferedProfileName || Object.keys(profiles)[0]);
            });

            function loadProfile(profileName) {
                self.loadProfile(profiles[profileName]);
                input.trigger("blur");
            }

            var input = $("#presetProfilesInput");
            input.on("typeahead:selected", function (e) {
                loadProfile(input.val());
            }).on("typeahead:autocompleted", function (e) {
                loadProfile(input.val());
            }).on("keypress", function (e) {
                if(event.which === 13) {
                    var profileName = input.val();
                    if (profileName in profiles) {
                        loadProfile(profileName);
                    } else {
                        var newProfile = self.toJSON(profileName);
                        zikesApi.saveProfile(newProfile)
                        .then(function(metadata) {
                            newProfile.metadata = metadata;
                            profiles[profileName] = newProfile;
                            self.loadProfile(newProfile);
                            resetTypeahead(profiles, newProfile.metadata.opaque.name);
                        });
                    }
                }
            }).on("blur", function() {
                if (profiles && self.mCurrentProfile) {
                    resetTypeahead(profiles, self.mCurrentProfile.metadata.opaque.name);
                }
            });
            $("#deleteProfile").click(function() {
                zikesApi.deleteProfile(self.mCurrentProfile.metadata.id);
                delete profiles[self.mCurrentProfile.metadata.opaque.name];
                self.loadProfile(profiles[Object.keys(profiles)[0]]);
                resetTypeahead(profiles, self.mCurrentProfile.metadata.opaque.name);
            });
            showHelp($("#presetProfiles"), {
                title : '<span><b>preset profiles</b></span>',
                content : '<span class="small">Type a new name here and the current preferences (slider positions) will be saved under that name. '
                         +'Any further manipulations will be saved as you make them.</span>'
            });
        });
    }

    RoutingPreference.prototype.generatePreference = function() {
        var preference = {
            means: "bicycle"
        };
        this.mSliders.forEach(function(slider) {
            slider.generatePreference(preference);
        });
        return preference;
    }

    RoutingPreference.prototype.invalidate = function() {
        this.mHasBeenFiddledWith = true;

        if (!$("#deleteProfile").hasClass("hidden")) {
            zikesApi.saveProfile(
                this.toJSON(this.mCurrentProfile)
            );
        }

        this.mUpdatedCb();
    }

    RoutingPreference.prototype.loadProfile = function(profile) {
        if (!profile || profile.metadata.opaque.name == this.mCurrentProfile) {
            return;
        }
        this.fromJSON(profile.preferences.ui);

        if (profile.metadata.owner) {
            $("#deleteProfileDisabled").addClass("hidden");
            $("#deleteProfile").removeClass("hidden");
        } else {
            $("#deleteProfile").addClass("hidden");
            $("#deleteProfileDisabled").removeClass("hidden");
        }
        $("#presetProfilesInput").val(profile.metadata.opaque.name);
        if (zikesApi.isLoggedIn() &&
            zikesApi.getPreferences().currentProfile != profile.metadata.opaque.name) {
            zikesApi.setPreferences({currentProfile: profile.metadata.opaque.name});
        }
        this.mCurrentProfile = profile;
    }

    RoutingPreference.prototype.fromJSON = function(json) {
        for (var i = 0; i < this.mSliders.length; i++) {
            var slider = this.mSliders[i];
            var sliderJSON = (json && json.sliders) ? json.sliders[slider.label()] : undefined;
            slider.fromJSON(sliderJSON);
        }
    }

    RoutingPreference.prototype.toJSON = function(profile) {
        var preferences = {
            generated: this.generatePreference(),
            ui: {
                sliders : {}
            }
        };
        for (var i = 0; i < this.mSliders.length; i++) {
            var slider = this.mSliders[i];
            preferences.ui.sliders[slider.label()] = slider.toJSON();
        }
        if (typeof profile !== 'object') {
            profile = {
                metadata : {
                    opaque : {
                        name: profile,
                        type: "routing"
                    },
                    access: {
                        ro: "",
                        rw: ""
                    }
                }
            };
        }
        profile.preferences = preferences;
        return profile;
    }





    /*
     *
     * One of many routing preference sliders
     *
     */
    function Slider(parent, generator, options) {
        this.mOptions   = options || {};
        this.mParent    = parent;
        this.mClass     = "routingPreference";
        this.mGenerator = new generator(this);
        this.mSliderOptions = this.mGenerator.masterSlideOptions();
        this.mLabel = this.mSliderOptions.label;
        if (this.mOptions) {
            if (this.mOptions.classs) {
                this.mClass = this.mOptions.classs;
            }
            if (this.mOptions.help) {
                this.mOptions.help.title = this.mOptions.help.title || '<span><b>'+this.mLabel+'</b></span>';
            }
        }
    }

    Slider.prototype._showHelp = function() {
        var self = this;
        var popover = showHelp(this.mRootDiv, this.mOptions.help);
        popover.on("show.bs.popover", function(e){
            self.expand();
        })
    }


    Slider.prototype.render = function(panel) {
        var self     = this;
        return when.all([toolAccordionElemTemplate.promise, sliderTemplate.promise])
        .then(function(templates) {
            panel.append(
                mustache.render(templates[0], {
                    id      : self.mLabel,
                    parent  : "toolsAccordion",
                    class   : self.mClass,
                    help    : self.mOptions.help
                })
            );
            self.mRootDiv = $("#"+self.mLabel);
            var title = self.mRootDiv.find("#title_"+self.mLabel);
            title.append(
                mustache.render(templates[1], self.mSliderOptions)
            );
            var body = panel.find("#collapse_"+self.mLabel+" .panel-body");
            self.mAccordion = panel.find("#collapse_"+self.mLabel);
            var rendered = self.mGenerator.render(body);
            self.mSlider = $("#"+self.mLabel+"_slider").slider();
            self.mSliderDiv = self.mSlider.parent().parent().find(".slider");
            self.mSliderDiv.css("margin-top", "-13px");
            self.mSlider.on('slideStop', function(ev) {
                self.expand();
                self.mGenerator.fromMasterSlider(self.mSlider.getValue());
                self.mParent.invalidate();
            });
            if (self.mOptions.help) {
                self._showHelp();
            }

            if (self.mSliderOptions.underConstruction) {
                self.mSliderDiv.find(".slider-selection").css("background", "url(graphics/uc.png)");
                self.mSliderDiv.find(".slider-handle").addClass("hide");
                var sliderTrack = self.mSliderDiv.find(".slider-track");
                sliderTrack.mousedown(function(e) {
                    e.stopPropagation();
                });
                sliderTrack.mouseup(function(e) {
                    e.stopPropagation();
                });
                sliderTrack.attr("data-toggle", "tooltip");
                sliderTrack.attr("data-placement", "bottom");
                sliderTrack.attr("title", "under construction");
            }
            return rendered;
        });
    }

    Slider.prototype.collapse = function() {
        this.mAccordion.collapse('hide');
    }

    Slider.prototype.expand = function() {
        this.mAccordion.collapse('show');
    }

    Slider.prototype.generatePreference = function(rootJson) {
        this.mGenerator.generate(rootJson);
    }

    Slider.prototype.invalidate = function() {
        this.mParent.invalidate();
    }

    Slider.prototype.label = function() {
        return this.mLabel;
    }

    Slider.prototype.fromJSON = function(json) {
        var value = json ? json.masterSlider : this.mSliderOptions.value;
        this.mSlider.setValue(value);
        if (typeof this.mGenerator.fromJSON != "undefined") {
            this.mGenerator.fromJSON(json ? json.generator : undefined);
        }
    }

    Slider.prototype.toJSON = function() {
        var result = {
            masterSlider: this.mSlider.getValue()
        };
        if (typeof this.mGenerator.toJSON != "undefined") {
            result["generator"] = this.mGenerator.toJSON();
        }
        return result;
    }

    return {
        RoutingPreference: RoutingPreference
    }

})();

module.exports = routingPreference;