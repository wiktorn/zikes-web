/**
 * scripts/ui/poi.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

var when          = require("when");
when["sequence"]  = require('when/sequence');
var googleAPI     = require("../lib/google.js");
var map           = require("./map.js");
var messages      = require("./messages.js");

var journeyItem   = require("./journeyItem.js");
var JourneyItem   = journeyItem.JourneyItem;
var journeyTemplates = journeyItem.templates;

var poi = (function() {

    function POI(parent, itemsDiv, latlng) {
        JourneyItem.call(this, parent, itemsDiv);
        var self = this;
        this.latlng(latlng);
        map.leaflet.on("zoomend", function() {
            resetPopup(self);
        });
    }
    POI.prototype = new JourneyItem();
    POI.prototype.constructor = POI;


    POI.prototype.name = function() {
        var result = JourneyItem.prototype.name.call(this);
        if (!result) {
            if (this.mParams.geocode) {
                result = googleAPI.geocodeHumanReadable(
                    this.mParams.geocode, 3
                );
            } else {
                result = "New Point of Interest";
            }
        }
        return result;
    }

    POI.prototype.doInvalidate = function() {
        if (!this.mParams.latlng) {
            this.delete();
            return;
        }
        JourneyItem.prototype.doInvalidate.call(this);
    }

    POI.prototype.toJSON = function() {
        return this.mParams;
    }

    POI.prototype.fromJSON = function(json) {
        this.mParams = json;
        this.show = function() { //prevent expanding accordion when deserialising
            delete self["show"];
            return when(true);
        }
        return this.latlng(this.mParams.latlng);
    }

    var KBkgColor = "#fff5cc";
    function popupContent(self) {
        var description = "";
        if (self.mParams.description) {
            description = '<p class="small text-muted" style="margin: 0;color:black">'+self.mParams.description+'</p>'
        }
        return '<div style="margin:-5px 0"><p style="margin: 0; color:red"><b>'+self.name()+'</b></p>'+description+'</div>';
    }

    var KMaxZoomForPopup = 11;
    function resetPopup(self) {
        if (self.mLmMarker) {
            self.mLmMarker.getPopup().setContent(
                popupContent(self)
            );
            if ((self.mParams.givenName ||
                 self.mParams.description) &&
                map.leaflet.getZoom() > KMaxZoomForPopup &&
                !self.mParams.hidePopup) {
                self.mLmMarker.openPopup();
            } else {
                self.mLmMarker.closePopup();
            }

        }
    }

    POI.prototype.latlng = function(latlng) {
        var self = this;
        function geocode() {
            return when(self.mParams.geocode || googleAPI.reverseGeocodeFind(self.mParams.latlng))
            .then(function(geocode) {
                self.mParams.geocode = geocode;
                return self.invalidate();
            });
        }

        var self = this;
        if (latlng) {
            if (this.mLmMarker) {
                map.leaflet.removeLayer(this.mLmMarker);
            }
            if (latlng instanceof L.LatLng) {
                latlng = [latlng.lat, latlng.lng]
            }
            this.mParams.latlng = latlng;

            return when(geocode()).then(function() {
                self.mLmMarker = L.marker(self.mParams.latlng, {
                    draggable : true,
                    icon : L.icon({
                        iconUrl: 'http://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.0/images/marker-icon.png',
                        iconSize: [20,36],
                        iconAnchor: [10, 36],
                        popupAnchor: [0, -36]
                    })
                }).addTo(map.leaflet);
                self.mLmMarker.bindPopup(
                    L.popup({
                        autoClose : false,
                        closeOnClick : false,
                        autoPan : false
                    })
                );
                self.mLmMarker.on("popupopen", function() {
                    delete self.mParams["hidePopup"];
                    var closeButton = $(self.mLmMarker.getPopup()._closeButton)
                    closeButton.click(function() {
                        self.mParams.hidePopup = true;
                    });
                    var popupDiv = $(self.mLmMarker.getPopup()._wrapper);
                    popupDiv.parent().css("opacity", 0.70);
                    popupDiv.css("background-color", KBkgColor);
                    $(self.mLmMarker.getPopup()._tip).css("background-color", KBkgColor);
                });
                resetPopup(self);

                self.mDivs.name.on('blur', function() {
                    resetPopup(self);
                });
                self.mDivs.description.on('blur', function() {
                    resetPopup(self);
                });
                self.mLmMarker.on("dragend", function() {
                    self.mParams.geocode = undefined;
                    self.latlng(self.mLmMarker.getLatLng());
                });
            })

        }
        return this.mParams.latlng;
    }

    POI.prototype.getBounds = function() {
        return  L.latLngBounds(
            L.latLng(this.mParams.latlng[0], this.mParams.latlng[1]),
            L.latLng(this.mParams.latlng[0], this.mParams.latlng[1])
        );
    }

    POI.prototype.toGpx = function() {
        var result = '<rte name="'+this.name()+'">\n';
        result += '  '+this.mRouteOnMap.gpxRtePts() + '\n</rte>';
        return result;
    }

    POI.prototype.render = function() {
        return JourneyItem.prototype.render.call(
            this,
            "POI"
        )
    }

    POI.prototype.delete = function() {
        JourneyItem.prototype.delete.call(this);
        if (this.mLmMarker) {
            map.leaflet.removeLayer(this.mLmMarker);
        }
    }

    return { POI : POI };

})();

module.exports = poi;