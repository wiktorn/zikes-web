package com.zikes.api.client;

import org.springframework.http.HttpStatus;

public class ZikesClientException extends Exception {

    private final HttpStatus status;

    public ZikesClientException(String message) {
        this(HttpStatus.INTERNAL_SERVER_ERROR, message);
    }

    public ZikesClientException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
