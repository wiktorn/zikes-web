package com.zikes.api.storage.gcs;

import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.zikes.api.config.GcsSettings;
import com.zikes.api.storage.ZikesIdManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;

public class UploadedTrackService {

    static final String TrackFileName    = "%s.gpx";
    static final String GiftTracksFolder = "anonymous/tracks/";
    static final String UserTracksFolder = "users/%s/uploaded/tracks/";
    static final int BUFFER_SIZE = 2048;

    @Autowired
    protected GcsSettings settings;

    @Autowired
    protected GcsService gcsService;

    public void put(InputStream uploadedTrack, String zikesUser) throws IOException {
        String fileName = String.format(TrackFileName, ZikesIdManager.generateId());
        GcsFilename giftFromUserFilename = new GcsFilename(settings.getBucket(), GiftTracksFolder + fileName);
        GcsOutputChannel giftOutputChannel =
                gcsService.createOrReplace(giftFromUserFilename, new GcsFileOptions.Builder()
                    .mimeType("text/xml")
                    .acl("bucket-owner-full-control")
                    .build()
                );

        if (zikesUser != null) {
            GcsFilename userTrackFilename = new GcsFilename(settings.getBucket(), String.format(UserTracksFolder, zikesUser) +fileName);
            GcsOutputChannel userOutputChannel =
                    gcsService.createOrReplace(userTrackFilename, new GcsFileOptions.Builder()
                        .mimeType("text/xml")
                        .acl("bucket-owner-full-control")
                        .build()
                    );
            try(OutputStream giftOutput = Channels.newOutputStream(giftOutputChannel);
                OutputStream userOutput = Channels.newOutputStream(userOutputChannel)) {
                copy(
                    uploadedTrack,
                    giftOutput,
                    userOutput
                );
            } finally {
                giftOutputChannel.close();
                userOutputChannel.close();
            }
        } else {
            try(OutputStream giftOutput = Channels.newOutputStream(giftOutputChannel)) {
                copy(
                        uploadedTrack,
                        giftOutput,
                        null
                );
            } finally {
                giftOutputChannel.close();
            }
        }
    }

    /**
     * Transfer the data from the inputStream to the outputStreams. Then close all streams.
     */
    private void copy(InputStream input, OutputStream o1, OutputStream o2) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = input.read(buffer);
        while (bytesRead != -1) {
            o1.write(buffer, 0, bytesRead);
            if (o2 != null) {
                o2.write(buffer, 0, bytesRead);
            }
            bytesRead = input.read(buffer);
        }
    }
}
