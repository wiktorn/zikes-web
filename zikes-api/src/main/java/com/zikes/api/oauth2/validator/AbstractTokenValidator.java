package com.zikes.api.oauth2.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.zikes.api.exceptions.ServiceException;
import com.zikes.api.oauth2.OAuthAccessToken;
import com.zikes.api.oauth2.OAuthTokenValidatorIfc;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractTokenValidator implements OAuthTokenValidatorIfc {
    private static final Logger logger = Logger.getLogger(AbstractTokenValidator.class.getSimpleName());

    protected abstract URL getEndpoint(String accessToken) throws MalformedURLException;

    protected abstract OAuthAccessToken parseToken(JsonNode json);

    @Override
    public Optional<OAuthAccessToken> getIfValid(String accessToken) throws ServiceException {
        if (Strings.isNullOrEmpty(accessToken)) return Optional.absent();

        try {
            HTTPResponse response = URLFetchServiceFactory.getURLFetchService().fetch(getEndpoint(accessToken));

            String body = new String(response.getContent(), "UTF-8");
            logger.log(Level.FINE, String.format("AccessToken validation response := [%s]", body));

            if (HttpStatus.OK.value() != response.getResponseCode()) return Optional.absent();

            JsonNode json = new ObjectMapper().readTree(body);

            OAuthAccessToken token = parseToken(json);

            return Optional.of(token);
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Failed to validate token", ex);
            throw new ServiceException("Failed to validate access token");
        }
    }
}
