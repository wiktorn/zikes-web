package com.zikes.api.endpoints;


import com.zikes.api.model.Journey;
import com.zikes.api.oauth2.OAuthAuthentication;
import com.zikes.api.storage.gcs.AccessControlledResourceService;
import com.zikes.api.storage.gcs.UploadedTrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@RestController
@RequestMapping("/trackUpload")
public class TrackUploadEndpoint {

    private static final String MediaTypeZIP        = "application/zip";
    private static final String MediaTypeXML        = "text/xml";
    private static final String MediaTypePlainText  = "text/plain";

    @Autowired
    private UploadedTrackService uploadedTrackService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity post(
            @RequestHeader(value="Content-Type") String contentType,
            InputStream in) throws IOException {
        String zikesUserId = null;
        if (SecurityContextHolder.getContext().getAuthentication() instanceof OAuthAuthentication) {
            OAuthAuthentication oauth = (OAuthAuthentication)SecurityContextHolder.getContext().getAuthentication();
            zikesUserId = oauth.getZikesUser().getEncodedId();
        }
        switch (contentType) {
            case MediaTypeZIP:
                try(ZipInputStream zin = new ZipInputStream(in)) {
                    ZipEntry zentry = null;
                    while ((zentry = zin.getNextEntry()) != null) {
                        uploadedTrackService.put(zin, zikesUserId);
                    }
                }
                break;
            case MediaTypeXML:
            case MediaTypePlainText:
                uploadedTrackService.put(in, zikesUserId);
                break;
            default:
                return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }
}
