/**
 * scripts/lib/utils.js
 * Library of utils
 */

'use strict';

var utils = (function() {

	var urlParams;
	(window.onpopstate = function () {
	    var match,
	        pl     = /\+/g,  // Regex for replacing addition symbol with a space
	        search = /([^&=]+)=?([^&]*)/g,
	        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
	        query  = window.location.search.substring(1);

	    urlParams = {};
	    while (match = search.exec(query))
	       urlParams[decode(match[1])] = decode(match[2]);
	})();

    return {
    	urlParams   : urlParams,
	    mtsToString : function(mts) {
	        var result = "0mts"
	        if (mts) {
	            if (mts < 1000) {
	                result = mts+"mts";
	            } else if (mts < 5000) {
	                result = (mts/1000).toFixed(1);
	                result += "km";
	            } else {
	                result = ((mts/1000) | 0) +"km";
	            }
	        }
	        return result;
	    },
	    millisToString : function(millis) {
	        var result = "0ms"
	        if (millis) {
	            if (millis > 1000) {
	                result = (millis/1000).toFixed(2) + "s";
	            } else if (millis < 10) {
	                result = millis.toFixed(2);
	                result += "ms";
	            } else {
	                result = (millis|0) +"ms";
	            }
	        }
	        return result;
	    },
	    largeNumberToStr: function(number) {
	        var result = "0"
	        var suffixes = ["", "k", "m"];
	        for (var i = 0; i < suffixes.length; i++) {
	            if (number < 999) {
	                break;
	            }
	            number = (number / 1000).toFixed(2);
	        }
	        if (number > 10) {
	            number = number | 0;
	        }
	        return number + suffixes[i];
    	},
 		assert: function(condition, message) {
		    if (!condition) {
		        message = "Assertion failed: '"+message+"'";
		        console.error(message);
		        if (window.location.hostname == "localhost") {
		            throw new Error(message);
		        }
		    }
		},
	    pip: function(point, polygon) {
	        // ray-casting algorithm based on
	        // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

	        var x = point.lat, y = point.lng;

	        var inside = false;
	        for (var i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
	            var xi = polygon[i][0], yi = polygon[i][1];
	            var xj = polygon[j][0], yj = polygon[j][1];

	            var intersect = ((yi > y) != (yj > y))
	                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
	            if (intersect) inside = !inside;
	        }

	        return inside;
	    },
	    pips: function (point, polygons) {
	        for (var i = 0; i < polygons.length; i++) {
	            if (utils.pip(point, polygons[i])) {
	                return true;
	            }
	        }
	        return false;
	    },
	    range: function(from, to, step, cb) {
	    	var result = []
	    	if (from > to) {
	    		step = Math.sign(step) == -1 ? step : -step
	    		for (var i = from; i >= to; i+=step) {
	    			result.push(cb(i));
	    		}
	    	} else {
	    		step = Math.sign(step) == 1 ? step : -step
	    		for (var i = from; i <= to; i+=step) {
	    			result.push(cb(i));
	    		}
	    	}
	    	return result;
	    },
	    escapeXML: function(text) {
			return text.replace(/&/g, '&amp;')
			           .replace(/</g, '&lt;')
			           .replace(/>/g, '&gt;')
			           .replace(/"/g, '&quot;')
			           .replace(/'/g, '&apos;');
	    },
	    mergeSimpleObjects(dst, src, soft) {
	    	dst = dst || {};
	    	for (var k in src) {
	    		var v = src[k];
	    		if (k in dst) {
		    		if (typeof v === 'object') {
		    			utils.mergeSimpleObjects(dst[k], v, soft);
		    		} else if (!soft) {
		    			dst[k] = v;
		    		}
	    		} else {
	    			dst[k] = utils.cloneSimpleObject(v);
	    		}
	    	}
	    	return dst;
	    },
	    cloneSimpleObject(o) {
	    	return o ? JSON.parse(
	    		JSON.stringify(o)
	    	) : o;
	    }
	}

})();

module.exports = utils;