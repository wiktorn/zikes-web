/**
 * scripts/ui/ui.js
 *
 * This prepares and manages the Zikes's UI.
 */

'use strict';

var messages  = require("./messages.js");

var ui = (function() {

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        var message = '<span class="lead">This website is for trip <b>planning</b>, which we think is best done from home.</span><br>'
            +'<span class="small text-muted">And although some of it could be done away from home, we could only prioritise the big browser experience. We regret to bring this up, but</span><br>'
            +'<span class="lead"><b>This website is not mobile ready.</b></span><br>';
            if (/Android|Opera Mini/i.test(navigator.userAgent)) {
                message += '<span class="small">If you\'re already a user and would like to access your trips. If you\'re lost, unsure how to get home and/or are after the default, '
                          +'lightweigth <b>Zikes</b> experience, why not install our <a class="zikesAnchor" href="https://dl.dropboxusercontent.com/u/11723457/Zikes/net.Zikes.apk">Android App</a>.</span><br>';
            }

        messages.popup(message, {indismissable:true}).enqueue();
        $(".navbar").hide();
        $("body").removeClass("room-for-navbar");
        $("#mainWorkArea").css("padding", "0");
        return
    }
    var fastidiousDefinition = '<span>a :  having high and often capricious standards :  difficult to please <i>critics … so fastidious that they can talk only to a small circle of initiates — Granville Hicks</i><br></span>'
                              +'<span>b :  showing or demanding excessive delicacy or care<br></span>'
                              +'<span>c :  reflecting a meticulous, sensitive, or demanding attitude <i>fastidious workmanship</i><br><br></span>'
                              +'<span class=&quot;small text-warning&quot;>Merriam Webster</span>'
    var fastidious = '<a class="zikesAnchor" data-html="true" data-toggle="popover" data-trigger="hover" title="\\fa-ˈsti-dē-əs, fə-\\" data-content="'+fastidiousDefinition+'">fastidious</a>'
    var welcomeMessage    = messages.popup(
         '<span class="lead"><b>Zikes</b> is here so you can be '+fastidious+' about which way you walk or ride when you fancy walking or riding.<br></span>'
        +'<span class="lead"><b><a class="zikesAnchor" id="showMeHow" href="#">Show me how</a></b><br></span>'
        +'<span class="small text-muted">We\'re renting a small (32GB) server in Germany and can\'t yet afford serving the whole world. What we don\'t serve we cover with a pinkish overlay.<br>'
        +'We will use these popups rarely and try communicating with you unintrusivelly via the panel directly below.</span>',
        {once:"welcome"}
    );

    welcomeMessage.whenShown()
    .then(function() {
        $("[data-toggle=popover]").popover();
        $("#showMeHow").click(function() {
            welcomeMessage.hide();
            require("./tutorial.js").basicTutorial();
        });
    })

    $("#showTutorial").click(function() {
        require("./tutorial.js").basicTutorial();
    });

    return {
        welcomeMessage : welcomeMessage
    }

})();

module.exports  = ui;