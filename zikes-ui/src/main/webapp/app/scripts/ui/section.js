/**
 * scripts/ui/section.js
 *
 * This implements a Section - a drawable path between two route markers
 */

'use strict';

var when         = require("when");
var utils        = require("../lib/utils.js");
var geometry     = require("../lib/geometry.js");
var routeProfile = require("../lib/routeProfile.js");
var googleAPI    = require("../lib/google.js");
var map          = require("./map.js");
var streetView   = require("./streetView.js");
var drawnSegment = require("./drawnSegment.js");
var ctxMenu      = require("./contextMenu.js");
var messages     = require("./messages.js");

var section = (function() {

    function zoomend() {
        this.invalidate();
    }

    /*
     *
     * Basic functionality of a waymarker (a to/from/via point)
     *
     */
    function WayMarkerBase(params) {
        if (!params) {
            return;
        }
        this.mParent = params.parent;
        this.mLmMarker = L.marker(params.latlng, {
            icon      : params.icon || this.mParent.mIcons.via,
            opacity   : 0.7,
            draggable : true
        }).addTo(map.leaflet);

        var self = this;
        this.mLmMarker.on("dragstart", function() {
            self.mLmMarker._bringToFront();
            self.onDragStart();
        });
        this.mLmMarker.on("dragend", function() {
            setTimeout(function() { //workaround for https://github.com/Leaflet/Leaflet/issues/5067
                self.onDragEnd();
            }, 0);
        });
        this.mLmMarker.on("mousedown", function() {
            self.mBeingDragged = true;
        });
        this.mLmMarker.on("mouseup", function() {
            delete self["mBeingDragged"];
        });
    }

    WayMarkerBase.prototype.delete = function() {
        this.mLmMarker.off();
        map.leaflet.removeLayer(this.mLmMarker);
    };

    WayMarkerBase.prototype.hide = function() {
        $(this.mLmMarker._icon).animate({opacity: 0});
    };

    WayMarkerBase.prototype.show = function() {
        $(this.mLmMarker._icon).animate({opacity: 1});
    };

    WayMarkerBase.prototype.onDragEnd = function() {};

    WayMarkerBase.prototype.onDragStart = function() {
        this.mPrevLatLng = this.getLatLng();
    };

    WayMarkerBase.prototype.unstale = function() {
        this.mPrevLatLng = undefined;
    };

    WayMarkerBase.prototype.setIcon = function(icon) {
        this.mLmMarker.setIcon(icon);
    };

    WayMarkerBase.prototype.setLatLng = function(latlng) {
        this.mPrevLatLng = this.getLatLng();
        this.mLmMarker.setLatLng(latlng);
    };

    WayMarkerBase.prototype.getLatLng = function(latlng) {
        return this.mLmMarker.getLatLng();
    };

    WayMarkerBase.prototype.cloneLatLng = function(latlng) {
        var result = this.getLatLng().clone();
        result.decoration = utils.cloneSimpleObject(this.getLatLng().decoration) || {};
        return result;
    };

    WayMarkerBase.prototype.pixDistanceFrom = function(other) {
        var pixa = this.mLmMarker._icon.getBoundingClientRect();
        pixa = L.point(pixa.left, pixa.top);
        var pixb = other.mLmMarker._icon.getBoundingClientRect();
        pixb = L.point(pixb.left, pixb.top);
        return pixa.distanceTo(pixb);
    };

    WayMarkerBase.prototype.toJSON = function() {
        return [this.getLatLng().lat, this.getLatLng().lng];
    };

    WayMarkerBase.prototype.hasMoved = function() {
        return !!this.mPrevLatLng;
    };

    WayMarkerBase.prototype.isTemp = function() {
        return !((this.mNext && this.mNext.mPrev == this) ||
                 (this.mPrev && this.mPrev.mNext == this));
    };







    /*
     * A Section is a node in a doubly-linked list representing a route with all its
     * via points. A Section object is therefore a route's start, end or one of its via points.
     * Section's mWaypoints is an array of latlng coordinates spanning between 'this' and mNext.
     * The tail (closing) section has no mWaypoints.
     */
    function Section(params) {
        if (!params) {
            return; //for prototype construction
        }
        WayMarkerBase.call(this, params);
        this.mPrev          = params.prev;
        this.mNext          = params.next;
        this.mWaypoints     = params.waypoints;
        this.mProfile       = undefined;
        this.getLatLng().decoration = this.getLatLng().decoration || {};
        this.getLatLng().decoration.distanceMts = {
            fromStart : 0
        }
        var self = this;
        this.mLmMarker.on("click", function(e) {
            var menu = ctxMenu(e.latlng).default().streetView({delete:true});
            if (!self.mNext) {
                //last
                menu.from({display: "Continue From"});
            }
            menu.separator();
            if (self.mPrev &&
                !(self instanceof RubberbandSection) &&
                !(self.mPrev instanceof RubberbandSection)) {
                menu.rubberband({
                    action: function() {
                        self.insertRubberband();
                    }
                });
            }
            if (!self.isTemp()) {
                menu.delete({action: function() {
                    self.delete();
                }});
            }
            if (!streetView.shown()) {
                menu = menu.streetView({
                    route:new WaypointIterator(self)
                })
            }
            menu.show();
        });
        this.mLmMarker.on('mouseover', function(e) {
            if (!self.isTemp()) {
                self.mParent.highlight(
                    self.distanceOffsetFromStartMts(),
                    true
                );
            }
            setTimeout(function() {
                if (self.mLmMarker._map) {
                    streetView.update(
                        new WaypointIterator(self)
                    );
                }
            }, 300);
        });
        this.mLmMarker.on('mouseout', function(e) {
            if (!self.isTemp()) {
                self.mParent.highlight(false);
            }
        });
        map.leaflet.on("zoomend", zoomend, this);
    }

    Section.prototype = new WayMarkerBase();
    Section.prototype.constructor = Section;


    Section.prototype.delete = function() {
        this.clear();
        if (!this.isTemp()) {
            if (this.mNext) {
                this.mNext.mPrev = this.mPrev;
            } else if (this.mPrev) {
                this.mPrev.setIcon(this.mParent.mIcons.to)
            }

            if (this.mPrev) {
                this.mPrev.clear();
                this.mPrev.mNext = this.mNext;
            } else {
                this.mParent.head(this.mNext);
                this.mNext.setIcon(this.mParent.mIcons.from)
            }
            this.mParent.invalidate();
        }
        map.leaflet.off("zoomend", zoomend, this);
        WayMarkerBase.prototype.delete.call(this);
    };



    Section.prototype.needsRouting = function(force) {
        return force ||
               (this.mNext &&
                (WayMarkerBase.prototype.hasMoved.call(this) ||
                 WayMarkerBase.prototype.hasMoved.call(this.mNext) || //either end has moved
                !this.mWaypoints) //or there is no waypoints
               );
    };

    Section.prototype.tail = function() {
        if (this.mNext) {
            return this.mNext.tail();
        }
        return this;
    };

    Section.prototype.prev = function(suchThat) {
        var current = this.mPrev;
        while(current && !suchThat(current)) {
            current = current.mPrev;
        }
        return current;
    };

    Section.prototype.next = function(suchThat) {
        var current = this.mNext;
        while(current && !suchThat(current)) {
            current = current.mNext;
        }
        return current;
    };

    Section.prototype.distanceOffsetFromStartMts = function() {
        var result = 0;
        var current = this.mPrev;
        while (current) {
            result += current.distanceMts()
            current = current.mPrev;
        }
        return result;
    };


    Section.prototype.toJSON = function() {
        var result = {
            from : WayMarkerBase.prototype.toJSON.call(this),
            ctor : this.constructor.name
        }
        if (this.mWaypoints) {
            result["waypoints"] = this.mWaypoints.map(function(elem) {
                var result = [elem.lat, elem.lng];
                if (elem.decoration) {
                    var decoration = {};
                    if (elem.decoration.e != undefined) {
                        decoration["e"] = elem.decoration.e;
                    }
                    if (elem.decoration.h) {
                        decoration["h"] = elem.decoration.h;
                    }
                    if (Object.keys(decoration).length) {
                        result.push(decoration);
                    }
                }
                return result;
            });
        }
        return result;
    };


    Section.prototype.append = function(section, dontProject) {
        utils.assert(this.tail() != section.tail(), "can't append sections sharing the same tail");
        //bi-list insert
        var prev = this.mNext ? this : this.mPrev;
        var next = this.mNext;

        //affix head
        prev.mNext = section;
        section.mPrev = prev;

        section.setIcon(this.mParent.mIcons.via);
        var current   = section;
        var oldParent = section.mParent;
        while(current) {
            current.mParent = this.mParent;
            if (!dontProject && current.projection && this.mWaypoints) {
                var appendToPrev  = undefined;
                var prependToNext = undefined;
                var splitAt = current.projection.geo.projectionSameAsIdx;
                var waypoints = this.mWaypoints;
                var tail      = undefined;
                if (splitAt != undefined) {
                    //projection is actually one of the waypoints, make sure it's on both segments
                    tail = this.mWaypoints[splitAt].clone();
                    waypoints.splice(splitAt, 0, tail);
                } else {
                    splitAt   = current.projection.idx.start+1;
                    tail      = current.projection.geo.projection.clone();
                    waypoints.splice(splitAt, 0, tail, tail.clone());
                }
                tail.decoration   = tail.decoration || {};
                tail.decoration.t = true; //terminator
                delete current["projection"];
                this.fromJSON(waypoints);
            }
            current = current.mNext;
        }
        if (oldParent && oldParent !== this.mParent ) {
            oldParent.mHead = undefined;
        }

        //affix tail
        if (!next) {
            this.delete();
        } else {
            var sectionTail = section.tail();
            next.mPrev = sectionTail;
            sectionTail.mNext = next;
        }
    };

    var rubberbandMessage = undefined;
    Section.prototype.insertRubberband = function() {
        if (this.isTemp()) {
            //temp would have disappeared by now
            var endRubberband = new Section({
                icon   : this.mParent.mIcons.via,
                latlng : this.cloneLatLng()
            });
            endRubberband.projection = this.projection;
            this.mPrev.append(endRubberband);
            this.delete();
        }
        this.mPrev.append(
            new RubberbandSection({
                icon   : this.mParent.mIcons.via,
                latlng : this.cloneLatLng()
            })
        );
        if (!rubberbandMessage) {
            rubberbandMessage = messages.popup(
                 '<span class="lead">So you\'ve been curious what those <b>rubberbands</b> were.<br></span>'
                +'<span class="small">They are a means of inserting arbitrary bird-flight sections into a route. Sections that don\'t follow roads and thus sections Zikes couldn\'t have offered. <br></span>'
                +'<span class="small text-muted">Perhaps you\'d like to make a <a class="zikesAnchor" href="http://zikes.website/?user=TMBx&journey=Zbhk_poeQgumCyGcr5wxNw" target="_blank">ferry crossing</a> part of a journey\'s leg. Perhaps you want to cheat on a train a bit or plought your bike through a field. <br>'
                +'Perhaps there is a newly built ramp that you know of but Zikes does not.<br></span>'
                +'<span class="lead">Your first rubberband is very short. <b>Stretch it</b> a bit (move that new marker) to see how it behaves.</span>',
                {once:"rubberband"}
            ).enqueue();
        }
        this.mParent.invalidate();
    };

    Section.prototype.orphan = function() {
        this.mNext = undefined;
        this.mPrev = undefined;
        return this;
    };


    Section.prototype.onDragEnd = function() {
        WayMarkerBase.prototype.onDragEnd.call(this);
        if (this.isTemp()) {
            this.mPrev.append(this.orphan(), true);
        } else if (this.mPrev || this.mNext) {
            //we'll merge to the tail
            //start assuming this is the tail that we've just move
            var self = this;
            var closeNeithbours = [];
            function saveIfClose(other) {
                if (other && other.pixDistanceFrom(self) < 8) {
                    closeNeithbours.push(other);
                }
            }
            var end = function(ofWhat) {return ofWhat.from(); }
            if (!this.mPrev) {
                //this is head that we've just moved
                end = function(ofWhat) {return ofWhat.to(); }
            }
            this.mParent.otherRoutes(function(other) {
                if (!other.isPending()) {
                    saveIfClose(end(other));
                }
            });
            if (closeNeithbours.length == 1) {
                //only offer to merge when there is exactly one neighbour
                var c = document.createElement('div');
                c.innerHTML='<a href="#">Merge</a>';
                var popup = L.popup()
                .setLatLng(this.getLatLng())
                .setContent(c)
                .openOn(map.leaflet);
                $(c).find("a").click(function() {
                    map.leaflet.removeLayer(popup);
                    if (!self.mPrev) {
                        closeNeithbours[0].append(self);
                    } else {
                        self.append(closeNeithbours[0]);
                    }
                    self.mParent.invalidate();
                });
                setTimeout(function() {
                    $(popup._wrapper).parent().fadeOut(2000, function() {
                        map.leaflet.removeLayer(popup);
                    });
                }, 1000);
            }
        }
        return this.mParent.invalidate();
    };


    /*
     * Distribute the json representation of a route's fragment (routing result or product of earlier toJSON) into the
     * sections following 'this'. In a general case, a route request has been issued between two existing
     * sections and number intermediates (optional via points). Now the result has come as single array of latlng
     * coordinates and, should via points been specified in the request, we need to find them between
     * this and 'end'.
     */
    Section.prototype.fromJSON = function(allWaypoints) {
        var sections = [];
        //Now scan the route request result and, whilst calculating the rolling distance and route profile, find best section
        //matches, i.e.: given a waypoint, calculate its distance to all the candiate sections and, within each section, store
        //the index of the nearest waypoint. The result should be:
        //All route's sections:
        //S          ->            S            ->           S              ->             S          ->          S
        //subset sections[] =     this                       x                            end
        //                         |                         |                             |
        //                         v                         V                             V
        //route req result  =      .........................................................
        var profile                 = new routeProfile.GeoRouteProfile(function(point) {return point.decoration; });
        var prevSectionWayPoint     = undefined;
        var fromSectionStartMts     = 0;
        var sectionWayPoints        = [];

        function calculateClimb(currentDecoration, idx) {
            var previousDecoration = undefined;
            var distanceFromPrev   = undefined;
            for (var i = idx-1; i >= 0; i--) {
                if (sectionWayPoints[i].decoration && sectionWayPoints[i].decoration.e != undefined) {
                    distanceFromPrev = currentDecoration.distanceMts.fromStart - sectionWayPoints[i].decoration.distanceMts.fromStart;
                    if (distanceFromPrev > 50) {
                        previousDecoration = sectionWayPoints[i].decoration;
                        break;
                    }
                }
            }
            if (previousDecoration) {
                return Math.round(100*(currentDecoration.e - previousDecoration.e)/distanceFromPrev);
            }
        }

        function newWaypoint(wp) {
            var result = wp;
            if (!(wp instanceof L.LatLng)) {
                result = new L.LatLng(wp[0], wp[1]);
                result.decoration = wp[2] || {}
            } else if (!result.decoration) {
                result.decoration = {};
            }
            result.decoration.distanceMts = {};
            return result;
        }

        for (var i = 0; i < allWaypoints.length; i++) {
            var sectionWaypoint = newWaypoint(allWaypoints[i]);
            var decoration      = sectionWaypoint.decoration;
            if (prevSectionWayPoint) {
                decoration.distanceMts.fromPrev = prevSectionWayPoint.distanceTo(sectionWaypoint);
                if ((decoration.distanceMts.fromPrev | 0) == 0) {
                    continue;
                }
                fromSectionStartMts += decoration.distanceMts.fromPrev;
            }
            decoration.distanceMts.fromStart = fromSectionStartMts | 0;
            sectionWayPoints.push(sectionWaypoint);
            prevSectionWayPoint = sectionWaypoint;
            if (decoration.e != undefined) {
                var climb = calculateClimb(decoration, sectionWayPoints.length-1);
                if (climb != undefined) {
                    decoration["climbBkwd"] = climb;
                }
                profile.add(sectionWaypoint);
            }
            if (decoration.t) {
                delete decoration["t"];
                sections.push({
                    profile     : profile.finish(decoration.distanceMts.fromStart),
                    waypoints   : sectionWayPoints
                });
                if (decoration.e == undefined) {
                    decoration.e = profile.last().get().e;
                }
                profile = new routeProfile.GeoRouteProfile(function(point) {return point.decoration;});
                fromSectionStartMts = 0;
                sectionWayPoints = [];
                prevSectionWayPoint = undefined;
            }
        }
        sections.push({
            profile     : profile.finish(fromSectionStartMts | 0),
            waypoints   : sectionWayPoints
        }); //for the tail
        this.setSections(sections);
    };

    Section.prototype.distanceMts = function() {
        if (this.mProfile) {
            return this.mProfile.distanceMts();
        }
        return 0;
    };

    Section.prototype.setSections = function(sections) {
        this.clear();
        this.mWaypoints   = sections[0].waypoints;
        this.mProfile     = sections[0].profile;
        if (sections.length > 1) {
            this.mNext.setSections(sections.splice(1))
        }
    };


    Section.prototype.prepareRoute = function(routesPromise, end) {
        var self = this;
        return when(routesPromise)
        .then(function(route) {
            self.fromJSON(route.waypoints);
        });
    };

    Section.prototype.projectLatLngOnto = function(latlng) {
        var nearestSegment = undefined;
        var minDistance = Number.MAX_VALUE;
        for (var i = 1; i < this.mWaypoints.length; i++) {
            var segment = new geometry.GeoLine(
                this.mWaypoints[i-1],
                this.mWaypoints[i]
            );
            var projection = segment.projectOnto(latlng, true);
            var distance   = latlng.distanceTo(projection);
            if (minDistance > distance) {
                minDistance = distance;
                nearestSegment = {
                    idxStart   : i-1,
                    idxEnd     : i,
                    start      : this.mWaypoints[i-1],
                    end        : this.mWaypoints[i],
                    segment    : segment,
                    projection : projection,
                };
            }
        }
        return nearestSegment;
    };

    Section.prototype.invalidate = function(force) {
        function areClose(div1, div2) {
            if (div1 && div2) {
                div1 = div1.getBoundingClientRect();
                div1 = L.point(div1.left, div1.top);
                div2 = div2.getBoundingClientRect();
                div2 = L.point(div2.left, div2.top);
                return div1.distanceTo(div2) < 20;
            }
            return true;
        }
        if (this.mNext) {
            if (!this.mPrev) {
                var tail = this.tail();
                if (areClose(this.mLmMarker._icon, tail.mLmMarker._icon)) {
                    WayMarkerBase.prototype.hide.call(this);
                    WayMarkerBase.prototype.hide.call(tail);
                } else {
                    WayMarkerBase.prototype.show.call(this);
                    WayMarkerBase.prototype.show.call(tail);
                }
            } else {
                var prev = this.prev(function(s) {return !!s.mLmMarker._icon});
                if (!prev || areClose(this.mLmMarker._icon, prev.mLmMarker._icon)) {
                    WayMarkerBase.prototype.hide.call(this);
                } else {
                    WayMarkerBase.prototype.show.call(this);
                }
            }
        }

        this.unstale();
        if ((!force && this.mDrawnRoute) || !this.mWaypoints) {
            return;
        }

        if (!this.mProfile) {
            var self = this;
            this.mProfile = new routeProfile.GeoRouteProfile(function(point) {return point.decoration; });
            this.mWaypoints.forEach(function(wp) {
                self.mProfile.add(wp);
            });
        }
        !this.mDrawnRoute || this.mDrawnRoute.clear();
        this.mDrawnRoute = undefined;
    }

    Section.prototype.style = function() {
        return this.mParent.style();
    }

    Section.prototype.draw = function(force) {
        if ((!force && this.mDrawnRoute) || !this.mWaypoints) {
            return;
        }
        function mouseMove(e) {
            if (lastHoverEvent.layerPoint.distanceTo(e.layerPoint) > KPixTolerance) {
                if (tempMarker) {
                    if (!tempMarker.mBeingDragged) {
                        tempMarker.delete();
                    }
                    tempMarker = undefined;
                }
                map.leaflet.off("mousemove", mouseMove);
                self.mParent.highlight(false);
            }
        };
        var self = this;

        !this.mDrawnRoute || this.mDrawnRoute.clear();
        this.mDrawnRoute = new drawnSegment.Polyline(
            this.mWaypoints,
            map.leaflet,
            this.style()
        ).show();

        var tempMarker          = undefined;
        var lastHoverEvent      = undefined;
        var KPixTolerance       = 10;
        this.mDrawnRoute.on('mouseover', function(e) {
            var segmentStart = e.nearest.geo.segment.mStart;
            self.mParent.highlight(
                segmentStart.decoration.distanceMts.fromStart +
                e.nearest.geo.projection.distanceTo(segmentStart)+
                self.distanceOffsetFromStartMts(),
                true
            );
            lastHoverEvent = e;
            if (tempMarker) {
                tempMarker.setLatLng(e.nearest.geo.projection);
            } else {
                tempMarker = new Section({
                    prev     : self,
                    next     : self.mNext,
                    latlng   : e.nearest.geo.projection,
                    parent   : self.mParent
                });
                map.leaflet.on("mousemove", mouseMove);
            }
            tempMarker.projection = e.nearest;
        });
        this.mDrawnRoute.bringToFront();
    };


    Section.prototype.clear = function() {
        this.mWaypoints = undefined;
        !this.mDrawnRoute || this.mDrawnRoute.clear();
        this.mProfile   = undefined;
        delete this["mDrawnRoute"];
    }

    Section.prototype.getBounds = function() {
        if (this.mDrawnRoute) {
            return this.mDrawnRoute.getBounds();
        }
    }

    Section.prototype.highlight = function(highlight) {
        if (this.mDrawnRoute) {
            if (highlight) {
                this.mDrawnRoute.highlight({
                    opacity: 1,
                    weight: 7
                });
            } else {
                this.mDrawnRoute.highlight();
            }
        }
    };






    /**
     *
     */
    function RubberbandSection(params) {
        Section.call(this, params);
    }

    RubberbandSection.prototype = new Section();
    RubberbandSection.prototype.constructor = RubberbandSection;


    RubberbandSection.prototype.needsRouting = function() {
        return false;
    };

    var KMaxApproxDistance = 45;
    function decorated(point, withPoint) {
        point.decoration = point.decoration || {};
        var approxAtDistance = point.distanceTo(withPoint);
        if (approxAtDistance <= KMaxApproxDistance ) {
            function accept() {
                point.decoration.e = withPoint.decoration.e;
                point.decoration.approxAtDistance = approxAtDistance;
            }
            if (point.decoration && point.decoration.e != undefined) {
                if (point.decoration.approxAtDistance != undefined && point.decoration.approxAtDistance > approxAtDistance) {
                    accept();
                }
            } else {
                accept();
            }
        }
        return point.decoration;
    };

    function whenDecorated(point, withProfileOfPoint, firstLast) {

        if (withProfileOfPoint &&
            withProfileOfPoint.mProfile &&
            decorated(
                point,
                withProfileOfPoint.mProfile[firstLast](function(dp) {return !!dp.point;}).point
            ).e != undefined) {
            return when(point)
        }
        return googleAPI.elevation([point])
        .then(function(elevations) {
            point.decoration = point.decoration || {};
            point.decoration.e = elevations[0];
            return point;
        });
    }

    RubberbandSection.prototype.invalidate = function(force) {
        if (force || !this.mWaypoints || this.hasMoved() || !this.mWaypoints[1].equals(this.mNext.getLatLng())) {
            var self = this;
            var next = this.mNext.cloneLatLng();
            next.decoration.distanceMts = next.decoration.distanceMts || {};
            next.decoration.distanceMts.fromStart = this.getLatLng().distanceTo(next);

            this.getLatLng().decoration = this.getLatLng().decoration || {distanceMts: {fromStart: 0}};

            return when.all([
                whenDecorated(this.getLatLng(), this.mPrev, "last"),
                whenDecorated(next,             this.mNext, "first")
            ]).then(function(waypoints) {
                self.clear();
                self.mWaypoints = waypoints;
                return Section.prototype.invalidate.call(self, force);
            });
        } else {
            return Section.prototype.invalidate.call(this, force);
        }
    };

    RubberbandSection.prototype.style = function() {
        var style = utils.cloneSimpleObject(
            Section.prototype.style.call(this)
        );
        style.dashArray="5, 5";
        return style;
    };

    RubberbandSection.prototype.delete = function() {
        Section.prototype.delete.call(this);
        if (this.mStyle && this.mPrev && this.mPrev instanceof RubberbandSection) {
            this.mPrev.invalidate();
        }
    };

    RubberbandSection.prototype.append = function(section) {
        section.delete();
        section = new RubberbandSection({
            latlng : section.getLatLng(),
            icon   : this.mParent.mIcons.via
        });
        Section.prototype.append.call(this, section);
    };






    function WaypointIterator(section, wayPointIndex) {
        this.mSection = section;
        if (wayPointIndex) {
            this.mWayPointIdx = wayPointIndex;
        }
    }

    WaypointIterator.prototype.current = function() {
        if (!("mSection" in this)) {
            return undefined;
        }
        if ("mWayPointIdx" in this && this.mSection.mWaypoints) {
            return this.mSection.mWaypoints[this.mWayPointIdx];
        }
        return this.mSection.getLatLng();
    }

    WaypointIterator.prototype.next = function(offset) {
        var self = this;
        function next() {
            if (self.mSection.isTemp()) {
                self.mWayPointIdx = self.mSection.mPrev.projectLatLngOnto(
                    self.mSection.getLatLng()
                ).idxStart;
                self.mSection     = self.mSection.mPrev;
            }
            if ("mWayPointIdx" in self) {
                if (self.mWayPointIdx == self.mSection.mWaypoints.length - 1) {
                    //last point of the section
                    if (self.mSection.mNext && self.mSection.mNext.mWaypoints) {
                        self.mSection = self.mSection.mNext;
                        delete self["mWayPointIdx"];
                    } else {
                        //fell off the cliff, iterator invalid now
                        delete self["mWayPointIdx"];
                        delete self["mSection"];
                        return undefined;
                    }
                } else {
                    ++self.mWayPointIdx;
                }
            } else if (self.mSection.mWaypoints) {
                self.mWayPointIdx = 0;
            } else {
                delete self["mWayPointIdx"];
                delete self["mSection"];
                return undefined;
            }
            return self.current();
        }

        offset = offset || 1;
        var metres = 0;
        var projectionTolerance = undefined;
        if (typeof offset === 'object') {
            if (offset.metres) {
                metres = offset.metres;
                projectionTolerance = 40;
            } else if (offset.minMetres) {
                metres = offset.minMetres;
            } else {
                console.error("Wrong offset!!");
                console.dir(offset);
            }
            offset = 1;
        }
        var previous = undefined;
        var result   = this.current();
        for (;offset > 0 || metres > 0; offset--) {
            previous = result;
            result   = next();
            if (!result) {
                return undefined;
            }
            metres -= previous.distanceTo(result);
        }
        metres = Math.abs(metres);
        if (projectionTolerance && (metres > projectionTolerance)) {
            //asked for some metres (not minMetres), but we've now overshot and are too far behind the current result
            if (result.distanceTo(previous) < projectionTolerance) {
                //we've overshot by a smaller distance than projectionTolerance, so return previous
                result = previous;
            } else {
                //project
                result = new geometry.GeoLine(previous, result).offsetMts(metres);
            }
        }
        return result;
    }

    WaypointIterator.prototype.peekNext = function(offset) {
        return new WaypointIterator(this.mSection, this.mWayPointIdx).next(offset);
    }

    return {
        Section: Section,
        RubberbandSection : RubberbandSection
    }

})();

module.exports = section;