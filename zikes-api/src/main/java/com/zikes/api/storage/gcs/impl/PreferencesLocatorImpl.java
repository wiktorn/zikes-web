package com.zikes.api.storage.gcs.impl;

import com.zikes.api.model.Metadata;
import com.zikes.api.storage.ZikesIdManager;
import com.zikes.api.storage.gcs.ResourceLocatorIfc;

public class PreferencesLocatorImpl implements ResourceLocatorIfc {
    private static final String DIR = "users/%s/plan/preferences/";

    @Override
    public String getPath(Long zikesId) {
        return String.format(DIR, ZikesIdManager.encodeId(zikesId));
    }

    @Override
    public String getPath(Long zikesId, String preferencesId) {
        return String.format(DIR, ZikesIdManager.encodeId(zikesId)) + preferencesId;
    }

    @Override
    public String getPath(Metadata metadata) {
        return getPath(metadata.getOwner(), metadata.getId());
    }
}
