package com.zikes.api.storage;

import org.apache.commons.codec.binary.Base64;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.UUID;

public class ZikesIdManager {
    public static String generateId() {
        UUID uuid = UUID.randomUUID();

        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());

        return new Base64().encodeBase64URLSafeString(bb.array());
    }

    public static String encodeId(Long id) {
        return new Base64().encodeBase64URLSafeString(BigInteger.valueOf(id).toByteArray());
    }

    public static Long decodeId(String encodedId) {
        return new BigInteger(new Base64(true).decode(encodedId)).longValue();
    }
}
