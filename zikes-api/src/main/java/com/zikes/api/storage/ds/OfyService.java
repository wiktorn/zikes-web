package com.zikes.api.storage.ds;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.zikes.api.model.ZikesUser;
import com.zikes.api.storage.ds.translator.ObjectNodeStringTranslatorFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("objectifyService")
public class OfyService {
    static {
        ObjectifyService.factory().getTranslators().add(new ObjectNodeStringTranslatorFactory());
        ObjectifyService.register(ZikesUser.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}
