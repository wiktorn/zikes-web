/**
 * tests/mocks.js
 *
 * This has some mocking primitives
 */


var when = require("when");

var mocks = (function() {

    function MockObject() {}

    function MockInvocation() {
        this.mInvocations   = [];
        this.mSubscriptions = [];
        this.method         = this.call.bind(this);
        var self            = this;
        this.method["calledTimes"] = function() {
            times = 0
            args  = (arguments.length === 1?[arguments[0]]:Array.apply(null, arguments));
            for (var i = 0; i < self.mInvocations.length; i++) {
                if (argsMatch(self.mInvocations[i], args)) {
                    ++times;
                }
            }
            return times;
        }
        this.method["whenCalled"] = function(args, times) {
            args = args == undefined ? [] : args;
            var promise = when.defer();
            var subscription = {
                promise:promise,
                args   : args,
                times  : times || 1
            };
            self.mInvocations.forEach(function(invocation) {
                checkSubscription(subscription, invocation);
            });
            self.mSubscriptions.push(subscription);
            return promise.promise;
        }
        return this;
    }

    MockInvocation.prototype.returns = function(what) {
        this.mReturns = what;
        return this;
    }

    function argsMatch(agrsRhs, argsLhs) {
        return JSON.stringify(agrsRhs) === JSON.stringify(argsLhs);
    }

    function checkSubscription(subscription, invocation) {
        if ( argsMatch(subscription.args, invocation) ) {
            if (--subscription.times == 0) {
                subscription.promise.resolve(invocation);
            }
        }
    }

    MockInvocation.prototype.call = function() {
        this.mInvocations.push(
            (arguments.length === 1?[arguments[0]]:Array.apply(null, arguments))
        );
        var self = this;
        this.mSubscriptions.forEach(function(subscription) {
            self.mInvocations.forEach(function(invocation) {
                checkSubscription(subscription, invocation)
            });
        });
        return typeof this.mReturns === "function" ? this.mReturns() : this.mReturns;
    }

    return {
        MockObject      : MockObject,
        MockInvocation  : MockInvocation
    }

})();

module.exports = mocks;