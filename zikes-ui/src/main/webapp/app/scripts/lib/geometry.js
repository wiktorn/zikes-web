/**
 * scripts/lib/geometry.js
 */

'use strict';
var intersects = require("../external/line-segments-intersect.js");

var geometry = (function() {

    function dot(v1,v2) {
        return (v1.x*v2.x)+(v1.y*v2.y);
    }

    function cross(v1, v2) {
        return (v1.x*v2.y)-(v1.y*v2.x)
    }

    function ldot(v1,v2) {
        return (v1.lat*v2.lat)+(v1.lng*v2.lng);
    }

    var GeoVector = L.LatLng;
    function GeoLine(start, end) {
        this.mStart = start;
        this.mEnd   = end;
        this.mV     = new GeoVector(this.mEnd.lat - this.mStart.lat, this.mEnd.lng - this.mStart.lng);
    }

    GeoLine.prototype.projectOnto = function(geoPoint, force) {
        if (this.lengthSqrd() == 0) {
            //0-lenght segment => point
            return force ? this.mStart : undefined;
        }
        var projectionRatio = ldot(
            this.mV,
            new GeoVector(geoPoint.lat - this.mStart.lat, geoPoint.lng - this.mStart.lng) //GeoVector: mStart->geoPoint
        )/this.lengthSqrd();

        if (projectionRatio < 0) {
            //beyond the mStart end of the segment
            return force ? this.mStart : undefined;
        } else if (projectionRatio > 1) {
            //beyond the mEnd end of the segment
            return force ? this.mEnd : undefined;
        }
        return new L.LatLng(
            this.mStart.lat + (projectionRatio * this.mV.lat),
            this.mStart.lng + (projectionRatio * this.mV.lng)
        );
    }

    GeoLine.prototype.offsetMts = function(metresFromStart) {
        var segmentLengthMts   = this.mStart.distanceTo(this.mEnd);
        return this.offsetRatio(metresFromStart/segmentLengthMts);
    }

    GeoLine.prototype.offsetRatio = function(ratio) {
        return new L.LatLng(
            this.mStart.lat + (this.mV.lat * ratio),
            this.mStart.lng + (this.mV.lng * ratio)
        );
    }

    GeoLine.prototype.lengthSqrd = function() {
        return Math.pow(this.mV.lat,2)+Math.pow(this.mV.lng,2);
    }

    GeoLine.prototype.length = function() {
        return Math.sqrt(this.lengthSqrd());
    }




    function PixVector() {
        if (arguments.length == 1 && arguments instanceof L.Point) {
            L.Point.call(
                this,
                arguments[0].x,
                arguments[0].y
            );
        } else if (arguments.length == 2) {
            if (arguments[0] instanceof L.Point && arguments[1] instanceof L.Point) {
                L.Point.call(
                    this,
                    arguments[1].x - arguments[0].x,
                    arguments[1].y - arguments[0].y
                );
            } else {
                L.Point.call(
                    this,
                    arguments[0],
                    arguments[1]
                );
            }
        }
    }
    PixVector.prototype = new L.Point();
    PixVector.prototype.constructor = PixVector;

    PixVector.prototype.angle = function() {
        return (180/Math.PI) * Math.atan2(this.x, this.y);
    }

    //see: http://www.freesteel.co.uk/wpblog/2009/06/05/encoding-2d-angles-without-trigonometry/
    PixVector.prototype.diamondAngle = function(other) {
        var result = undefined;
        if (this.y >= 0) {
            result = (this.x >= 0 ? this.y/(this.x+this.y) : 1-this.x/(-this.x+this.y));
        } else {
            result = (this.x < 0 ? 2-this.y/(-this.x-this.y) : 3+this.x/(this.x-this.y));
        }
        result = 2-result;
        if (other) {
            result = result - other.diamondAngle();
        }
        return result;
    }


    PixVector.prototype.dot = function(other) {
        return dot(this, other);
    }

    PixVector.prototype.cross = function(other) {
        return cross(this, other);
    }

    PixVector.prototype.slope = function() {
        return this.y/this.x;
    }

    PixVector.prototype.lengthSqrd = function() {
        return Math.pow(this.x,2) + Math.pow(this.y, 2);
    }

    PixVector.prototype.length = function() {
        return Math.sqrt(this.lengthSqrd());
    }

    PixVector.prototype.manhattanLenght = function() {
        return this.x + this.y;
    }





    function PixLine(start, end) {
        this.mStart = start;
        this.mEnd   = end;
        this.mV     = new PixVector(this.mEnd.x - this.mStart.x, this.mEnd.y - this.mStart.y);
    }

    PixLine.prototype.projectOnto = function(pixPoint, force) {
        if (this.lengthSqrd() == 0) {
            //0-lenght segment => point
            return force ? this.mStart : undefined;
        }
        var projectionRatio = dot(
            this.mV,
            new PixVector(pixPoint.x - this.mStart.x, pixPoint.y - this.mStart.y) //PixVector: mStart->pixPoint
        )/this.lengthSqrd();

        if (projectionRatio < 0) {
            //beyond the mStart end of the segment
            return force ? this.mStart : undefined;
        } else if (projectionRatio > 1) {
            //beyond the mEnd end of the segment
            return force ? this.mEnd : undefined;
        }
        return new L.Point(
            this.mStart.x + (projectionRatio * this.mV.x),
            this.mStart.y + (projectionRatio * this.mV.y)
        );
    }

    PixLine.prototype.offsetMts = function(metresFromStart) {
        var segmentLengthMts   = this.mStart.distanceTo(this.mEnd);
        return this.offsetRatio(metresFromStart/segmentLengthMts);
    }

    PixLine.prototype.offsetRatio = function(ratio) {
        return new L.LatLng(
            this.mStart.x + (this.mV.x * ratio),
            this.mStart.y + (this.mV.y * ratio)
        );
    }

    PixLine.prototype.lengthSqrd = function() {
        return Math.pow(this.mV.x,2)+Math.pow(this.mV.y,2);
    }

    PixLine.prototype.length = function() {
        return Math.sqrt(this.lengthSqrd());
    }

    PixLine.prototype.flip = function() {
        return new PixLine(this.mEnd, this.mStart);
    }

    PixLine.prototype.toString = function() {
        return "["+this.mStart.toString()+", "+this.mEnd.toString()+"]";
    }

    PixLine.prototype.intersect = function(other) {
        var point = intersects(
            [[this.mStart.x, this.mStart.y], [this.mEnd.x, this.mEnd.y]],
            [[other.mStart.x, other.mStart.y], [other.mEnd.x, other.mEnd.y]]
        );
        if (point) {
            return L.point(point[0], point[1]);
        }
    }



    function linearApprox(v1, v2, v1v2Distance, approxAtDistance) {
        var slope = 1.0*(v2-v1)/v1v2Distance;
        return v1+(slope*approxAtDistance);
    }

    return {
        GeoLine : GeoLine,
        PixLine : PixLine,
        PixVector : PixVector,
        linearApprox : linearApprox
    }

})();

module.exports = geometry;