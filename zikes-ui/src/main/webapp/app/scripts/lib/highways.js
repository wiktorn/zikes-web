/**
 * scripts/lib/highways.js
 *
 * This, based on the value of the master slider,
 * and values of subordinate (per-highway) sliders
 * calculates and return the highway routing preference
 */

'use strict';

var when     = require("when");
var mustache = require("mustache");
var map      = require("../ui/map.js");

var highwaysPreference = (function() {

    var verticalSlider = when.defer();
    $.get("templates/tools.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#routingPreferenceVerticalSlider_tmpl").html()
        verticalSlider.resolve(html);
    });

    var KVerticalSliderSpreadFactor = 10;
    var KDistanceFunctionSlope = 1;
    var KHighwaysMeta = {
        "path" : {
            color: "#996633"
        },
        "track" : {
            color: "#c68c53"
        },
        "bridleway" : {
            foldInto : "track"
        },
        "steps" : {
            color: "#ff751a"
        },
        "pedestrian" : {
            color: "#ffff66"
        },
        "footway" : {
            foldInto : "pedestrian"
        },
        "cycleway" : {
            color: "#80ff80"
        },
        "residential" : {
            color: "#ffffff"
        },
        "living_street" : {
            foldInto: ["residential"]
        },
        "road" : {
            color: "#cce0ff"
        },
        "tertiary_link" : {
            foldInto: "road"
        },
        "unclassified" : {
            foldInto: "road"
        },
        "bus_guideway" : {
            foldInto: "road"
        },
        "service" : {
            foldInto: "road"
        },
        "tertiary" : {
            foldInto: "road"
        },
        "secondary" : {
            color: "#99c2ff"
        },
        "secondary_link" : {
            foldInto: "secondary"
        },
        "primary": {
            color: "#66a3ff"
        },
        "primary_link" : {
            foldInto: "primary"
        },
        "trunk" : {
            color: "#3385ff"
        },
        "trunk_link" : {
            foldInto: "trunk"
        },
        "motorway" : {
            color: "#0066ff"
        },
        "motorway_link" : {
            foldInto: "motorway"
        }
    };

    /************************************************************
     *
     * Highway
     *
     ************************************************************/
    function Highway(name, meta) {
        this.mName     = name;
        this.mMeta     = meta;
        this.mIncluded = [];
    }

    Highway.prototype.name = function() {
        return this.mName;
    }

    Highway.prototype.id = function() {
        if (map.highwaysById) {
            return map.highwaysById[this.mName];
        }
    }

    Highway.prototype.color = function() {
        return this.root().mMeta.color;
    }

    Highway.prototype.included = function() {
        return this.mIncluded;
    }


    Highway.prototype.root = function() {
        if ("foldInto" in this.mMeta) {
            return KHighways.highway(this.mMeta.foldInto);
        }
        return this;
    }


    /************************************************************
     *
     * Highways
     *
     ************************************************************/
    function Highways() {
        this.mHighways = {};
        for (var name in KHighwaysMeta) {
            this.mHighways[name] = new Highway(name, KHighwaysMeta[name]);
        }
        for (var name in this.mHighways) {
            var highway = this.mHighways[name];
            if ("foldInto" in highway.mMeta) {
                this.mHighways[highway.mMeta.foldInto].mIncluded.push(highway);
            }
        }
        var bySize = [
            "path","track","steps","pedestrian","cycleway","residential","road","secondary","primary","trunk","motorway"
        ];
        this.mBySize = [];
        for (var i = 0; i < bySize.length; i++) {
            this.mBySize.push(
                this.mHighways[bySize[i]]
            );
        }
    }

    Highways.prototype.highway = function(idOrName) {
        var name = idOrName;
        if (typeof idOrName === 'number' && map.highwaysById()) {
            name = map.highwaysById()[name];
        }
        return this.mHighways[name];
    }

    Highways.prototype.bySize = function() {
        return this.mBySize;
    }
    var KHighways = new Highways();



    /******************************************************************
     *
     * HighwaySlider
     * Managing a per-highway slider that takes values between [0..1]
     * for max and min punishment respectivelly.
     *
     * The value of the slider is:
     * - what is served by HighwaySlider.value and what should be used
     *   to calculate the preference;
     * - what is represented visually;
     * - what is calculated from the two two components:
     *   - HighwaySlider.mValue    - 0..1 intrisic value of the slider
     *   - HighwaySlider.mAdjustTo - 0..1 factor with which
     *                               HighwaySlider.mValue is adjusted
     *                               to produce the final slider value.
     * HighwaySlider.mAdjustTo reflects the position of the master
     * slider (one for all highways). This assures that the master
     * and individual sliders can be manipulated and saved individually.
     * This also causes that individual highway sliders don't stick after
     * being set (they are always subject to master slider).
     *****************************************************************/
    function HighwaySlider(highway, minMax, parent) {
        this.mParent   = parent;
        this.mHighway  = highway;
        this.mValue    = 1.0;
        this.mAdjustTo = 1.0;
        this.mMinMax   = minMax;
    }

    HighwaySlider.prototype.render = function(panel, template, padding) {
        panel.append(
            mustache.render(template, {
                label: this.mHighway.name(),
                min: {
                    value : this.mMinMax[0]
                },
                max: {
                    value : this.mMinMax[1]
                },
                value: 0,
                widthpx: 23
            })
        );
        this.mSlider = $("#"+this.mHighway.name()+"_slider").slider();
        var parent = this.mSlider.parent();
        var width  = this.mSlider.outerWidth();
        parent.find(".slider-selection").css("background", this.mHighway.color());
        var handle = parent.find(".slider-handle");
        handle.css("display", "none");
        handle.css("width", width);
        parent.parent().find(".slider").css("width", width);
        var track = parent.parent().find(".slider-track");
        track.css("width", width);
        track.css("margin-left", "-7px");
        track.css("background", "#2b3e50");
        if (padding) {
            parent.parent().find(".slider").css("margin-right", "6.9px");
        }
        var self = this;
        var dMinMax = this.mMinMax[1]-this.mMinMax[0];
        this.mSlider.on('slideStop', function(ev) {
            var value = dMinMax-(self.mSlider.getValue()-self.mMinMax[0]); //move to 0 based range
            self.mValue = value/(self.mAdjustTo*dMinMax);
            self.mParent.mParent.invalidate();
        });
    }

    HighwaySlider.prototype.name = function() {
        return this.mHighway.name();
    }

    HighwaySlider.prototype.included = function() {
        return this.mHighway.included();
    }

    HighwaySlider.prototype.value = function() {
        return this.mValue * this.mAdjustTo;
    }

    function setSliderValue(slider, absValue, adjustment, minMax) {
        if (slider) {
            var value = minMax[1]-(absValue*adjustment*(minMax[1]-minMax[0]));
            slider.setValue(value);
        }
    }

    HighwaySlider.prototype.toJSON = function() {
        return {
            value: this.mValue
        };
    }

    HighwaySlider.prototype.fromJSON = function(json) {
        this.mValue = json ? json.value : 1.0;
        setSliderValue(this.mSlider, this.mValue, this.mAdjustTo, this.mMinMax);
    }

    HighwaySlider.prototype.adjustTo = function(masterSliderFactor) {
        this.mAdjustTo = masterSliderFactor;
        setSliderValue(this.mSlider, this.mValue, this.mAdjustTo, this.mMinMax);
    }


    /************************************************************
     *
     * HighwaysPreference
     *
     ************************************************************/
    function HighwaysPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }

    HighwaysPreference.prototype.masterSlideOptions = function() {
        this.mMaxValue = KHighways.bySize().length*KVerticalSliderSpreadFactor;
        this.mMinValue = 0;
        return {
            label: "roads",
            value: JSON.stringify([this.mMinValue, this.mMaxValue]),
            min: {
                value: this.mMinValue,
                label: "smaller roads"
            },
            max: {
                value: this.mMaxValue,
                label: "bigger roads"
            },
            step: 1
        }
    }


    HighwaysPreference.prototype.render = function(panel) {
        var minMax = [1, KHighways.bySize().length+1]
        var self   = this;
        this.mHighwaySliders = [];
        return when(verticalSlider.promise)
        .then(function(template) {
            for (var i = 0; i < KHighways.bySize().length; i++) {
                var highwaySlider = new HighwaySlider(KHighways.bySize()[i], minMax, self);
                highwaySlider.render(panel, template, i != KHighways.bySize().length-1);
                self.mHighwaySliders.push(highwaySlider);
            }
        });
    }


    /*
     * Must strictly generate value between 0..1
     */
    function factorFromMasterSlider(self, masterSliderValue, x) {
        var leftSliderValue  = Math.min(masterSliderValue[0], masterSliderValue[1]);
        var rightSliderValue = Math.max(masterSliderValue[0], masterSliderValue[1]);
        x *= KVerticalSliderSpreadFactor;
        var adjust = 0;
        if (x <= leftSliderValue) {
            var distance = leftSliderValue-x;
            adjust = distance/self.mMaxValue;
        } else if (x >= rightSliderValue) {
            var distance = x-rightSliderValue+KVerticalSliderSpreadFactor;
            adjust = distance/self.mMaxValue;
        }
        return 1.0 - adjust;
    }

    HighwaysPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        for (var i = 0; i < this.mHighwaySliders.length; i++) {
            var highwaySlider = this.mHighwaySliders[i];
            highwaySlider.adjustTo(
                factorFromMasterSlider(this, masterSliderValue, i)
            );
        }
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    function slider2factor(sliderValue) {
        return Math.pow(sliderValue,2);
    }

    HighwaysPreference.prototype.generate = function(rootJson) {
        var result = {};
        for (var i = 0; i < this.mHighwaySliders.length; i++) {
            var highwaySlider = this.mHighwaySliders[i];
            var factor = slider2factor(1.0/highwaySlider.value()).toFixed(2);
            result[highwaySlider.name()] = {
                cost: factor
            }
            for (var j = 0; j < highwaySlider.included().length; j++) {
                result[highwaySlider.included()[j].name()] = {
                    cost: factor
                }
            }
        }
        rootJson["highwayCostFactors"] = result;
    }

    HighwaysPreference.prototype.fromJSON = function(json) {
        for (var i = 0; i < this.mHighwaySliders.length; i++) {
            var highwaySlider = this.mHighwaySliders[i];
            var sliderJSON = json ? json[highwaySlider.name()] : undefined;
            highwaySlider.fromJSON(sliderJSON);
        }
    }

    HighwaysPreference.prototype.toJSON = function() {
        var result = {};
        for (var i = 0; i < this.mHighwaySliders.length; i++) {
            var highwaySlider = this.mHighwaySliders[i];
            result[highwaySlider.name()] = highwaySlider.toJSON();
        }
        return result;
    }

    return {
        HighwaysPreference  : HighwaysPreference,
        sliderGenerator     : HighwaysPreference,
        highways            : KHighways
    }

})();

module.exports = highwaysPreference;