package com.zikes.api.oauth2.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.zikes.api.oauth2.OAuthAccessToken;
import com.zikes.api.oauth2.OAuthProvider;

import java.net.MalformedURLException;
import java.net.URL;

public class GoogleTokenValidator extends AbstractTokenValidator {
    private static final String TOKEN_INFO_ENDPOINT = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s";

    @Override
    protected URL getEndpoint(String accessToken) throws MalformedURLException {
        return new URL(String.format(TOKEN_INFO_ENDPOINT, accessToken));
    }

    @Override
    protected OAuthAccessToken parseToken(JsonNode json) {
        OAuthAccessToken token = new OAuthAccessToken();
        token.setProvider(OAuthProvider.GOOGLE);
        token.setExpiresIn(json.get("expires_in").asInt());
        token.setUserId(json.get("user_id").asText());

        return token;
    }
}
