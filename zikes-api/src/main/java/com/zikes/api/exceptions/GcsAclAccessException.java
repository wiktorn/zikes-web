package com.zikes.api.exceptions;

import org.springframework.security.acls.model.AclDataAccessException;

public class GcsAclAccessException extends AclDataAccessException {

    public GcsAclAccessException(String msg) {
        super(msg);
    }
}
