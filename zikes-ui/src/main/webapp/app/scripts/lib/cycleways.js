/**
 * scripts/lib/cycleway.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the cycleway preference
 */

'use strict';

var cyclewayPreference = (function() {


    function CyclewaySliderPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }

    var KSlider =  {
        min : 1,
        max : 40
    };
    var KFactor = {
        min: 0.1,
        max: 1.0
    }
    var Ka = (KFactor.min - KFactor.max)/(KSlider.max-KSlider.min);
    var Kb = KFactor.max - Ka;
    CyclewaySliderPreference.prototype.masterSlideOptions = function() {
        var result = {
            label: "cycleways",
            value: KSlider.min,
            min: {
                value: KSlider.min,
                label: ""
            },
            max: {
                value: KSlider.max,
                label: "more cycleways"
            },
            step: 1
        }
        this.mMasterSliderValue = result.value;
        return result;
    }


    CyclewaySliderPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        this.mMasterSliderValue = masterSliderValue
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    CyclewaySliderPreference.prototype.render = function(panel) {
    }

    CyclewaySliderPreference.prototype.generate = function(rootJson) {
        var factor = Math.round(100*(Ka*this.mMasterSliderValue + Kb))/100.0;
        var highwayFactors = rootJson["highwayCostFactors"]
        highwayFactors["default"] = {
            designated: factor.toFixed(2)
        }
        for (var highwayType in highwayFactors) {
            var factors4HighwayType = highwayFactors[highwayType];
            factors4HighwayType["designated"] = (parseInt(factors4HighwayType["cost"]) * factor).toFixed(2);
        }
    }

    return {
        CyclewaySliderPreference: CyclewaySliderPreference,
        sliderGenerator : CyclewaySliderPreference
    }

})();

module.exports = cyclewayPreference;