package com.zikes.api.endpoints;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.internal.mapper.ObjectMapperType;
import com.zikes.api.model.ZikesUser;
import org.junit.Before;

import static com.jayway.restassured.RestAssured.get;

public abstract class AbstractFunctionalTest {
    protected final LocalServiceTestHelper helper = createLocalServiceTestHelper();

    private LocalServiceTestHelper createLocalServiceTestHelper() {
        LocalDatastoreServiceTestConfig testConfig = new LocalDatastoreServiceTestConfig();
        testConfig.setBackingStoreLocation("tmp/functionaltest-datastore");
        LocalServiceTestHelper testHelper = new LocalServiceTestHelper(testConfig);
        return testHelper;
    }

    protected ZikesUser zikesUser;

    @Before
    public void setUp() {
        helper.setUp();
        RestAssured.port = 7777;

        zikesUser = get("/users/metadata").as(ZikesUser.class, ObjectMapperType.JACKSON_2);
    }
}
