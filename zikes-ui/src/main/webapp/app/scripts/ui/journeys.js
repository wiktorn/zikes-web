/**
 * scripts/ui/journeys.js
 *
 * This manages the journeys panel
 */

'use strict';

var when         = require("when");
when["sequence"] = require('when/sequence');
var mustache     = require("mustache");
var Sortable     = require("sortablejs");

var fileSaver    = require("../external/fileSaver.js");

var zikesApi     = require("../lib/zikesRouter.js");
var utils        = require("../lib/utils.js");
var googleAPI    = require("../lib/google.js");
var utils        = require("../lib/utils.js");

var map          = require("./map.js");
var Route        = require("./panelRoute.js").Route;
var POI          = require("./poi.js").POI;
var share        = require("./share.js");
var messages     = require("./messages.js");

var RoutingPreference = require("./routingPreference.js").RoutingPreference;
var journeyTemplates  = require("./journeyItem.js").templates;

var journeys = (function() {

    function Journeys(journeysPanelDiv, journeyPanelDiv) {
        this.mJourneysDiv = journeysPanelDiv;
        var self          = this;
        this.mJourney     = new Journey(journeyPanelDiv);
        this.mJourney.shown(function(shown) {
            if (!shown) {
                if (!self.mShown) {
                    map.expand();
                } else {
                    self.show();
                }
            }
        });
        if (utils.urlParams.journey && utils.urlParams.user) {
            when(map.loaded).then(function() {
                return zikesApi.loadPlannedJourney(utils.urlParams.journey, utils.urlParams.user);
            }).then(function(journeyJSON) {
                self.mJourney.fromJSON(journeyJSON);
            }).otherwise(function(e) {
                var errorMessage = undefined;
                if (e.status == 403) {
                    errorMessage = messages.popup(
                         '<span class="lead">The author of this journey does not authorise you to see it.</span>'
                    )
                } else if (e.status == 404) {
                    errorMessage = messages.popup(
                         '<span class="lead">This journey does not exist (might have been deleted).</span>'
                    )
                } else {
                    errorMessage = messages.popup(
                         '<span class="lead">An error occurred accessing this journey.<br></span>'
                        +'<span >The server sais: '+JSON.stringify(e)+'</span>'
                    )
                }
                errorMessage.enqueue()
                .whenHidden().then(function() {
                    self.clearUrlQuery();
                });
            });
        }
    }

    Journeys.prototype.clearUrlQuery = function() {
        window.history.pushState("object or string", "Title", "/"+window.location.href.substring(window.location.href.lastIndexOf('/') + 1).split("?")[0]);
    }


    Journeys.prototype.show = function() {
        var self = this;
        this.mShown = true;
        return when(this.render())
        .then(function() {
            return when.all([
                map.shrink(),
                self.mJourney.hide()
            ]);
        });
    }

    Journeys.prototype.hide = function() {
        this.mShown = false;
        return map.expand();
    }

    Journeys.prototype.current = function() {
        return this.mJourney;
    }

    Journeys.prototype.render = function() {
        function append(journeyMeta, template) {
            self.mJourneysDiv.append(mustache.render(template, {
                id: journeyMeta.id,
                description: journeyMeta.opaque.description || "description",
                name: journeyMeta.opaque.name
            }));
            var journeyDiv = $("#"+journeyMeta.id);

            journeyDiv.find("#open").click(function() {
                zikesApi.loadPlannedJourney(journeyMeta.id)
                .then(function(journeyJSON) {
                    self.mJourney.fromJSON(journeyJSON);
                });
            });
            journeyDiv.find("#delete").click(function() {
                zikesApi.deletePlannedJourney(journeyMeta.id)
                .then(function() {
                    journeyDiv.remove();
                    self.show();
                });
            });
        }

        var self = this;
        var now = new Date().getTime();
        if (!this.mLastReloaded || (now-this.mLastReloaded > 10000) ) {
            this.mLastReloaded = now;
            return when.all([zikesApi.plannedJourneys(), journeyTemplates])
            .then(function(results) {
                var journeyMetas = results[0];
                var template = results[1].Journey;
                self.mJourneysDiv.empty();
                var heading = "Your Journeys"
                if (journeyMetas.length == 0) {
                    heading = "You have no saved journeys";
                }
                self.mJourneysDiv.append("<h5>"+heading+"</h5>");
                journeyMetas.map(function(journeyMeta) {
                    append(journeyMeta, template);
                });
            });
        }
    }











    function Button(div, onClickCb) {
        this.mDiv = div;
        if (onClickCb) {
            this.click(onClickCb);
        }
    }

    Button.prototype.show = function(flag) {
        var delayMs = 1000;
        flag = (flag == undefined) ? true : flag;
        var result = when.defer();
        if (flag) {
            this.mDiv.fadeIn(delayMs, function() {
                result.resolve();
            });
        } else {
            this.mDiv.fadeOut(delayMs, function() {
                result.resolve();
            });
        }
        return result.promise.delay(delayMs);
    }

    Button.prototype.hide = function() {
        return this.show(false);
    }

    Button.prototype.click = function(cb) {
        return this.mDiv.click(cb);
    }




    function Journey(panelDiv) {
        function newMetadata() {
            return {
                opaque: {},
                access: {
                    ro: "",
                    rw: ""
                }
            };
        }
        this.mJourneyDiv = panelDiv;
        this.mItemsDiv   = panelDiv.find("#journeyItems");
        this.mClosables  = [];
        this.mObservers  = {
            shown : []
        };
        this.mItems     = [];
        this.mMetadata  = newMetadata();
        var self        = this;
        this.mRoutingPreferences = new RoutingPreference(
            function() { self.route(); }
        );
        this.mActions = {
            download : new Button($("#journeyDownload"), function() {
                self.download();
            }),
            save : new Button($("#journeySave"), function() {
                self.autoSave();
            }),
            share : new Button($("#journeyShare"), function() {
                var l = window.location;
                var port = l.port == 80 ? "" : ":"+l.port;
                var dialog = new share.Dialog(
                    l.protocol+"//"+l.hostname+port+"?user="+self.mMetadata.userId+"&journey="+self.mMetadata.id,
                    self.mMetadata.access,
                    function(accessMeta) {
                        self.mMetadata.access = accessMeta;
                        return self.autoSave()
                        .then(function() {
                            return self.mMetadata.access;
                        });
                    }
                );
                self.mClosables.push(dialog);
                dialog.show();
            }),
            close : new Button($("#journeyClose"), function() {
                self.mMetadata = newMetadata();
                self.hide();
            })
        }

        this.mJourneyDiv.find("#journeyName").blur(function() {
            if (self.name() != $(this).text()) {
                //something got editted
                if ($(this).text() == "") {
                    self.mMetadata.opaque.givenName = undefined;
                } else {
                    self.mMetadata.opaque.givenName = $(this).text();
                }
                self.invalidate();
            }
        });

        new Sortable(
            this.mItemsDiv[0], {
                handle: ".draghandle",
                onUpdate : function(evt) {
                    //we need to save that new order
                    var moved = self.mItems.splice(evt.oldIndex, 1);
                    self.mItems.splice(evt.newIndex, 0, moved[0]);
                    self.invalidate();
                }
            }
        );
    }

    Journey.prototype.forEach = function(what, cb) {
        this.mItems.forEach(function(item) {
            if (item instanceof what) {
                cb(item);
            }
        });
    }

    var elevationMessage = undefined;
    Journey.prototype.invalidate = function(options) {
        options = options || {};
        var self = this;
        this.mMetadata.opaque.name = this.name();
        this.mJourneyDiv.find("#journeyName").text(this.mMetadata.opaque.name);
        var distanceMts = 0;
        var showsElevation = false;
        this.forEach(Route, function(route) {
            if (route.isPending()) {
                if (!self.mPendingRoute || self.mPendingRoute.isUnititialised()) {
                    self.deleteItem(route, true);
                    self.mPendingRoute = route;
                } else {
                    route.delete();
                }
            }
            distanceMts += route.distanceMts();
            showsElevation = showsElevation || route.showsElevation();
        });
        if (showsElevation && !elevationMessage) {
            var routeOrRoutes = this.mItems.length > 1 ? {subject : "Some routes", verb : "are"} : {subject : "The route", verb: "is"};
            elevationMessage = messages.popup(
                 '<span class="lead">'+routeOrRoutes.subject+' that\'s just appeared '+routeOrRoutes.verb+' rugged and thus coloured to highlight the <b>elevation</b>.<br></span>'
                +'<span class="small text-muted">Which, you should note, is <b>different from climb</b> - we tried showing climb instead, but it didn\'t look informative (too many flips).<br></span>'
                +'<span class="small">Every route has its individual colour spectrum (unrelated to other routes in the journey).</span>',
                {once:"elevation"}
            ).enqueue();
        }

        this.mJourneyDiv.find("#journeyDistance").text(utils.mtsToString(distanceMts));
        if (this.mPendingRoute && !this.mPendingRoute.isPending()) {
            this.mItems.push(this.mPendingRoute);
            this.mPendingRoute = undefined;
        }

        if (this.mItems.length) {
            if (this.mItems.length == 1) {
                var theOnlyItem = this.mItems[0]
                if (theOnlyItem instanceof Route && theOnlyItem.isPending()) {
                    return;
                }
            }
            this.show();
            when.sequence([
                function() {
                    return self.mActions.save.show(!self.mMetadata.opaque.autosave);
                },
                function() {
                    return self.mActions.share.show(!!self.mMetadata.id);
                }
            ]);
            if (this.mMetadata.opaque.autosave && !options.dontSave) {
                this.save();
            }

        } else {
            self.hide();
            if (this.mMetadata.opaque.autosave && !options.dontSave) {
                this.delete();
            }
        }
    }

    Journey.prototype.shown = function(cb) {
        this.mObservers.shown.push(cb);
    }

    Journey.prototype.show = function() {
        var journeyDiv = $("#journeyWrapper");
        if (!journeyDiv.hasClass("hidden")) {
            return when(true);
        }
        if (!("mShowing" in this)) {
            journeyDiv.removeClass("hidden");
            var self = this;
            this.mShowing = map.shrink()
            .then(function() {
                var finished = when.defer();
                journeyDiv.animate({"left" : "0px"}, function() {
                    finished.resolve();
                });
                return when(finished.resolve)
                .then(function() {
                    self.mObservers.shown.forEach(function(observerCb) {
                        observerCb(true);
                    });
                    delete self["mShowing"];
                });
            });
        }
        return this.mShowing;
    }

    Journey.prototype.hide = function() {
        var journeyDiv = $("#journeyWrapper");
        if (journeyDiv.hasClass("hidden")) {
            return when(true);
        }
        if (!("mHiding" in this)) {
            var self = this;
            var animatedAway = when.defer();
            $("#journeyWrapper").animate({"left" : "-300px"}, function() {
                journeyDiv.addClass("hidden");
                animatedAway.resolve();
            });
            this.mHiding = when(animatedAway.promise).then(function() {
                self.mItems.forEach(function(item) {
                    delete item["mParent"] //prevent from invalidating parent
                    item.delete();
                });
                self.mItems = [];
                self.mObservers.shown.forEach(function(observerCb) {
                    observerCb(false);
                });
                delete self["mHiding"];
            });
        }
        this.mClosables.map(function(closable) {
            closable.close();
        })
        return this.mHiding;
    }

    Journey.prototype.name = function() {
        if (this.mMetadata.opaque.givenName) {
            return this.mMetadata.opaque.givenName;
        }
        var from, to;
        this.forEach(Route, function(route) {
            if (!route.isPending()) {
                if (!from) {
                    from = route.from().geocode;
                }
                to = route.to().geocode;
            }
        });
        if (from && to) {
            var fromTo = googleAPI.geocodeHumanReadable([from,to]);
            return fromTo[0] + " -> " + fromTo[1];
        }
        return "New Journey";
    }

    Journey.prototype.pendingRoute = function() {
        var shownRoute = this.shownRoute();
        if (shownRoute && shownRoute.isPending()) {
            return shownRoute;
        }
        if (!this.mPendingRoute) {
            this.mPendingRoute = new Route(this, this.mItemsDiv);
        }
        return this.mPendingRoute;
    }

    Journey.prototype.newPOI = function(latlng) {
        this.mItems.push(
            new POI(this, this.mItemsDiv, latlng)
        );
    }

    Journey.prototype.route = function(milestones) {
        if (milestones) {
            //this is request from the UI towards the server
            return zikesApi.requestRoute(
                milestones,
                this.mRoutingPreferences.generatePreference()
            );
        } else {
            //this is request towards the UI - to reroute as circumstances changed
            var shownRoute = this.shownRoute();
            if (shownRoute) {
                shownRoute.route();
            }
        }
    }

    Journey.prototype.shownRoute = function() {
        var shownRoute = undefined;
        this.forEach(Route, function(route) {
            if (route.isShown()) {
                shownRoute = route;
            }
        });
        return shownRoute;
    }

    Journey.prototype.download = function() {
        if (this.mItems.length) {
            var result = '<?xml version="1.0" encoding="UTF-8"?>\n'
            result    += '<gpx xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" creator="BTT for Qt" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">\n';
            result    += '<metadata><name>'+utils.escapeXML(this.name())+'</name></metadata>\n';
            this.mItems.forEach(function(item) {
                result += item.toGpx();
            });
            result += "</gpx>";
            var blob = new Blob([result], {type: "text/xml"});
            fileSaver.saveAs(blob, this.name()+".gpx");
        }
    }

    Journey.prototype.toJSON = function() {
        return {
            metadata: this.mMetadata,
            journey: {
                items: this.mItems.map(function(item) {
                    if (item instanceof Route) {
                        return { route : item.toJSON() };
                    } else if (item instanceof POI) {
                        return { poi : item.toJSON() };
                    }
                    return { item : item.toJSON() };
                })
            }
        }
    }

    Journey.prototype.fromJSON = function(json) {
        var self = this;
        this.mMetadata = undefined;
        this.mItems.forEach(function(item) {
            item.delete();
        });
        this.mMetadata = json.metadata;
        utils.assert(this.mItems.length == 0);
        this.invalidate = function() {} //ignore any invalidates flowing during deserialisation
        when.sequence(
            json.journey.items.map(function(itemJSON) {
                return function() {
                    var item = undefined;
                    if ("route" in itemJSON) {
                        item = new Route(self, self.mItemsDiv);
                        itemJSON = itemJSON["route"];
                    } else if ("poi" in itemJSON) {
                        item = new POI(self, self.mItemsDiv);
                        itemJSON = itemJSON["poi"];
                    } else {
                        assert(false, "unknown journey item type: "+ JSON.stringify(itemJSON))
                        return;
                    }

                    self.mItems.push(item);
                    return item.fromJSON(itemJSON);
                }
            })
        ).then(function() {
            return self.show();
        }).then(function() {
            self.centre();
            delete self["invalidate"];
            self.invalidate({dontSave:true});
        });
    }

    Journey.prototype.centre = function() {
        if (this.mItems.length) {
            var bounds = this.mItems.reduce(function(cum, item) {
                return cum.extend(item.getBounds())
            }, this.mItems[0].getBounds());

            map.leaflet.fitBounds(
                bounds,
                {
                    animate : true,
                    padding : [30,30]
                }
            );
        }
    }

    Journey.prototype.save = function() {
        var self = this;
        return zikesApi.savePlannedJourney(
            this.toJSON()
        ).then(function(metadata) {
            self.mMetadata = metadata;
            self.invalidate({dontSave:true});
        });
    }

    Journey.prototype.delete = function() {
        if (this.mMetadata) {
            zikesApi.deletePlannedJourney(
                this.mMetadata.id
            );
        }
        this.hide();
    }

    Journey.prototype.autoSave = function() {
        this.mMetadata.opaque.autosave = true;
        return this.save();
    }

    Journey.prototype.deleteItem = function(item, dontInvalidate) {
        if (item === this.mPendingRoute) {
            this.mPendingRoute = undefined;
        } else {
            for (var i = 0; i < this.mItems.length; i++) {
                if (this.mItems[i] === item) {
                    this.mItems.splice(i,1);
                    break;
                }
            }
        }
        if (!dontInvalidate) {
            this.invalidate();
        }
    }

    return {
        Journeys: Journeys,
        Journey : Journey,
        journeys : new Journeys($("#journeys"), $("#journey"))
    }
})();

module.exports = journeys;