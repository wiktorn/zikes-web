var assert   = require("assert");
var geometry = require("../scripts/lib/geometry.js");

suite("Geometry", function() {

    test("should be able to project a point onto a line", function() {
        assert.deepEqual(
            new geometry.GeoLine(
                new L.LatLng(
                    51.5280828104,-0.151345191193
                ),
                new L.LatLng(
                    51.5289371874,-0.145980773163
                )
            ).projectOnto(
                new L.LatLng(
                    51.5294044249,-0.148469863129
                )
            ),
            new L.LatLng(
                51.52856212240955,-0.1483357117875383
            )
        );

        assert.deepEqual(
            new geometry.PixLine(
                new L.point(662,650),
                new L.point(726,668)
            ).projectOnto(
                new L.point(726, 670), true
            ),
            new L.point(726,668)
        );
    });

    test("should be able to produce a point at an offset on a line segment", function() {
        assert.deepEqual(
            new geometry.GeoLine(
                new L.LatLng(
                    51.5280828104,-0.151345191193
                ),
                new L.LatLng(
                    51.5289371874,-0.145980773163
                )
            ).offsetMts(100),
            new L.LatLng(
                51.52830559919402,-0.1499463564589885
            )
        );
    });
});
