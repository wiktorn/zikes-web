var assert        = require("assert");
var when          = require("when");
var mocks         = require("./mocks.js");
var utils         = require("../scripts/lib/utils.js");


var proxyquire = require('proxyquireify')(require);

var stubs = {
  "./http.js": {
      get: function ()  { return when('wirklich wunderbar'); },
      post: function () { return when('schokolade'); }
  },
  "../lib/google.js" : {
    geocodeFindUI : function() {}
  },
  "../lib/geoip.js" : {
    get : function() {return {}}
  },
  "@noCallThru": true
};
var routeModule   = proxyquire("../scripts/ui/mapRoute.js", stubs);
var sectionModule = proxyquire("../scripts/ui/section.js", stubs);


var mockServerRouteMeta = {
    avgLookup_us:48,minLookup_us:33,maxLookup_us:62,routing_us:44,lengthMts:175,routingRequests:[{name:"astar",visitedJunctions:19}]
};


suite("Route", function() {

  suite("a->b", function() {
    var from  = [51.53636589077571,-0.09503602981567383];
    var to    = [51.53739360230881,-0.09237527847290039];
    var route = [[51.5363235474,-0.0950741171837,{"e":21,"h":12}],[51.5362243652,-0.0949624031782],[51.5361976624,-0.0949330031872,{"e":21,"h":12}],[51.5362739563,-0.0947683006525,{"e":21,"h":12}],[51.5363922119,-0.0945058986545,{"e":21,"h":12}],[51.5366973877,-0.0938593000174,{"e":21,"h":12}],[51.5368537903,-0.0935136005282,{"e":21,"h":12}],[51.5369873047,-0.09320320189,{"e":21,"h":12}],[51.5372467041,-0.0926453024149,{"e":21,"h":12}],[51.5373725891,-0.0923748984933, {"e":21}]]
    var distanceRawMts = 0;
    for (var i = 1; i < route.length; i++) {
        var f = new L.LatLng(route[i-1][0], route[i-1][1]);
        var t = new L.LatLng(route[i][0], route[i][1]);
        distanceRawMts += f.distanceTo(t);
    }
    distanceRawMts = distanceRawMts | 0;

    test("should (de)serialse both ways", function(done) {
        var routeUnderTest = new routeModule.Route({
            route : function(milestones) {
                return when({
                    waypoints: utils.cloneSimpleObject(route),
                    meta: mockServerRouteMeta
                });
            },
            invalidate: function() {
            },
            style: function() {
                return {
                    color : {
                        base : '#FF0000'
                    }
                }
            }
        });
        routeUnderTest.from(from);
        routeUnderTest.to(to);
        when(routeUnderTest.invalidate(true))
        .then(function() {
            assert.deepEqual(routeUnderTest.toJSON(), [{
                from:from,
                ctor:"Section",
                waypoints:route
            },{
                from:to,
                ctor:"Section"
            }]);
            assert.equal(routeUnderTest.distanceMts(), distanceRawMts);
        })
        .otherwise(function(err) {
            done(err);
        })
        .then(function() {
            done();
        });
    });
  });

  suite("a->b->c", function() {
    var from  = [51.51216458809261,-0.14804184436798096];
    var via   = [51.51171388332373,-0.1472318172454834];
    var to    = [51.512141218325326,-0.14508068561553955];
    var route = [[51.5121307373,-0.148056000471,{"e":32,"h":13}],[51.5125312805,-0.14590279758,{"e":28,"h":11}],[51.5123939514,-0.145521402359,{"e":28,"h":11}],[51.5122413635,-0.145161002874],[51.5121192932,-0.14503569901,{"e":28,"h":8}],[51.5121040344,-0.145098894835,{"e":28,"h":8}],[51.5117912292,-0.146770894527,{"e":30,"h":8}],[51.5117034912,-0.147229924798,{"e":30,"t":true}],[51.5117034912,-0.147229924798,{"e":30,"h":8}],[51.5115966797,-0.147805199027,{"e":31,"h":13}],[51.5121307373,-0.148056000471,{"e":32,"h":13}],[51.5125312805,-0.14590279758,{"e":28,"h":11}],[51.5123939514,-0.145521402359,{"e":28,"h":11}],[51.5122413635,-0.145161002874],[51.5121192932,-0.14503569901,{"e":28}]]

    function prepareRoute(parent) {
        var routeUnderTest = new routeModule.Route(parent);
        routeUnderTest.from(from);
        routeUnderTest.to(to);
        new sectionModule.Section({
            parent   : routeUnderTest,
            latlng   : via,
            prev     : routeUnderTest.mHead,
            next     : routeUnderTest.mHead.tail(),
            icon     : routeUnderTest.mIcons.from
        }).onDragEnd();
        return routeUnderTest;
    }

    function prepareParent() {
        var mockParent = new mocks.MockObject();
        mockParent["route"] = new mocks.MockInvocation()
        .returns(function() {
            return {
                waypoints: utils.cloneSimpleObject(route),
                meta: mockServerRouteMeta
            };
        }).method;
        mockParent["invalidate"] = new mocks.MockInvocation().method;
        mockParent["style"] = new mocks.MockInvocation()
        .returns(function() {
            return {
                color : {
                    base : '#FF0000'
                }
            };
        }).method;
        return mockParent;
    }

    test("should (de)serialse a complex a->b->c route", function(done) {
        var parent = prepareParent();
        var routeUnderTest = prepareRoute(parent);
        when(routeUnderTest.invalidate())
        .then(function() {
            var s1 = route.slice(0,8);
            delete s1[s1.length-1][2].t;
            var s2 = route.slice(8);
            assert.deepEqual(routeUnderTest.toJSON(), [
                {
                    from: from,
                    ctor: "Section",
                    waypoints: s1
                },
                {
                    from: via,
                    ctor: "Section",
                    waypoints: s2
                },
                {
                    from: to,
                    ctor: "Section"
                }
            ]);
            assert.equal(parent.invalidate.calledTimes(), 2);
            assert.equal(parent.route.calledTimes([from,via,to]), 2);
        })
        .otherwise(function(err) {
            done(err);
        })
        .then(function() {
            done();
        });
    });


    test("should delete a marker and reroute", function(done) {
        var parent = prepareParent();
        var routeUnderTest = prepareRoute(parent);
        parent.invalidate.whenCalled()
        .then(function() {
            routeUnderTest.mHead.mNext.delete();
        });
        parent.invalidate.whenCalled(undefined, 2)
        .then(function() {
            assert.equal(parent.route.calledTimes([from,to]), 1);
            assert.equal(routeUnderTest.mHead.mNext.mNext, undefined);
            done();
        })
        .otherwise(function(err) {
            done(err);
        });
    });

    test("should leave just from when tail is gone", function(done) {
        var parent = prepareParent();
        var routeUnderTest = prepareRoute(parent);
        parent.invalidate.whenCalled()
        .then(function() {
            routeUnderTest.mHead.tail().delete();
        });
        parent.invalidate.whenCalled(undefined, 2)
        .then(function() {
            assert(!!routeUnderTest.from());
            assert(!!routeUnderTest.to());
            assert.equal(routeUnderTest.mHead.mNext.mNext, undefined);
        })
        .then(function() {
            routeUnderTest.mHead.tail().delete();
            return parent.invalidate.whenCalled()
        })
        .then(function() {
            assert(!!routeUnderTest.from());
            assert(routeUnderTest.to() == undefined);
            assert.equal(routeUnderTest.mHead.mNext, undefined);
            done();
        })
        .otherwise(function(err) {
            done(err);
        });
    });

    test("should leave just to when head is gone", function(done) {
        var parent = prepareParent();
        var routeUnderTest = prepareRoute(parent);
        parent.invalidate.whenCalled()
        .then(function() {
            routeUnderTest.mHead.delete();
        });
        parent.invalidate.whenCalled(undefined, 2)
        .then(function() {
            assert(!!routeUnderTest.from());
            assert(!!routeUnderTest.to());
            assert.equal(routeUnderTest.mHead.mNext.mNext, undefined);
        })
        .then(function() {
            routeUnderTest.mHead.delete();
            return parent.invalidate.whenCalled()
        })
        .then(function() {
            assert(routeUnderTest.from() == undefined);
            assert(!!routeUnderTest.to());
            assert.equal(routeUnderTest.mHead.mNext, undefined);
            done();
        })
        .otherwise(function(err) {
            done(err);
        });
    });
  });
});