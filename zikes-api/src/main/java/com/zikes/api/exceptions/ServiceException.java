package com.zikes.api.exceptions;

import org.springframework.http.HttpStatus;

import javax.servlet.ServletException;

public class ServiceException extends ServletException {
    private final HttpStatus status;

    public ServiceException(String message) {
        this(HttpStatus.INTERNAL_SERVER_ERROR, message);
    }

    public ServiceException(HttpStatus status, String message) {
        super(message);

        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
