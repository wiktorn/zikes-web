/**
 * scripts/lib/routeProfile.js
 *
 * This collects and santises a geo route profile, i.e.: such consisting of distances and elevations
 */

'use strict';

var geometry = require("./geometry.js");

var routeProfile = (function() {

    function GeoRouteProfile(accessFun) {
        this.mProfile = [{
            get : function() {
                return {
                    e : undefined,
                    h : undefined,
                    distanceMts : {
                        fromStart : 0
                    }
                };
            }
        }];
        this.mMinEleMts = undefined;
        this.mMaxEleMts = undefined;
        this.mMaxClimb  = undefined;
        this.mMinClimb  = undefined;
        this.mAccessFun = accessFun || function(point) {return point;}
    }

    function max(accu,nval) {
        return accu == undefined ? nval : Math.max(accu, nval);
    }
    function min(accu,nval) {
        return accu == undefined ? nval : Math.min(accu, nval);
    }

    GeoRouteProfile.prototype.add = function(point) {
        var eleMts = this.mAccessFun(point).e;
        if (eleMts != undefined && eleMts != null && !isNaN(eleMts)) {
            var dp = {
                point,
                get : this.mAccessFun.bind(this, point)
            };
            this.mMinEleMts = min(this.mMinEleMts, eleMts);
            this.mMaxEleMts = max(this.mMaxEleMts, eleMts);
            this.mMinClimb  = min(this.mMinClimb,  dp.get().climbBkwd || 0);
            this.mMaxClimb  = max(this.mMaxClimb,  dp.get().climbBkwd || 0);

            if (dp.get().distanceMts.fromStart == 0) {
                this.mProfile[0] = dp
            } else {
                this.mProfile.push(dp);
            }
        }
    }

    GeoRouteProfile.prototype.last = function(that) {
        var lastIdx = this.mProfile.length-1;
        if (that) {
            for (var i = lastIdx; i >= 0; i--) {
                if (that(this.mProfile[i])) {
                    return this.mProfile[i];
                }
            }
        } else {
            return this.mProfile[lastIdx];
        }
    }

    GeoRouteProfile.prototype.first = function(that) {
        if (that) {
            return this.mProfile.find(that);
        }
        return this.mProfile[0];
    }

    GeoRouteProfile.prototype.minEleMts = function() {
        return this.mMinEleMts;
    }

    GeoRouteProfile.prototype.maxEleMts = function() {
        return this.mMaxEleMts;
    }

    GeoRouteProfile.prototype.minClimb = function() {
        return this.mMinClimb;
    }

    GeoRouteProfile.prototype.maxClimb = function() {
        return this.mMaxClimb;
    }

    GeoRouteProfile.prototype.midEleMts = function() {
        return this.mMinEleMts+(1.0*this.dEleMts()/2);
    }

    GeoRouteProfile.prototype.dEleMts = function() {
        return this.mMaxEleMts - this.mMinEleMts;
    }

    GeoRouteProfile.prototype.distanceMts = function() {
        return this.last().get().distanceMts.fromStart;
    }

    GeoRouteProfile.prototype.profile = function() {
        return this.mProfile;
    }

    var KExpectDatapointAtLeastEveryKms = 5;
    var KExpectDatapointEveryFractionOfTotalDistance = 0.15;
    GeoRouteProfile.prototype.finish = function(totalDistanceMts) {
        var self = this;
        //is last present? if not, we'll approximate from the previous two
        var last = this.last().get();
        if (last.distanceMts.fromStart < totalDistanceMts) {
            var lastIdx = this.mProfile.length-1;
            this.mProfile.push({
                get : function() {
                    return {
                        e : geometry.linearApprox(
                            self.mProfile[lastIdx-1].get().e,
                            self.mProfile[lastIdx].get().e,
                            self.mProfile[lastIdx].get().distanceMts.fromStart - self.mProfile[lastIdx-1].get().distanceMts.fromStart,
                            totalDistanceMts - self.mProfile[lastIdx].get().distanceMts.fromStart
                        ),
                        h : self.mProfile[lastIdx].get().h,
                        distanceMts : {
                            fromStart : totalDistanceMts
                        }
                    };
                }
            });
        }

        var first = this.first().get();
        if (first.e == undefined) {
            this.mProfile[0] = {
                get : function() {
                    return {
                        e : geometry.linearApprox(
                            self.mProfile[1].get().e,
                            self.mProfile[2].get().e,
                            self.mProfile[2].get().distanceMts.fromStart - self.mProfile[1].get().distanceMts.fromStart,
                           -self.mProfile[1].get().distanceMts.fromStart
                        ),
                        h : first.h || self.mProfile[1].get().h,
                        distanceMts : first.distanceMts
                    };
                }
            }
        }
        return this;
    }





    function MergedProfile() {
        this.mProfiles  = [];
        this.mDistances = [];
        this.mMinEleMts = undefined;
        this.mMaxEleMts = undefined;
        this.mMaxClimb  = undefined;
        this.mMinClimb  = undefined;
    }

    MergedProfile.prototype.add = function(geoProfile) {
        if (geoProfile instanceof GeoRouteProfile) {
            this.mProfiles.push(geoProfile);
            this.mMinEleMts = min(this.mMinEleMts, geoProfile.minEleMts());
            this.mMaxEleMts = max(this.mMaxEleMts, geoProfile.maxEleMts());
            this.mMinClimb  = min(this.mMinClimb, geoProfile.minClimb());
            this.mMaxClimb  = max(this.mMaxClimb, geoProfile.maxClimb());

            if (this.mDistances.length == 0) {
                this.mDistances.push(geoProfile.distanceMts())
            } else {
                this.mDistances.push(geoProfile.distanceMts()+this.mDistances[this.mDistances.length-1]);
            }
        }
    }

    MergedProfile.prototype.minEleMts = function() {
        return this.mMinEleMts;
    }

    MergedProfile.prototype.maxEleMts = function() {
        return this.mMaxEleMts;
    }

    MergedProfile.prototype.minClimb = function() {
        return this.mMinClimb;
    }

    MergedProfile.prototype.maxClimb = function() {
        return this.mMaxClimb;
    }

    MergedProfile.prototype.dEleMts = function() {
        return this.mMaxEleMts - this.mMinEleMts;
    }

    MergedProfile.prototype.midEleMts = function() {
        return this.mMinEleMts+(1.0*this.dEleMts()/2);
    }

    MergedProfile.prototype.isRugged = function() {
        return this.dEleMts() > 100 || (this.dEleMts() > 10 && this.dEleMts() > this.distanceMts()/80);
    }

    MergedProfile.prototype.eleMts = function(atDistanceMts) {
        var it = this.iterator();
        var prev = undefined;
        while(true) {
            var current = it.current();
            if (!current) {
                return undefined;
            }
            if (current.distanceMts.fromStart == atDistanceMts) {
                return current.eleMts;
            }
            if (current.distanceMts.fromStart > atDistanceMts) {
                return geometry.linearApprox(
                    prev.eleMts,
                    current.eleMts,
                    current.distanceMts.fromStart - prev.distanceMts.fromStart,
                    atDistanceMts-prev.distanceMts.fromStart
                );
            }
            prev = current;
            it.next();
        }
        return current.eleMts;
    }

    MergedProfile.prototype.distanceMts = function() {
        return this.mDistances[this.mDistances.length-1];
    }

    MergedProfile.prototype.dataPointsNo = function() {
        return this.mProfiles.reduce(function(cum, geoProfile) {
            return cum + geoProfile.mProfile.length;
        }, 0);
    }

    MergedProfile.prototype.iterator = function() {
        return new MergedProfileIterator(this);
    }






    function MergedProfileIterator(profile) {
        this.mParent       = profile;
        this.mProfileIdx   = 0;
        this.mDPIndex      = 0;
    }

    MergedProfileIterator.prototype._nextIdx = function(idxs) {
        if (idxs[0] == undefined || idxs[0] > this.mParent.mProfiles.length-1) {
            return undefined;
        }
        if (idxs[1]+1 > this.mParent.mProfiles[idxs[0]].mProfile.length-1) {
            return this._nextIdx([idxs[0]+1, 0]);
        }
        return [idxs[0], idxs[1]+1];
    }

    MergedProfileIterator.prototype._value = function(idxes) {
        if (idxes) {
            var dp = this.mParent.mProfiles[idxes[0]].mProfile[idxes[1]].get();
            var distanceOffset = idxes[0]-1 >= 0 ?
                 this.mParent.mDistances[idxes[0]-1] : 0;
            return {
                eleMts : dp.e,
                highwayFwd : dp.h,
                distanceMts : {
                    fromStart : distanceOffset + dp.distanceMts.fromStart
                }
            }
        }
    }

    MergedProfileIterator.prototype.current = function() {
        if (this.mProfileIdx != undefined && this.mDPIndex != undefined) {
            return this._value([this.mProfileIdx, this.mDPIndex]);
        }
    }

    MergedProfileIterator.prototype.peekNext = function(fromIdxs) {
        fromIdxs = fromIdxs || [this.mProfileIdx, this.mDPIndex];
        var nextIdxs = this._nextIdx(fromIdxs);
        if (nextIdxs) {
            return this._value(nextIdxs);
        }
    }

    MergedProfileIterator.prototype.next = function() {
        var nextIdxs = this._nextIdx([this.mProfileIdx, this.mDPIndex]);
        if (nextIdxs) {
            this.mProfileIdx = nextIdxs[0];
            this.mDPIndex    = nextIdxs[1];
        } else {
            this.mProfileIdx = undefined;
            this.mDPIndex = undefined;
        }
        return this;
    }

    return {
        GeoRouteProfile: GeoRouteProfile,
        MergedProfile  : MergedProfile
    }

})();

module.exports = routeProfile;