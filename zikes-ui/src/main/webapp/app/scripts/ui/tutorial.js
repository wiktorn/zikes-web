/**
 * scripts/ui/tutorial.js
 *
 * This, given a GeoRouteProfile, can prepare and draw this profile on canvas
 */

'use strict';
var when      = require("when");
when.delay    = require("when/delay");
var utils     = require("../lib/utils.js");
var messages  = require("./messages.js");
var map       = require("./map.js");
var journeys  = require("./journeys.js").journeys;

var tutorial = (function() {

    var KeyEnter = 13;
    var KeyDown  = 40;

    function _shakeOnce(distance, duration) {
        var finished = when.defer();
        this.animate({"margin-left":(distance*-1)}, duration/4)
        .animate({"margin-left":distance}, duration/2)
        .animate({"margin-left":0}, duration/4, function() {
            finished.resolve();
        });
        return finished.promise;
    };

    function _shake(shakes, distance, duration) {
        var self = this;
        if (shakes == 0) {
            return;
        }
        return _shakeOnce.bind(this)(distance, duration/shakes)
        .then(function() {
            return _shake.bind(self)(shakes-1, distance - distance/shakes, duration - duration/shakes);
        });
    }

    function wait(cb, retries, delayMs) {
        var resource = cb();
        if (resource) {
            return when(resource);
        }
        retries = retries || 5;
        if (retries < 0) {
            throw Error("Timeout awaiting resource");
        }
        delayMs = delayMs || 200;
        return when.delay(delayMs).then(function() {
            return wait(cb, retries-1, delayMs*2);
        });
    }

    function $wait(jq) {
        return wait(function() {
            var ele = $(jq);
            if (ele.length > 0) {
                return ele;
            }
        })
    }

    function sendKeys(string, div, retries) {
        function recursive(string, div) {
            if (string.length > 0) {
                if (string.length < 8) {
                    google.maps.event.trigger( div[0], 'focus', {} );
                }
                var newContent = div.val()+string[0];
                div.val(newContent);
                return when.delay(40)
                .then(function() {
                    return recursive(string.slice(1), div)
                });
            }
        }
        div.val("");

        return recursive(string, div).then(function() {
             return wait(function() {
                var pacContainer = $(".pac-container")
                if (pacContainer.css("display") != "none") {
                    return pacContainer
                }
             });
        });
    }


    function rectCentre(rect) {
        return {
            top: rect.top + (rect.bottom-rect.top)/2,
            left: rect.left + (rect.right-rect.left)/2
        }
    }

    function geoSearch(query, mouseDiv) {
        var searchBox = $("#geo-search-box");
        searchBox.trigger("focus");

        return moveDiv(mouseDiv, searchBox)
        .then(function() {
            return sendKeys(query, searchBox);
        })
        .delay(500)
        .then(function() {
            simulateKeyEvent(KeyDown, searchBox[0]);
        })
        .delay(300)
        .then(function() {
            simulateKeyEvent(KeyEnter, searchBox[0]);
        })
        .delay(1000);
    }

    function moveDiv(div2Move, dst) {
        var KSpeedPixPMs = 0.7;
        var done = when.defer();
        if (!dst.left || !dst.top) {
            if (dst.x && dst.y) {
                dst = {
                    left : dst.x-KMouseHalfWidth,
                    top : dst.y
                }
            } else {
                dst = rectCentre(
                    dst[0].getBoundingClientRect()
                );
            }
        }
        var src = rectCentre(
            div2Move[0].getBoundingClientRect()
        );
        var distance = Math.sqrt(Math.pow(dst.left - src.left, 2) + Math.pow(dst.top - src.top, 2));
        var duration = KSpeedPixPMs*distance;
        div2Move.animate(dst, duration, function() {
            done.resolve();
        });
        return done.promise;
    }


    function moveRange(from, to, cb) {
        if (to.x && to.y) {
            to = {
                left : to.x,
                top  : to.y
            }
        }
        if (from.x && from.y) {
            from = {
                left : from.x,
                top  : from.y
            }
        }
        var dx = to.left - from.left;
        var dy = to.top  - from.top;
        var distance = Math.sqrt(
            Math.pow(dx,2) + Math.pow(dy,2)
        );
        if (!cb) {
            cb = function(x,y) { return new L.point(x,y); }
        }
        var result = [];
        if (Math.abs(dx) > Math.abs(dy)) {
            var slope = 1.0*dy/dx;
            result = utils.range(0, dx, KEveryPix, function(x) {
                return cb(from.left + x, from.top + x*slope);
            });
        } else {
            var slope = 1.0*dx/dy;
            result = utils.range(0, dy, KEveryPix, function(y) {
                return cb(from.left + y*slope, from.top + y)
            });
        }
        return result;
    }



    var KEveryPix = 5;
    function mousemove(mouseDiv, from, to, cb) {
        var events = moveRange(from, to);
        events.shift();//otherwise we're doubling up with down

        function fireEvents(_events) {
            if (_events.length > 0) {
                var event = _events.shift();
                cb(event);
                mouseDiv.css("left", event.x-KMouseHalfWidth);
                mouseDiv.css("top", event.y);
                return when.delay(8).then(function() {
                    return fireEvents(_events);
                })
            }
            return when(true);
        }
        return fireEvents(events);
    }


    function simulateKeyEvent(code, div) {
        var evt = document.createEvent('Event');
        evt.initEvent('keydown', true, true);
        evt.keyCode = code;
        div.dispatchEvent(evt);
    }

    /**
     * pageX and pageY:
     * Relative to the top left of the fully rendered content area in the browser. This reference point is below the url bar and back
     * button in the upper left. This point could be anywhere in the browser window and can actually change location if there are
     * embedded scrollable pages embedded within pages and the user moves a scrollbar.
     *
     * screenX and screenY:
     * Relative to the top left of the physical screen/monitor, this reference point only moves if you increase or decrease the number
     * of monitors or the monitor resolution.
     *
     * clientX and clientY:
     * Relative to the upper left edge of the content area (the viewport) of the browser window. This point does not move even if
     * the user moves a scrollbar from within the browser.
     */
    function makeMouseEvent(eventType, clientPoint) {
        return new MouseEvent(
            eventType, {
                clientX : clientPoint.x,
                clientY : clientPoint.y,
                pageX : clientPoint.x,
                pageY : clientPoint.y,
                bubbles : true,
                view: window
            }
        );
    }


    var KMouseWithHeight = 32;
    var KMouseWithHeightPressed = KMouseWithHeight-7;
    var KMouseHalfWidth = KMouseWithHeight/2;
    function containerToClient(containerPoint) {
        return new L.point(
            containerPoint.x + map.rect().left,
            containerPoint.y + map.rect().top
        );
    }
    var mapCentre = L.point(
        map.rect().left + (map.rect().right - map.rect().left)/2,
        map.rect().top + (map.rect().bottom - map.rect().top)/2
    )

    function BasicTutorial() {
        this.mMouseDiv = '<img id="tutorialMouseCursor" src="graphics/hand_icon.png" style="width:'+KMouseWithHeight+'px;height:'+KMouseWithHeight+'px;position:absolute;left:'+mapCentre.x+'px;top:'+mapCentre.y+'px;z-index:401"/>';
    }

    var KFrom     = "Camden Arts Centre";
    var KTo       = "Gospel Oak Station";
    var KDragVia  = new L.LatLng(51.5473775729,-0.159696517181);
    var KToLatLng = new L.LatLng(51.5549990907,-0.151065168571);

    BasicTutorial.prototype.run = function() {
        var self = this;

        when.delay(500)
        .then(function() {
            map.busy(true);
            return messages.popup(
                 "<span class=\"lead\"><b>Zikes</b> is for planning bicycle journeys.<br></span>"
                 + "<span>Journeys have legs and points of interests - say - to remind you where you might want to stop for lunch.</span><br>"
                 + "<span class=\"lead\">Let's plot a basic route, a leg in a journey. It will take less than a minute.</span><br>",
                 { closeLabel : "Continue" }
            ).enqueue()
             .whenHidden()
        })
        .then(function() {
            $("body").append(self.mMouseDiv);
            self.mMouseDiv = $("#tutorialMouseCursor");
            return _shake.call(self.mMouseDiv, 10, 20, 1500)
        })
        .then(function() {
            return geoSearch(KFrom, self.mMouseDiv);
        })
        .then(function() {
            return self.click($wait(".leaflet-popup-content #from a"));
        })
        .then(function() {
            return geoSearch(KTo, self.mMouseDiv)
        })
        .then(function() {
            return messages.popup(
                 "<span class=\"lead\">We don't want to actually roll into the station. Just next to it will do.<br></span>",
                 {timeoutSec: 4, indismissable: true}
            ).enqueue()
             .whenShown()
        })
        .delay(500)
        .then(function() {
            var containerPointToClick;
            var oldContextMenu;
            return when($wait(".leaflet-popup-content #from"))
            .delay(500)
            .then(function() {
                oldContextMenu = $(".leaflet-popup-content #from")[0];
                containerPointToClick = map.leaflet.latLngToContainerPoint(KToLatLng);
                return moveDiv(self.mMouseDiv, containerToClient(containerPointToClick))
            })
            .then(function() {return animateMouseDown(self.mMouseDiv);})
            .then(function() {return animateMouseUp(self.mMouseDiv);})
            .then(function() {
                return  map.leaflet.fireEvent("click", {
                  latlng: KToLatLng,
                  layerPoint: map.leaflet.latLngToLayerPoint(KToLatLng),
                  containerPoint: map.leaflet.latLngToContainerPoint(KToLatLng)
                });
            })
            .then(function() {
                return wait(function() {
                    var newContextMenu = $(".leaflet-popup-content #from");
                    if (newContextMenu.length == 1) {
                        newContextMenu = newContextMenu[0];
                        if (oldContextMenu != newContextMenu) {
                            return true
                        }
                    }
                });
            })
            .then(function() {
                return self.click($wait(".leaflet-popup-content #to a"));
            })
            .delay(300);
        })
        .then(function() {
            return wait(function() {
                if (journeys.current().mItems.length > 0) {
                    self.mRoute = journeys.current().mItems[0];
                    return self.mRoute;
                }
            }).delay(1000)
        })
        .then(function() {
            self.mMouseDiv.addClass("hidden");
            var elevationMessage = messages.q("popup").find(function(m) {
                return m.mOptions.once == "elevation"
            });
            if (elevationMessage) {
                return elevationMessage.whenHidden()
                .then(function() {
                    return messages.popup(
                        '<span class="lead">But what if we didn\'t like rugged routes?</span>',
                        {timeoutSec: 3, indismissable: true}
                    ).enqueue();
                });
            } else {
                var KArkwrightSteepLink="https://www.google.co.uk/maps/@51.5508812,-0.18347,3a,75y,10.86h,83.36t/data=!3m6!1e1!3m4!1scVotCYrfvVYoWlRhop2Igw!2e0!7i13312!8i6656";
                return messages.popup(
                     '<span class="lead">We have the first route.<br></span>'
                     +'<span>For those who know or who just eyed its elevation profile, the route sends us climbing the <a href="'+KArkwrightSteepLink+'" class="zikesAnchor" target="_blank">relentlessly steep Arkwright Rd.</a><br></span>'
                     +'<span class="lead">But what if we didn\'t like climbing?</span>',
                     { closeLabel : "Continue" }
                ).enqueue()
                 .whenHidden();
             }
        })
        .delay(1000)
        .then(function() {
            self.mMouseDiv.removeClass("hidden");
            var prevDistanceMts = self.mRoute.distanceMts();
            return self.move(
                $("#toolsAccordion #climb .slider-handle"),
                {left: -30, top: 0}
            ).then(function() {
                return prevDistanceMts;
            })
        })
        .then(function(prevDistanceMts) {
            return wait(function() {
                return self.mRoute.distanceMts() != prevDistanceMts;
            });
        })
        .then(function() {
            self.mRoute.centre();
        })
        .delay(1000)
        .then(function() {
            return messages.popup(
                 '<span class="lead">What if we didn\'t like going up Belsize Lane either?<br></span>',
                 {timeoutSec: 4, indismissable: true}
            ).enqueue()
        })
        .delay(2000)
        .then(function() { return self.dragPath(KDragVia);})
        .then(function() {
            var finalMessage = messages.popup(
                 '<span class="lead">Well, that\'s it. As we continue adding to this tutorial, it should get you started.<br></span>'
                 +'<span>Play with the other sliders. Drag the route around. Inspect it, scrutinise it by '
                 +'<a id="streetViewTutorial" href="#" class="zikesAnchor">riding it virtually</a>.<br></span>'
                 +'<span>Once satisfied, save it. Name it your way if you like, the headings are all editable. '
                 +'Once saved, fasten your phone (or garmin device) to your bike and <a href="https://sites.google.com/site/andrdzikes/" class="zikesAnchor" target=\"_blank\">ride it for real</a>.<br></span>'
                 +'You can run this tutorial again by choosing <b>About->Tutorial</b> from the top navigation bar menu.</span>'
            );
            finalMessage.enqueue()
             .whenShown()
             .then(function() {
                $("#streetViewTutorial").click(function() {
                    when(self.mViaMarker.fireEvent("click", {
                      latlng: self.mViaMarker.getLatLng(),
                      layerPoint: map.leaflet.latLngToLayerPoint(self.mViaMarker.getLatLng()),
                      containerPoint: map.leaflet.latLngToContainerPoint(self.mViaMarker.getLatLng())
                    }))
                    .delay(300)
                    .then(function() {
                        return self.click($(".leaflet-popup-content #streetView a"));
                    });
                });
            });
            return finalMessage.whenHidden();
        })
        .otherwise(function(e) {
            console.error(e);
            return messages.popup(
                 '<span class="lead">Ouh bugger. This tutorial has just tumbled.<br></span>'
                 +'<span>..as it normally doesn\'t. It is not a recording, it is a relativelly delicate simulation of user actions, which relies on responsive, external resources and it has not been easy to write.<br></span>'
                 +'<spam>We\'re nonetheless embarassed. You can run it again, if you care, by choosing <b>About->Tutorial</b> from the top navigation bar menu.</span>')
            .enqueue();
        })
        .finally(function() {
            self.mMouseDiv.remove();
            map.busy(false);
        });
    }

    function animateMouseDown(div) {
        var done = when.defer()
        div.animate({width: KMouseWithHeightPressed, height: KMouseWithHeightPressed}, 200, function() {
            done.resolve();
        })
        return done.promise;
    }

    function animateMouseUp(div) {
        var done = when.defer()
        div.animate({width: KMouseWithHeight, height: KMouseWithHeight}, 200, function() {
            done.resolve();
        })
        return done.promise;
    }



    BasicTutorial.prototype.click = function(div) {
        var self = this;
        return when(div)
        .then(function(_div) {
            div = _div;
            return moveDiv(self.mMouseDiv, div);
        })
        .delay(800)
        .then(function() {return animateMouseDown(self.mMouseDiv);})
        .then(function() {return animateMouseUp(self.mMouseDiv);})
        .then(function() {
            div.trigger("click");
        })
        .delay(500)
    }

    BasicTutorial.prototype.move = function(div, offset) {
        var self = this;
        return moveDiv(this.mMouseDiv, div)
        .delay(300)
        .then(function() {return animateMouseDown(self.mMouseDiv);})
        .then(function() {
            div.trigger("mousedown");
            var centre = rectCentre(
                div[0].getBoundingClientRect()
            );
            return mousemove(
                self.mMouseDiv,
                centre, {
                    left: centre.left+offset.left,
                    top: centre.top+offset.top
                }, function(event) {
                    var e = $.Event('mousemove');
                    e.pageX = event.x;
                    e.pageY = event.y;
                    div.trigger(e);
                }
            );
        })
        .then(function() {return animateMouseUp(self.mMouseDiv);})
        .then(function() {div.trigger("mouseup");})
    }


    BasicTutorial.prototype.dragPath = function(dstLatLng) {
        var self = this;
        var section = self.mRoute.mRouteOnMap.mHead;
        var midIdx  = Math.round(section.mWaypoints.length/2);
        var srcLatlngPoint = section.mWaypoints[midIdx];
        var mapRect = $("#map")[0].getBoundingClientRect();
        function containerToClient(containerCoords) {
            return new L.point(
                containerCoords.x + mapRect.left,
                containerCoords.y + mapRect.top
            )
        }

        var dragFrom  = containerToClient(
            map.leaflet.latLngToContainerPoint(srcLatlngPoint)
        );
        var dragTo    = containerToClient(
            map.leaflet.latLngToContainerPoint(dstLatLng)
        );
        map.leaflet.on("layeradd", function (layer) {
            if (layer.layer._icon && layer.layer._icon.classList.contains("toFromIcon")) {
                self.mViaMarker = layer.layer;
            }
        });

        return moveDiv(this.mMouseDiv, dragFrom)
        .then(function() {
            section.mDrawnRoute.fireEvent("mouseover", {
              latlng: srcLatlngPoint,
              layerPoint: map.leaflet.latLngToLayerPoint(srcLatlngPoint),
              containerPoint: map.leaflet.latLngToContainerPoint(srcLatlngPoint)
            });
        })
        .delay(400)
        .then(function() {return animateMouseDown(self.mMouseDiv);})
        .then(function() {
            self.mViaMarker._icon.dispatchEvent(
                makeMouseEvent("mousedown", dragFrom)
            );
            return mousemove(
                self.mMouseDiv,
                dragFrom,
                dragTo,
                function(point) {
                    self.mViaMarker._icon.dispatchEvent(
                        makeMouseEvent("mousemove", point)
                    );
                }
            );
        }).delay(300)
        .then(function() {return animateMouseUp(self.mMouseDiv);})
        .then(function() {
            self.mViaMarker._icon.dispatchEvent(
                makeMouseEvent("mouseup", dragTo)
            );
        }).delay(1000);
    }

    return {
        basicTutorial : function() {
            new BasicTutorial().run();
        }
    }

})();

module.exports = tutorial;