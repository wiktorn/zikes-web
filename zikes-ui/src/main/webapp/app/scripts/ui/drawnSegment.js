/**
 * scripts/ui/drawnSegment.js
 */

'use strict';

var chroma   = require("chroma-js");
var geometry = require("../lib/geometry.js");
var utils    = require("../lib/utils.js");

var drawn = (function() {

    function createGradient(svg,id,stops){
      var svgNS = svg.namespaceURI;
      var grad  = document.createElementNS(svgNS,'radialGradient');
      grad.setAttribute('id',id);
      for (var i=0;i<stops.length;i++){
        var attrs = stops[i];
        var stop = document.createElementNS(svgNS,'stop');
        for (var attr in attrs){
          if (attrs.hasOwnProperty(attr)) stop.setAttribute(attr,attrs[attr]);
        }
        grad.appendChild(stop);
      }

      var defs = svg.querySelector('defs') ||
          svg.insertBefore(document.createElementNS(svgNS,'defs'), svg.firstChild);
      return defs.appendChild(grad);
    }

    function createGradients(thresholds, id) {
        var prevColor = undefined;
        for (var i = 0; i < thresholds.lenght; i++) {
            if (prevColor) {
                createGradient($('svg')[0],""+id+"_"+threshold[0],[
                    {offset:'0%',  'stop-color':prevColor},
                    {offset:'100%','stop-color':threshold[1]}
                ]);
            }
            prevColor = threshold[1];
        }
    }

    function preparePolylines(waypoints, style) {
        if (!style.schema) {
            return [
                new L.Polyline(waypoints, {
                    weight: style.weight,
                    opacity: style.opacity,
                    smoothFactor: style.smoothFactor,
                    lineCap : style.lineCap,
                    dashArray : style.dashArray,
                    color : style.color,
                    waypointsIdx : {from : 0, to: waypoints.length-1}
                })
            ];
        }
        var result = [];

        function poly(toIdx, color, prevPoly) {
            var waypointsIdx = {
                from: prevPoly ? prevPoly.toIdx : 0,
                to  : toIdx
            }
            var result = new L.Polyline(
                waypoints.slice(waypointsIdx.from, waypointsIdx.to+1), {
                    weight: style.weight,
                    opacity: style.opacity,
                    smoothFactor: style.smoothFactor,
                    lineCap : style.lineCap,
                    dashArray : style.dashArray,
                    color : color,
                    waypointsIdx : waypointsIdx
                }
            );
            result.toIdx = toIdx;
            result.color = color;
            result.expand = function(toIdx) {
                result.setLatLngs(
                    result.getLatLngs().concat(
                        waypoints.slice(result.toIdx+1, toIdx+1)
                    )
                )
                result.toIdx = toIdx;
            }
            return result;
        }

        function newDatapoint(datapoint, idx) {
            if (datapoint == undefined) {
                return;
            }
            var color = (style.schema.thresholds.find(function(th) {
                return datapoint <= th[0];
            }) || style.schema.thresholds[style.schema.thresholds.length-1])[1];
            if (result.length > 0) {
                var prev = result[result.length-1];
                if (prev.color == color) {
                    prev.expand(idx);
                } else {
                    result.push(
                        poly(idx, color, prev)
                    );
                }
            } else {
                result.push(
                    poly(idx, color)
                );
            }
        }

        var datapoint = undefined;
        for (var i = 0; i < waypoints.length; i++) {
            datapoint = style.schema.datapoint.reduce(function(cum, e) {
                return cum ? cum[e] : undefined
            }, waypoints[i])
            newDatapoint(datapoint,i);
        }
        if (datapoint == undefined) {
            var previous = result[result.length-1];
            result.push(
                poly(
                    waypoints.length-1,
                    previous ? previous.options.color : style.color,
                    previous
                )
            );
        }
        return result;
    }

    /**
    style : {
        weight: 6,
        opacity: 0.5,
        lineCap : "butt",
        smoothFactor: 3,
        color : "#0003F0",
        schema: {
            reach      : "spot",
            datapoint  : ["decoration", "e"],
            thresholds : drawnSegment.eleThresholds(
                {ele: profile.minEleMts(), color: "#00FF00"},
                {ele: profile.maxEleMts(), color: "#FF0000"},
                6
            )
        },
        horizonIcons : {
            from   : fromIcon,
            to     : toIcon
        }
    }
    */
    function Polyline(waypoints, map, style) {
        this.mStyle = style;
        this.mMap = map;
        this.mDirectionMarker = new DirectionMarker(this);
        this.mPolys = preparePolylines(
            waypoints, style
        );
    }

    function resetDirectionMarker() {
        this.mDirectionMarker.invalidate();
    }

    function clearDirectionMarker() {
        this.mDirectionMarker.clear();
    }

    Polyline.prototype.show = function() {
        var self = this;
        this.mPolys.forEach(function(poly) {
            poly.addTo(self.mMap);
        });

        this.mMap.on("moveend", resetDirectionMarker, this);
        this.mMap.on("viewreset", resetDirectionMarker, this);
        this.mMap.on("movestart", clearDirectionMarker, this);
        return this;
    }

    function projectPix(poly, pixPoint) {
        function projectOnto(points, nearestSegment, idxStart, idxEnd) {
            nearestSegment = nearestSegment || { pix : {_distanceSqrd:Number.MAX_VALUE}};
            idxStart = idxStart || 0;
            idxEnd   = idxEnd || points.length-1;
            for (var i = idxStart+1; i <= idxEnd; i++) {
                var segment = new geometry.PixLine(points[i-1], points[i]);
                var projection = segment.projectOnto(pixPoint, true);
                var distance   = new geometry.PixLine(projection, pixPoint).lengthSqrd();
                if (distance < nearestSegment.pix._distanceSqrd) {
                    nearestSegment = {
                        pix : {
                            projection : projection,
                            segment    : segment,
                            _distanceSqrd : distance
                        },
                        idx : {
                            start : i-1,
                            end   : i
                        }
                    };
                }
            }
            return nearestSegment;
        }

        /*
         * _rings is an array of projected geographical points (screen coordinates of the polygon points),
         * and _parts is an array of arrays of points that eventually gets rendered after clipping and simplifying points.
         */
        var rings = poly._rings[0];
        var parts = poly._parts;
        var nearestSegment = undefined;
        for (var j = 0; j < parts.length; j++) {
            nearestSegment = projectOnto(parts[j], nearestSegment);
        }

        //nearestSegment.projection is the sought after projection onto 'this', but it's been found scanning _parts
        //and thus the underlying subsegment needs to be found with rings between nearestSegment.pixStart
        //and nearestSegment.pixEnd
        if (nearestSegment) {
            var startIdx;
            for (var i = 0; i < rings.length; i++) {
                if (nearestSegment.pix.segment.mStart.equals(rings[i])) {
                    var endIdx = i+1;
                    var nearestEdgeSegment = undefined;
                    while (endIdx < rings.length && !nearestSegment.pix.segment.mEnd.equals(rings[endIdx-1])) {
                        nearestEdgeSegment = projectOnto(rings, nearestEdgeSegment, endIdx-1, endIdx);
                        endIdx++;
                    }
                    nearestSegment = nearestEdgeSegment
                    break;
                }
            }
            if (!nearestSegment) {
                return;
            }
            nearestSegment.geo = {
                segment : new geometry.GeoLine(
                    poly.getLatLngs()[nearestSegment.idx.start],
                    poly.getLatLngs()[nearestSegment.idx.end]
                )
            }
            if (nearestSegment.pix.segment.mStart === nearestSegment.pix.projection) {
                nearestSegment.geo.projection = poly.getLatLngs()[nearestSegment.idx.start];
                nearestSegment.geo.projectionSameAsIdx = nearestSegment.idx.start;
            } else if (nearestSegment.pix.segment.mEnd === nearestSegment.pix.projection) {
                nearestSegment.geo.projection = poly.getLatLngs()[nearestSegment.idx.end];
                nearestSegment.geo.projectionSameAsIdx = nearestSegment.idx.end;
            } else {
                var ratio = new geometry.PixLine(
                    nearestSegment.pix.segment.mStart, nearestSegment.pix.projection
                ).length()/nearestSegment.pix.segment.length();
                nearestSegment.geo.projection = nearestSegment.geo.segment.offsetRatio(ratio);
            }
            nearestSegment.poly = poly;
            nearestSegment.pix.distance = new geometry.PixLine(nearestSegment.pix.projection, pixPoint).length();
        }
        return nearestSegment;
    }

    Polyline.prototype.on = function(eventType, cb) {
        var overridenCb = cb;
        if (eventType == "mouseover") {
            overridenCb = function(e) {
                e.nearest = projectPix(e.target, e.layerPoint);
                e.nearest.idx = {
                    start : e.nearest.idx.start + e.nearest.poly.options.waypointsIdx.from,
                    end   : e.nearest.idx.end   + e.nearest.poly.options.waypointsIdx.from,
                }
                e.nearest.idx.count = e.nearest.idx.end - e.nearest.idx.start;
                cb(e);
            }
        }
        var self = this;
        this.mPolys.forEach(function(poly) {
            poly.on(eventType, overridenCb);
        });
    }

    Polyline.prototype.fireEvent = function(ev, options) {
        var nearestDistance = undefined;
        this.mPolys.reduce(function(nearestPoly, poly) {
            var nearestLayerPoint = poly.closestLayerPoint(options.layerPoint);
            if (!nearestDistance || (nearestLayerPoint && nearestLayerPoint.distance < nearestDistance)) {
                nearestDistance = nearestLayerPoint.distance;
                return poly;
            }
            return nearestPoly;
        }).fireEvent(ev, options);
    }

    Polyline.prototype.clear = function() {
        var self = this;
        this.mPolys.forEach(function(poly) {
            self.mMap.removeLayer(poly);
        });
        this.mDirectionMarker.clear();
        this.mMap.off("moveend",   resetDirectionMarker, this);
        this.mMap.off("viewreset", resetDirectionMarker, this);
        this.mMap.off("movestart", clearDirectionMarker, this);
        return this;
    }


    Polyline.prototype.bringToFront = function() {
        this.mPolys.forEach(function(poly) {
            poly.bringToFront();
        });
        return this;
    }

    Polyline.prototype.getBounds = function() {
        return this.mPolys.reduce(function(bounds, poly) {
            return bounds.extend(poly.getBounds());
        }, new L.latLngBounds());
    }

    Polyline.prototype.highlight = function(highlightStyle) {
        this.mPolys.forEach(function(poly) {
            if (highlightStyle) {
                if (poly.prevStyle) {
                    return;
                }
                poly.prevStyle   = utils.cloneSimpleObject(poly.options);
                var newStyle     = utils.cloneSimpleObject(poly.options);
                if (highlightStyle.color) {
                    newStyle.color = chroma.mix(newStyle.color, highlightStyle.color, 0.7).hex()
                }
                newStyle.weight  = highlightStyle.weight  || newStyle.weight;
                newStyle.opacity = highlightStyle.opacity || newStyle.opacity;

                poly.setStyle(newStyle);
            } else if (poly.prevStyle) {
                poly.setStyle(poly.prevStyle);
                delete poly["prevStyle"];
            }
        });
    }

    Polyline.prototype.setStyle = function(style) {
        this.mPolys.forEach(function(poly) {
            utils.mergeSimpleObjects(poly.options, style);
            poly.setStyle(poly.options);
        });
    }





    function DirectionMarker(forPolyLine) {
        this.mPolyLine  = forPolyLine;
        this.mMap       = this.mPolyLine.mMap;
        this.mMarkers   = [];
    }

    DirectionMarker.prototype.clear = function() {
        var self = this;
        this.mMarkers = this.mMarkers.reduce(function(cum, marker) {
            self.mMap.removeLayer(marker); return cum;
        }, []);
    }

    var KOffsetFromEdgePix = 20;

    DirectionMarker.prototype.invalidate = function() {
        var self = this;
        var mapContainerPixBounds = this.mMap.getContainer().getBoundingClientRect();
        var mapContainerPixBounds = L.bounds(
            L.point(0,0),
            L.point(mapContainerPixBounds.width, mapContainerPixBounds.height)
        );
        var mapLayerPixBounds = L.bounds(
            self.mMap.containerPointToLayerPoint(mapContainerPixBounds.min),
            self.mMap.containerPointToLayerPoint(mapContainerPixBounds.max)
        )
        this.clear();
        var lastPolyLatLngs = this.mPolyLine.mPolys[this.mPolyLine.mPolys.length-1].getLatLngs();
        var toFromVisibility   = [
            mapContainerPixBounds.contains(self.mMap.latLngToContainerPoint(this.mPolyLine.mPolys[0].getLatLngs()[0])),
            mapContainerPixBounds.contains(self.mMap.latLngToContainerPoint(lastPolyLatLngs[lastPolyLatLngs.length-1]))
        ];

        function offsetFor(intersection, offset, segment) {
            if (offset) {
                return intersection+offset;
            }
            return intersection + Math.sign(intersection - segment) * KOffsetFromEdgePix;
        }

        function show(vanishingSegment, icon) {
            var closerLeft = Math.abs(vanishingSegment.mStart.x - mapContainerPixBounds.min.x) < Math.abs(vanishingSegment.mStart.x - mapContainerPixBounds.max.x);
            var closerTop  = Math.abs(vanishingSegment.mStart.y - mapContainerPixBounds.min.y) < Math.abs(vanishingSegment.mStart.y - mapContainerPixBounds.max.y);
            var closestEdges = [
                closerLeft ? {
                    edge : new geometry.PixLine(
                        mapContainerPixBounds.min,
                        L.point(mapContainerPixBounds.min.x, mapContainerPixBounds.max.y)
                    ), ox : KOffsetFromEdgePix}
                    :
                    {edge : new geometry.PixLine(
                        L.point(mapContainerPixBounds.max.x, mapContainerPixBounds.min.y),
                        mapContainerPixBounds.max
                    ), ox : - KOffsetFromEdgePix },
                closerTop ?
                    {edge : new geometry.PixLine(
                        mapContainerPixBounds.min,
                        L.point(mapContainerPixBounds.max.x, mapContainerPixBounds.min.y)
                    ), oy : KOffsetFromEdgePix }
                    :
                    {edge : new geometry.PixLine(
                        L.point(mapContainerPixBounds.min.x, mapContainerPixBounds.max.y),
                        mapContainerPixBounds.max
                    ), oy : - KOffsetFromEdgePix}
            ];
            var edgeIntersection = undefined;
            closestEdges.every(function(edge) {
                edgeIntersection = edge.edge.intersect(vanishingSegment);
                if (edgeIntersection) {
                    edgeIntersection.x = offsetFor(edgeIntersection.x, edge.ox, vanishingSegment.mEnd.x);
                    edgeIntersection.y = offsetFor(edgeIntersection.y, edge.oy, vanishingSegment.mEnd.y);
                    return;
                }
                return true;
            });
            if (edgeIntersection) {
                var marker = L.marker(
                    self.mMap.containerPointToLatLng(edgeIntersection), {
                    icon      : icon,
                    opacity   : 0.4,
                    clickable : false,
                    draggable : false
                }).addTo(self.mMap);
                $(marker._icon).removeClass("leaflet-interactive");
                self.mMarkers.push(marker);
            }
        }

        if (!(toFromVisibility[0] && toFromVisibility[1])) {
            var firstVisibleSegment = undefined;
            var lastVisibleSegment  = undefined;
            this.mPolyLine.mPolys.forEach(function(poly) {
                poly._parts.forEach(function(part) {
                    for (var i = 1; i < part.length; i++) {
                        if (mapLayerPixBounds.contains(part[i])) {
                            if (!firstVisibleSegment) {
                                firstVisibleSegment = new geometry.PixLine(
                                    self.mMap.layerPointToContainerPoint(part[i-1]),
                                    self.mMap.layerPointToContainerPoint(part[i])
                                );
                            }
                        } else if (firstVisibleSegment && !lastVisibleSegment) {
                            lastVisibleSegment = new geometry.PixLine(
                                self.mMap.layerPointToContainerPoint(part[i-1]),
                                self.mMap.layerPointToContainerPoint(part[i])
                            ).flip();
                        }
                    }
                });
            });

            if (!(toFromVisibility[0] || toFromVisibility[1])) {
                toFromVisibility[0] || firstVisibleSegment && show(firstVisibleSegment, this.mPolyLine.mStyle.horizonIcons.from);
                toFromVisibility[1] || lastVisibleSegment && show(lastVisibleSegment, this.mPolyLine.mStyle.horizonIcons.to);
            }
        }
    }

    function thresholds42(min, max, steps) {
        var step = (max.value - min.value)/(steps-1);
        var result = [];
        for (var i = 0; i < steps-1; i++) {
            result.push([
                min.value + i*step,
                chroma.mix(min.color, max.color, 1.0*i/steps).hex()
            ]);
        }
        result.push([max.value, max.color]);
        return result;
    }

    return {
        Polyline: Polyline,
        thresholds : function(stops, resultion) {
            var result = [];
            var resultion42 = Math.round(resultion/(stops.length-1))+1;
            for (var i = 1; i < stops.length; i++) {
                if (result.length > 0) {
                    result.pop(); //otherwise they will repeat between stops
                }
                result = result.concat(
                    thresholds42(stops[i-1], stops[i], resultion42)
                );
            }
            return result;
        }
    }

})();

module.exports = drawn;