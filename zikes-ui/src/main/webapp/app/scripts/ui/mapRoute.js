/**
 * scripts/ui/mapRoute.js
 *
 * This draws the route on the map and takes care of the interactions
 * with it.
 */

'use strict';

var when         = require("when");

var routeProfile = require("../lib/routeProfile.js");
var map          = require("./map.js");
var ctxMenu      = require("./contextMenu.js");
var sectionMod   = require("./section.js");
var drawnSegment = require("./drawnSegment.js");

var route = (function() {

    var naviIconSize = [23, 23];
    var toIcon = L.divIcon({
        className: "toFromIcon",
        html: '<div class="glyphicon glyphicon-log-in"/>',
        iconSize: naviIconSize
    });
    var fromIcon = L.divIcon({
        className: "toFromIcon",
        html: '<div class="glyphicon glyphicon-log-out"/>',
        iconSize: naviIconSize
    });
    var viaIcon = L.divIcon({
        className: "toFromIcon",
        iconSize: [15,15]
    });

    function geoComparableDistance(from, to) {
        return Math.pow(from.lat - to.lat, 2) + Math.pow(from.lng - to.lng, 2);
    }


    function Route(parent) {
        this.mHead          = undefined;
        this.mHasFrom       = false;
        this.mParent        = parent;
        this.mIcons         = {
            to   : toIcon,
            from : fromIcon,
            via  : viaIcon
        }
    }

    Route.prototype.invalidate = function(options) {
        options = options || {};
        var self = this;
        var tail = this.mHead;
        var head = undefined;
        var routeRequestCoordinates = [];
        var routesPrepared  = [];

        while (tail) {
            //Some sections may need (re)routing (they were dragged) and if they do,
            //we need to keep addting their new coordinates to routeRequestCoordinates
            //for as long as we encounter stale nodes and thus compose a single
            //consistent from,[via...],to request.
            if (tail.needsRouting(options.reroute)) {
                if (!head) {
                    //remember the first section (which will receive the route request result)
                    head = tail;
                }
                routeRequestCoordinates.push([tail.getLatLng().lat, tail.getLatLng().lng]);
            } else if (routeRequestCoordinates.length) {
                //If the stale nodes are interleaved with not stale ones, we need to issue
                //more than one routing request (for detached sections).
                routeRequestCoordinates.push([tail.getLatLng().lat, tail.getLatLng().lng]);
                routesPrepared.push(
                    head.prepareRoute(this.mParent.route(routeRequestCoordinates), tail)
                );
                routeRequestCoordinates = [];
                head = undefined;
            }
            tail = tail.mNext;
        }
        //and the last request
        if (routeRequestCoordinates.length > 1) {
            routesPrepared.push(
                head.prepareRoute(this.mParent.route(routeRequestCoordinates), this.mHead.tail())
            );
        }

        return when.all(routesPrepared)
        .then(function() {
            return when.all(
                self.mapSections(function(section) {
                    return section.invalidate(options.redraw);
                })
            );
        })
        .then(function() {
            var shownElevation = self.showsElevation();
            self.style(true);
            var forceRedraw = self.showsElevation() ? !shownElevation : shownElevation;
            self.mapSections(function(section) {
                return section.draw(forceRedraw);
            })
            return self.mParent.invalidate();
        });
    };

    Route.prototype.style = function(regenerate) {
        if (!this.mStyle || regenerate) {
            var profile     = this.getProfile();
            var parentStyle = this.mParent.style();
            this.mStyle = {
                weight: 5,
                opacity: 0.7,
                smoothFactor: 3,
                lineCap: "butt",
                color : parentStyle.color.base,
                horizonIcons : {
                    from   : fromIcon,
                    to     : toIcon
                }
            };
            if (parentStyle.showElevation && profile.isRugged()) {
                this.mStyle.schema = {
                    reach      : "spot",
                    datapoint  : ["decoration", "e"],
                    thresholds : drawnSegment.thresholds(
                        [{value: profile.minEleMts(), color: parentStyle.color.base},
                         {value: profile.maxEleMts(), color: parentStyle.color.max}],
                        7
                    )
                }
            }
        }
        return this.mStyle;
    };

    Route.prototype.showsElevation = function() {
        return this.mStyle && this.mStyle.schema;
    }

    Route.prototype.head = function(newHead) {
        if (!newHead) {
            //getter
            return this.mHead;
        }
        //the scenario for this setter is now only one.
        //the former 'from' has been removed by user
        //action. If this changes, maybe the conclussion
        //below will not be correct
        this.mHasFrom = false;
        this.mHead = newHead;
        return this.mHead;
    };

    /**
     * Setter and getter for this.from.
     * @latlng  - array of floats carrying the new coordinates for this.from.
     *            when undefined, the method behaves as a getter.
     * @returns - (setter) true - if the given latlng differs from current (false otherwise)
     *            (getter) head Section object (if exists)
     */
    Route.prototype.from = function(latlng) {
        if (!latlng) {
            //getter
            if (this.mHead) {
                if (this.mHead != this.mHead.tail() || this.mHasFrom) {
                    return this.mHead;
                }
            }
            return undefined;
        }
        //setter...
        var currentFrom = this.from();
        if (currentFrom) {
            if (currentFrom.getLatLng() == latlng) {
                return false;
            }
            this.mHead.setLatLng(latlng);
        }
        else {
            this.mHead = new sectionMod.Section({
                next     : this.mHead,
                icon     : fromIcon,
                latlng   : latlng,
                parent   : this
            });
            if (this.mHead.mNext) {
                this.mHead.mNext.mPrev = this.mHead;
            }
        }
        this.mHasFrom = true;
        return true;
    };

    /**
     * Setter and getter for this.to.
     * @latlng  - array of floats carrying the new coordinates for this.to.
     *            when undefined, the method behaves as a getter.
     * @returns - (setter) true - if the given latlng differs from current (false otherwise)
     *            (getter) tail Section object (if exists)
     */
    Route.prototype.to = function(latlng)
    {
        if (!latlng) {
            //getter
            if (this.mHead) {
                if (this.mHead != this.mHead.tail() || !this.mHasFrom) {
                    return this.mHead.tail();
                }
            }
            return undefined;
        }
        //setter...
        var currentTo = this.to();
        if (currentTo) {
            if (currentTo.getLatLng() == latlng) {
                return false;
            }
            this.mHead.tail().setLatLng(latlng);
        } else {
            var to = new sectionMod.Section({
                icon     : toIcon,
                latlng   : latlng,
                parent   : this
            });
            if (!this.mHead) {
                this.mHead = to;
            } else {
                to.mPrev = this.mHead.tail();
                this.mHead.tail().mNext = to;
            }
        }
        return true;
    };

    Route.prototype.isPending = function() {
        return !(this.mHead && this.mHead.mNext);
    };

    Route.prototype.isUnititialised = function() {
        return !this.mHead;
    };

    Route.prototype.otherRoutes = function(cb) {
        return this.mParent.otherJoureyItems(cb);
    };

    Route.prototype.toJSON = function() {
        return this.mapSections(function(section) {
            return section.toJSON();
        });
    };

    Route.prototype.fromJSON = function(sectionsJSON) {
        this.delete();
        for (var i = 0; i < sectionsJSON.length; i++) {
            var icon = viaIcon;
            if (!this.mHead) {
                icon = fromIcon;
            } else if (i == sectionsJSON.length-1) {
                icon = toIcon;
            }
            var ctorParams = {
                icon     : icon,
                latlng   : sectionsJSON[i].from,
                parent   : this
            };
            var section = new sectionMod[sectionsJSON[i].ctor || "Section"](ctorParams);
            if (!this.mHead) {
                this.mHead = section
            } else {
                section.mPrev = this.mHead.tail();
                section.mPrev.mNext = section;
                section.mPrev.fromJSON(sectionsJSON[i-1].waypoints);
            }
        }
        return this.invalidate();
    };

    Route.prototype.distanceMts = function() {
        var result = 0;
        this.mapSections(function(section) {
            result += section.distanceMts();
        });
        return result | 0;
    };

    Route.prototype.getProfile = function() {
        var result  = new routeProfile.MergedProfile();
        var current = this.mHead;
        while (current && current.mWaypoints) {
            if (!current.mProfile) {
                return //abort, unreliable profile
            }
            result.add(current.mProfile);
            current = current.mNext;
        }
        return result;
    };

    Route.prototype.getBounds = function() {
        var bounds = new L.LatLngBounds();
        this.mapSections(function(section) {
            bounds.extend(section.getBounds());
        });
        return bounds;
    };

    Route.prototype.highlight = function(highlight, onlyParent) {
        var self = this;
        function doHighlight() {
            if (!onlyParent) {
                self.mapSections(function(section) {
                    section.highlight(highlight);
                });
            }
            if (typeof highlight === 'number' || highlight == false) {
                self.mParent.highlight(highlight);
            }
        }
        if (highlight == false) {
            clearTimeout(this.mHighlightTimeout)
            delete this["mHighlightTimeout"];
            doHighlight();
        }
        this.mHighlightTimeout = setTimeout(function() {
            doHighlight();
        }, 300);
    };


    Route.prototype.delete = function() {
        this.mapSections(function(section) {
            section.delete();
        });
        this.mHead    = undefined;
        this.mHasFrom = false;
    };

    Route.prototype.mapSections = function(f) {
        var result = [];
        var current = this.mHead;
        while (current) {
            result.push(f(current));
            current = current.mNext;
        }
        return result;
    };

    Route.prototype.gpxRtePts = function() {
        var result = "";
        this.mapSections(function(section) {
            if (section.mWaypoints) {
                var waypoint;
                var ele = "";
                for (var i = 0; i < section.mWaypoints.length; i++) {
                    waypoint = section.mWaypoints[i];
                    if (waypoint.decoration && waypoint.decoration.e != undefined) {
                        ele = '<ele>'+waypoint.decoration.e+'</ele>';
                    }
                    result += '<rtept lat="'+waypoint.lat+'" lon="'+waypoint.lng+'">'+ele+'</rtept>\n';
                    ele = "";
                }
            }
        });
        return result;
    }


    return {
        Route : Route
    }

})();

module.exports = route;