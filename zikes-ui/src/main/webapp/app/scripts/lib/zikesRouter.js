/**
 * scripts/lib/zikes.js
 *
 * This handles communication with the zikes-api, i.e.: the backend.
 */

'use strict';

var http = require("./http.js");
var auth = require("../ui/auth.js");
var when = require("when");

var zikesRouter = (function() {


    /****************************************************************************
     *
     *
     * Communication with the routing server
     *
     *
     ****************************************************************************/
    var pendingRequest = undefined;
    var zikesServer = document.location.hostname != "localhost" ? "https://api-dot-zikes-web-client.appspot.com" : "http://localhost:7777";
    function requestRoute(coordinates, preferences) {
        if (pendingRequest) {
            return when.reject(new Error("busy"));
        }
        var coordinatesStr = "";
        for (var i = 0; i < coordinates.length; i++) {
            var milestone = coordinates[i];
            coordinatesStr+=milestone[0]+","+milestone[1];
            if (i+1 < coordinates.length) {
                coordinatesStr += ",";
            }
        }

        return http.post(zikesServer + "/route?milestones="+coordinatesStr, preferences)
        .otherwise(function(reason) {
            if (reason.message) {
                reason = reason.message;
                try {
                    reason = JSON.parse(reason);
                } catch(err) {}
            }
            var error = new Error("Unable to route");
            error.reason = reason;
            throw error;
        });
    }
    var defaultRoutingPrefs = {};
    function meta() {
        return http.get(zikesServer + "/meta.json").then(function(meta) {
            defaultRoutingPrefs = {};
            meta.defaultRoutingPrefs.map(function(preference){
                defaultRoutingPrefs[preference.metadata.opaque.name] = preference;
            });
            return meta;
        });
    }



    /****************************************************************************
     *
     *
     * Authentication with zikes-api function for the journey/user meta
     *
     *
     ****************************************************************************/
    var authProviderHeaderName = "Authorization-Provider";
    var authTokenHeaderName    = "Authorization";

    function authHeaders(authenticatedUser) {
        var result = {};
        result[authProviderHeaderName] = authenticatedUser.authResponse.network.toUpperCase();
        result[authTokenHeaderName]    = "Bearer "+authenticatedUser.authResponse.access_token; //Bearer see: http://self-issued.info/docs/draft-ietf-oauth-v2-bearer.html
        return result;
    }

    var userData = when.defer();
    auth.on("logout", function() {
        userData = when.defer();
    });
    auth.on("login", function(authenticatedUser) {
        return new http.HttpRequest(
            zikesServer + "/users/metadata"
        ).headers(
            authHeaders(authenticatedUser)
        ).get().then(function(zikesUserMetaData) {
            if (zikesUserMetaData.opaque.name != authenticatedUser.userDetails.name) {
                //first time see this user, let's write what we
                //know of them
                new http.HttpRequest(
                    zikesServer + "/users/metadata"
                ).headers(
                    authHeaders(authenticatedUser)
                ).put({
                    opaque : {
                        name: authenticatedUser.userDetails.name,
                        email: authenticatedUser.userDetails.email,
                        encodedId: zikesUserMetaData.encodedId
                    }
                });
            }
            userData.resolve({
                authenticatedUser : authenticatedUser,
                zikesUserMetaData : zikesUserMetaData
            });
        });
    });

    function login(speculative) {
        var prompt = "Login to complete action";
        if (speculative) {
            prompt = undefined;
        }
        return auth.login(prompt)
        .then(function(authenticatedUser) {
            if (authenticatedUser) {
                return userData.promise;
            }
        });
    }

    function isLoggedIn() {
        return userData.promise.inspect().state == "fulfilled";
    }

    function userId() {
        if (isLoggedIn() ) {
            return userData.promise.inspect().value.zikesUserMetaData.encodedId;
        }
    }


    /****************************************************************************
     *
     *
     * Planned journeys
     *
     *
     ****************************************************************************/
    function userPath(uid) {
        return zikesServer + "/users/"+uid;
    }

    function userPlanPath(uid) {
        return userPath(uid)+"/plan";
    }

    function userPlanJourneysPath(uid) {
        return userPlanPath(uid)+"/journeys"
    }

    function plannedJourneys() {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanJourneysPath(userData.zikesUserMetaData.encodedId)
            ).headers(
                authHeaders(userData.authenticatedUser)
            ).get();
        });
    }

    function loadPlannedJourney(id, uid) {
        return when(login(!!uid)).then(function(userData) {
            uid = uid || userData.zikesUserMetaData.encodedId;
            var req = new http.HttpRequest(
                userPlanJourneysPath(uid)+"/"+id
            );
            if (userData) {
                req.headers(
                    authHeaders(userData.authenticatedUser)
                );
            }
            return req.get().then(function(journey) {
                if (userData) {
                    //this is the current user's journey - lets make it easy to share
                    journey.metadata.userId = userData.zikesUserMetaData.encodedId;
                } else {
                    //this is someone else's journey, lets make it easy to copy
                    delete journey.metadata["id"];
                    delete journey.metadata.opaque["autosave"];
                }
                return journey;
            });
        });
    }

    function deletePlannedJourney(id) {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanJourneysPath(userData.zikesUserMetaData.encodedId)+"/"+id
            ).headers(
                authHeaders(userData.authenticatedUser)
            ).delete();
        });
    }

    function savePlannedJourney(journey) {
        return login().then(function(userData) {
            var req = new http.HttpRequest(
                userPlanJourneysPath(userData.zikesUserMetaData.encodedId)+"/"+(journey.metadata.id || "")
            ).headers(
                authHeaders(userData.authenticatedUser)
            );
            if (journey.metadata.id) {
                //update
                req = req.put(journey);
            } else {
                //create
                req = req.post(journey);
            }
            return req.then(function(response) {
                journey.metadata = response;
                journey.metadata.userId = userData.zikesUserMetaData.encodedId;
                return journey.metadata;
            });
        });
    }

    /****************************************************************************
     *
     *
     * Preferences
     *
     *
     ****************************************************************************/
    function userPlanPreferencesPath(uid) {
        return userPlanPath(uid)+"/preferences"
    }

    var userPreferences = {
        metadata: {
            access: {
                ro: "",
                rw: ""
            },
            opaque: {
                type: "user"
            }
        },
        preferences: {
            currentProfile: "vanilla bicycle"
        }

    };

    var userProfiles = {};
    function allProfiles() {
        var result = {}
        for (var key in defaultRoutingPrefs) {
            result[key] = defaultRoutingPrefs[key];
        }
        for (var key in userProfiles) {
            result[key] = userProfiles[key];
        }
        return result;
    }

    var preferenceListerners = [];
    var profileListeners     = [];
    var preferencesLoaded   = when(userData.promise).then(function(userData) {
        return new http.HttpRequest(
            userPlanPreferencesPath(userData.zikesUserMetaData.encodedId)
        ).headers(
            authHeaders(userData.authenticatedUser)
        ).get().then(function(allPreferenceMetas) {
            return when.all(
                allPreferenceMetas.map(function(preference) {
                    return getPreference(preference.id);
                })
            )
        }).then(function(allPreferences) {
            allPreferences.map(function(preference) {
                if (preference.metadata.opaque.type == "user") {
                    userPreferences = preference;
                } else {
                    userProfiles[preference.metadata.opaque.name] = preference;
                }
            });
        }).then(function() {
            preferenceListerners.map(function(cb) {
                cb(userPreferences.preferences);
            });
            profileListeners.map(function(cb) {
                cb(allProfiles());
            });
        });
    });

    function getProfiles(cb) {
        var result = allProfiles()
        if (cb) {
            profileListeners.push(cb);
            cb(result);
        }
        return result;
    }

    function getPreferences(cb) {
        if (cb) {
            preferenceListerners.push(cb)
            cb(userPreferences.preferences);
        }
        return userPreferences.preferences;
    }

    function setPreferences(newPreferences) {
        for (var k in newPreferences) {
            userPreferences.preferences[k] = newPreferences[k];
        }
        if (preferencesLoaded.inspect().state == "fulfilled") {
            when(userData.promise).then(function(userData) {
                var req = new http.HttpRequest(
                    userPlanPreferencesPath(userData.zikesUserMetaData.encodedId)+"/"+(userPreferences.metadata.id || "")
                ).headers(
                    authHeaders(userData.authenticatedUser)
                );
                if (userPreferences.metadata.id) {
                    //update
                    req = req.put(userPreferences);
                } else {
                    //create
                    req = req.post(userPreferences);
                }
                return req.then(function(response) {
                    userPreferences.metadata = response;
                    preferenceListerners.map(function(cb) {
                        cb(userPreferences.preferences);
                    });
                });
            });
        }
    }


    function deleteProfile(id) {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanPreferencesPath(userData.zikesUserMetaData.encodedId)+"/"+id
            ).headers(
                authHeaders(userData.authenticatedUser)
            ).delete();
        });
    }

    function getPreference(id) {
        return login().then(function(userData) {
            return new http.HttpRequest(
                userPlanPreferencesPath(userData.zikesUserMetaData.encodedId)+"/"+id
            ).headers(
                authHeaders(userData.authenticatedUser)
            ).get();
        });
    }

    function saveProfile(profile) {
        return login().then(function(userData) {
            var req = new http.HttpRequest(
                userPlanPreferencesPath(userData.zikesUserMetaData.encodedId)+"/"+(profile.metadata.id || "")
            ).headers(
                authHeaders(userData.authenticatedUser)
            );
            if (profile.metadata.id) {
                //update
                req = req.put(profile);
            } else {
                //create
                req = req.post(profile);
            }
            return req.then(function(response) {
                profile.metadata = response;
                return profile.metadata;
            });
        });
    }


    return {
        requestRoute         : requestRoute,
        meta                 : meta,
        getPreferences       : getPreferences,
        setPreferences       : setPreferences,
        getProfiles          : getProfiles,
        saveProfile          : saveProfile,
        deleteProfile        : deleteProfile,
        plannedJourneys      : plannedJourneys,
        loadPlannedJourney   : loadPlannedJourney,
        savePlannedJourney   : savePlannedJourney,
        deletePlannedJourney : deletePlannedJourney,
        isLoggedIn           : isLoggedIn
    };
})();

module.exports = zikesRouter;