/**
 * scripts/ui/routeProfile.js
 *
 * This, given a GeoRouteProfile, can prepare and draw this profile on canvas
 */

'use strict';

var highways      = require("../lib/highways.js").highways;
var geometry      = require("../lib/geometry.js");

var routeProfile = (function() {

    function PixRouteProfile(geoProfile, profileCanvas, highlightCanvas, dEleDiv) {
        this.mGeoProfile      = geoProfile;
        this.mProfileCanvas   = profileCanvas;
        this.mHighlightCanvas = highlightCanvas;
        this.mDEleDiv         = dEleDiv;
        this.mFlattenBy       = Math.min(
            1.0*geoProfile.dEleMts()/200, 1
        )
        this.mPixWidth        = profileCanvas.width;
        this.mXFactor         = 1.0*this.mPixWidth/geoProfile.distanceMts();

        this.mPixHight        = profileCanvas.height;
        var raisedYWindow     = (this.mPixHight-this.mFlattenBy*15);
        this.mPixYAxis        = raisedYWindow/2;
        this.mGeoYAxis        = geoProfile.midEleMts();
        this.mYFactor         = 1;
        if (geoProfile.dEleMts() > 0) {
            this.mYFactor     = raisedYWindow/geoProfile.dEleMts();
        }
        this.prepare();
        this.highlight(false);
    }

    PixRouteProfile.prototype.pixy = function(eleMts) {
        var dEle = eleMts - this.mGeoYAxis;
        return Math.round(
            this.mPixYAxis-(dEle*this.mFlattenBy*this.mYFactor)
        );
    }

    PixRouteProfile.prototype.pixx = function(distanceMts) {
        return Math.round(this.mXFactor*distanceMts);
    }


    PixRouteProfile.prototype.pixyFromX = function(x) {
        var left  = this.mPixProfile[0];
        var right = undefined;
        this.mPixProfile.every(function(dp) {
            if (dp.x >= x) {
                right = dp;
                return false;
            }
            left = dp;
            return true;
        });
        if (x==left.x || right==undefined) {
            return left.y;
        } else if (x==right.x) {
            return right.y;
        }
        return Math.round(
            geometry.linearApprox(
                left.y, right.y, right.x-left.x, x-left.x
            )
        );
    }

    PixRouteProfile.prototype.dEleMts = function(eleMts) {
        return this.mGeoProfile.dEleMts();
    }

    function ProfileRollup(initialDp, highwayFwd, stepMts, prevDp) {
        var distanceFromPrev = (prevDp ? initialDp.distanceMts.fromStart - prevDp.distanceMts.fromStart : 0);
        this.mBeginMts       = initialDp.distanceMts.fromStart - distanceFromPrev;
        this.mDps            = [initialDp];
        this.mHighwaysBckwd  = {};
        if (highwayFwd) {
            this.mHighwaysBckwd[highwayFwd] = distanceFromPrev;
        }
        this.mStepMts       = stepMts;
        this.mTargetDistMts = {
            min: initialDp.distanceMts.fromStart + stepMts.min,
            max: initialDp.distanceMts.fromStart + stepMts.max
        }
        this.mEleTrend      = undefined;
        this.mReferenceDP   = initialDp;
    }

    ProfileRollup.prototype.last = function() {
        return this.mDps[this.mDps.length-1];
    }

    ProfileRollup.prototype.add = function(dp) {
        if (dp.distanceMts.fromStart > this.mTargetDistMts.max) {
            //reject
            return new ProfileRollup(dp, this.last().highwayFwd, this.mStepMts, this.last());
        }
        var highwayChanged  = dp.highwayFwd != this.last().highwayFwd;
        var dEleMts         = dp.eleMts - this.last().eleMts;
        var eleTrendChanged = this.mEleTrend != undefined &&
                              Math.sign(dEleMts) != Math.sign(this.mEleTrend);

        if (dp.distanceMts.fromStart > this.mTargetDistMts.min &&
            (highwayChanged || eleTrendChanged)) {
            //reject
            return new ProfileRollup(dp, this.last().highwayFwd, this.mStepMts, this.last());
        }
        this.mEleTrend = this.mEleTrend == undefined ? dEleMts : this.mEleTrend + dEleMts;
        if (!(this.last().highwayFwd in this.mHighwaysBckwd)) {
            this.mHighwaysBckwd[this.last().highwayFwd] = 0;
        }
        this.mHighwaysBckwd[this.last().highwayFwd] += dp.distanceMts.fromStart - this.last().distanceMts.fromStart;
        this.mDps.push(dp);

        //for now, and at the risk of hiding a massive hole, we simply
        //retain the higest elevation seen
        if (dp.eleMts > this.mReferenceDP.eleMts) {
            this.mReferenceDP = dp;
        }
    }

    ProfileRollup.prototype.eleMts = function() {
        return this.mReferenceDP.eleMts;
    }

    ProfileRollup.prototype.bestDistanceMts = function() {
        return this.mReferenceDP.distanceMts.fromStart;
    }

    ProfileRollup.prototype.endDistanceMts = function() {
        return this.last().distanceMts.fromStart;
    }

    ProfileRollup.prototype.midDistanceMts = function() {
        return this.mBeginMts + (this.endDistanceMts() - this.mBeginMts)/2;
    }

    ProfileRollup.prototype.startDistanceMts = function() {
        return this.mBeginMts;
    }

    ProfileRollup.prototype.highway = function() {
        var highwayKeys = Object.keys(this.mHighwaysBckwd).map(parseInt);
        if (highwayKeys.length > 0) {
            var longest = 0;
            return highwayKeys.reduce(function(cum, key) {
                if (highways[key] > longest) {
                    longest = highways[key]
                    cum = key;
                }
                return cum;
            });
        }
    }



    var KXPixMaxStep = 6;
    var KXPixMinStep = 3;
    PixRouteProfile.prototype.prepare = function() {
        this.mPixProfile = undefined;
        if (this.mGeoProfile != undefined && this.mGeoProfile.dataPointsNo() > 2) {
            var stepMts = {
                min : this.mGeoProfile.distanceMts()/(this.mPixWidth/KXPixMinStep),
                max : this.mGeoProfile.distanceMts()/(this.mPixWidth/KXPixMaxStep)
            };
            var dpIt = this.mGeoProfile.iterator();
            var first = dpIt.current();
            this.mPixProfile = [{
                x: 0,
                y: this.pixy(first.eleMts)
            }];
            var rollup = new ProfileRollup(
                dpIt.next().current(),
                first.highwayFwd,
                stepMts
            );
            while(dpIt.peekNext()) {
                var newRollup = rollup.add(
                    dpIt.next().current()
                );
                if (newRollup) {
                    this.mPixProfile.push({
                        x: this.pixx(rollup.bestDistanceMts()),
                        y: this.pixy(rollup.eleMts()),
                        highway: rollup.highway()
                    });
                   rollup = newRollup;
                }
            }
            this.mPixProfile.push({
                x: this.mPixWidth,
                y: this.pixy(rollup.eleMts()),
                highway: rollup.highway()
            });
        }
    }

    var KHighwaySpectrumHeightPix = 4;
    PixRouteProfile.prototype.draw = function() {
        var canvas = this.mProfileCanvas;
        var c2 = canvas.getContext("2d");
        c2.clearRect(0, 0, canvas.width, canvas.height);
        if (this.mPixProfile == undefined) {
            return;
        }
        c2.fillStyle = "#5bc0de";
        c2.beginPath();
        c2.moveTo(this.mPixProfile[0].x, this.mPixProfile[0].y);
        for (var i = 1; i < this.mPixProfile.length; i++ ) {
            var point = this.mPixProfile[i];
            c2.lineTo(point.x, point.y);
        }
        c2.lineTo(canvas.width, canvas.height-KHighwaySpectrumHeightPix);
        c2.lineTo(0,canvas.height-KHighwaySpectrumHeightPix);
        c2.closePath();
        c2.fill();

        var prevX = 0;
        for (var i = 1; i < this.mPixProfile.length; i++ ) {
            var point = this.mPixProfile[i];
            var highway = highways.highway(point.highway);
            if (highway) {
                c2.beginPath();
                c2.rect(prevX, canvas.height-KHighwaySpectrumHeightPix, point.x-prevX, canvas.height);
                c2.fillStyle = highway.color();
                c2.fill();
            }
            prevX = point.x;
        }
    }


    var drawnProfile = false;
    PixRouteProfile.prototype.highlight = function(milestoneMts, cb) {
        var canvas = this.mHighlightCanvas;
        var c2     = canvas.getContext("2d");
        var isMilestoneNumber = !isNaN(parseFloat(milestoneMts)) && isFinite(milestoneMts)
        if (!isMilestoneNumber || !this.mPixProfile) {
            drawnProfile = false;
            setTimeout(function() {
                if (!drawnProfile) {
                    c2.clearRect(0, 0, canvas.width, canvas.height);
                    cb && cb();
                }
            }, 1000);

        } else {
            drawnProfile = true;
            c2.clearRect(0, 0, canvas.width, canvas.height);
            var bottom           = canvas.height-15;
            var x                = this.pixx(milestoneMts);
            var y                = this.pixyFromX(x);
            c2.beginPath();
            c2.moveTo(x-1, y);
            c2.lineTo(x+1, y);
            c2.lineTo(x+1, bottom-1);
            c2.lineTo(x-1, bottom-1);
            c2.fillStyle = "#ebebeb";
            c2.closePath();
            c2.fill();
            var halfCanvasWidth = canvas.width/2;
            var dEleX = Math.max(
                Math.min(
                    x-halfCanvasWidth,
                    halfCanvasWidth-22
                ), 22-halfCanvasWidth
            ) - 2;
            this.mDEleDiv.css(
                "margin-left",
                dEleX+"px"
            );
            this.mDEleDiv.text(
                Math.round(
                    this.mGeoProfile.eleMts(milestoneMts)
                ) + "mts"
            );
        }
    }


    return {
        PixRouteProfile: PixRouteProfile
    }

})();

module.exports = routeProfile;