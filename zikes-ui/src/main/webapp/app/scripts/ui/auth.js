/**
 * scripts/ui/auth.js
 *
 * Handles authentication (with ui)
 */

'use strict';

var when     = require("when");
var hello    = require("hellojs");
var mustache = require("mustache");

var auth = (function() {

    var networks = {
        facebook: "955510821203918",
        google: "787067721578-396usv4alu7u9rjfnf0mi2pa6m7r8jps.apps.googleusercontent.com"
    }

    var authDialogTemplate = when.defer();
    $.get("templates/ui.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#authDialogTemplate_tmpl").html()
        authDialogTemplate.resolve(html);
    });


    function AuthenticatedUser(network) {
        this.network = network;
        this.authResponse = hello(network).getAuthResponse();
    }


    /**
     * Returns a promise resolving when 'this' is fully authenticated
     */
    AuthenticatedUser.prototype.login = function(force) {
        var tokenValidity = this.tokenValidity();
        if (!force && tokenValidity == KValid) {
            return when(this.authResponse);
        }

        var display = "popup";
        if (!force && this.network != "facebook") {
            display = "none";
        }
        if (!force && tokenValidity == KAboutToExpire) {
            force = null;
        }
        var result = when.defer();
        var self = this;
        hello(this.network).login({
            display: display,
            force  : force
        })
        .then(function(response) {
                self.authResponse = response.authResponse;
                result.resolve(self.authResponse);
            }, function(e) {
                console.error("error loggin in", e.error);
                result.reject(e);
            }
        );
        return result.promise;
    }

    AuthenticatedUser.prototype.logout = function() {
        hello.logout(this.network);
    }

    AuthenticatedUser.prototype.setUserDetails = function(details) {
        this.userDetails = details;
    }


    var KTokenValidityMarginSec = 100;
    var KAbsent        = 0;
    var KExpired       = 1;
    var KAboutToExpire = 2;
    var KValid         = 4;
    AuthenticatedUser.prototype.tokenValidity = function() {
        if (!(this.authResponse &&
             this.authResponse.access_token)) {
            return KAbsent;
        }

        var currentTime   = (new Date()).getTime() / 1000;
        var timeRemaining = this.authResponse.expires - currentTime;
        if (timeRemaining < 0) {
            return KExpired;
        }
        if (timeRemaining < KTokenValidityMarginSec) {
            return KAboutToExpire;
        }
        return KValid;
    }






    function getAuthenticatedUser() {
        var helloStorage = JSON.parse(
            window.localStorage.getItem("hello")
        );
        if (helloStorage) {
            for (var networkName in networks) {
                var authenticatedNetwork = helloStorage[networkName];
                if (authenticatedNetwork && authenticatedNetwork.access_token) {
                    return new AuthenticatedUser(networkName);
                }
            }
        }
    }

    var authenticatedUser = getAuthenticatedUser();
    hello.init(networks, {redirect_uri: 'auth_redirect.html'});
    login(); //speculative


    function login(prompt) {
        if (authenticatedUser) {
            return authenticatedUser.login(false);
        } else if (prompt) {
            return when(authDialogTemplate.promise)
            .then(function(template) {
                var result = when.defer();
                function doLogin(network) {
                    var au = new AuthenticatedUser(network);
                    var whenLoggedIn = au.login(true);
                    whenLoggedIn.then(function() {
                        authenticatedUser = au;
                    });
                    result.resolve(whenLoggedIn);
                }

                $(document.body).append(
                    mustache.render(
                        template, {
                            prompt:prompt
                        }
                    )
                );
                var modal = $("#authModal").modal({"backdrop" : "static"});
                modal.find("#google").click(function() {
                    modal.modal("hide");
                    doLogin("google");
                });
                modal.find("#facebook").click(function() {
                    modal.modal("hide");
                    doLogin("facebook");
                });
                modal.on('hidden.bs.modal', function (e) {
                    modal.remove();
                });
                return result.promise;
            });
        }
        return when();
    }

    var subscribers = {
        login:  [],
        logout: []
    }
    hello.on("auth.login", function(r) {
        if (!authenticatedUser || authenticatedUser.network != r.network) {
            authenticatedUser = new AuthenticatedUser(r.network);
        }
        hello( r.network ).api( '/me' ).then( function(p) {
            var userNavItem = $("#userNavItem");
            userNavItem.find(".dropdown-toggle").html(
                '<img src="'+ p.thumbnail + '" width="30px" height="30px" style="border-width:1px;border-style:inset;"/><span style="padding-left:4px">'+p.name+'</span><b class="caret"></b>'
            );
            $("#loginNavItem").addClass("hide");
            userNavItem.removeClass("hide");
            authenticatedUser.setUserDetails(p);
            subscribers.login.forEach(function(subscriber) {
                subscriber(authenticatedUser);
            });
        }, function(e) {
            console.error("The token we have been assured is valid turns out stale, we need to force you to hard-authenticate again. Sorry about that", e);
            authenticatedUser.login(true);
        });
    });

    hello.on("auth.logout", function(r) {
        $("#loginNavItem").removeClass("hide");
        $("#userNavItem").addClass("hide");
        authenticatedUser = undefined;
        subscribers.logout.forEach(
            function(subscriber) {subscriber();}
        );
    });

    function logout() {
        if (authenticatedUser) {
            authenticatedUser.logout();
        }
        authenticatedUser = undefined;
    }

    function on(what, cb) {
        subscribers[what].push(cb);
    }

    $("#loginNavItem").click(function() {
        login("Choose Network");
    });

    $("#logoutNavItem").click(function() {
        logout();
    });

    return {
        login            : login,
        logout           : logout,
        on               : on
    };
})();

module.exports = auth;