package com.zikes.api.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.googlecode.objectify.annotation.*;
import com.zikes.api.storage.ZikesIdManager;

@Entity
@Cache
@Index
@JsonAutoDetect
public class ZikesUser {
    @Id
    private String providedId;

    @JsonIgnore
    private Long zikesId;

    @Ignore
    private String encodedId;

    private ObjectNode opaque;

    private long created;
    private long lastModified;

    @OnLoad
    public void onLoad() {
        this.encodedId = ZikesIdManager.encodeId(zikesId);
    }

    public String getProvidedId() {
        return providedId;
    }

    public void setProvidedId(String providedId) {
        this.providedId = providedId;
    }

    public Long getZikesId() {
        return zikesId;
    }

    public void setZikesId(Long zikesId) {
        this.zikesId = zikesId;
    }

    public String getEncodedId() {
        return encodedId;
    }

    public ObjectNode getOpaque() {
        return opaque;
    }

    public void setOpaque(ObjectNode opaque) {
        this.opaque = opaque;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }
}
