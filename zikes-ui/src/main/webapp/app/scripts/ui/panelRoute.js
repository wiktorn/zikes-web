/**
 * scripts/ui/panelRoute.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

var when          = require("when");
var mustache      = require("mustache");

var googleAPI     = require("../lib/google.js");
var utils         = require("../lib/utils.js");

var map           = require("./map.js");
var mapRoute      = require("./mapRoute.js");
var messages      = require("./messages.js");
var routeProfile  = require("./routeProfile.js");

var journeyItem   = require("./journeyItem.js");
var JourneyItem   = journeyItem.JourneyItem;
var journeyTemplates = journeyItem.templates;

var panelRoute = (function() {

    var colours = [
      { base : "#0000FF", min: "#000066", max: "#ff0000"},
      { base : "#006699", min: "#002233", max: "#ff0000"},
      { base : "#cc00cc", min: "#330033", max: "#ffff00"},
      { base : "#cc0000", min: "#330000", max: "#ffff00"},
      { base : "#663300", min: "#000000", max: "#ffff00"},
      { base : "#006600", min: "#000000", max: "#ffff00"}
    ]

    function colorTemplateParams() {
        var result = {};
        var keys = Object.keys(colours);
        for (var i = 0; i < keys.length; i++) {
            result["c"+i] = colours[i].base;
        }
        return result;
    }

    function Route(parent, itemsDiv) {
        JourneyItem.call(this, parent, itemsDiv);
        this.mParams["from"] = {};
        this.mParams["to"]   = {};
        this.mCentreOnRoute  = true;

        var self = this;
        this.mRouteOnMap = new mapRoute.Route({
            style : self.style.bind(self),
            otherJoureyItems : function(cb) {
                self.otherItems(Route, function(item) {cb(item.mRouteOnMap);});
            },
            highlight : function(milestoneMts) {
                if (!isNaN(parseFloat(milestoneMts)) && isFinite(milestoneMts)) {
                    self.show();
                }
                if (self.mPixProfile) {
                    self.mPixProfile.highlight(
                        milestoneMts,
                        function() {
                            resetDelta(self);
                        }
                    );
                }
            },
            invalidate : function() {
                return self.render().then(function() {
                    function section2LatLng(section) { return section ? section.getLatLng() : section;}
                    self.from(section2LatLng(self.mRouteOnMap.from()));
                    self.to(section2LatLng(self.mRouteOnMap.to()));
                    self.mPixProfile = new routeProfile.PixRouteProfile(
                        self.mRouteOnMap.getProfile(),
                        self.mDivs.profileCanvas,
                        self.mDivs.highlightCanvas,
                        self.mDivs.dEle
                    );
                    if (!self.mRouteOnMap.isPending()) {
                        if (self.mCentreOnRoute) {
                            self.centre();
                            self.mCentreOnRoute = false;
                        }
                    }
                    return self.invalidate();
                })
            },
            route : function(milestones) {
                var beforeMts = new Date().getTime();
                map.busy(true);
                self.busy(true);
                var routed = self.mParent.route(milestones);

                routed.then(function(route) {
                    var serverRoutingMs  = route.meta.routing_us/1000;
                    var networkLatencyMs = (new Date().getTime() - beforeMts)-serverRoutingMs;
                    var visitedJunctions = route.meta.routingRequests.reduce(function(cum, cur) {return cum+cur.visitedJunctions}, 0);
                    messages.info(
                        "To plot this route we visited "+utils.largeNumberToStr(visitedJunctions)
                         +" junctions and it took us "+utils.millisToString(serverRoutingMs)+" to do so. It took us additional "
                         +utils.millisToString(networkLatencyMs)+" to send this result accross the wire."
                    ).enqueue();
                })
                .catch(function(err) {
                    var message = undefined;
                    if (typeof err.reason === 'object') {
                        if (err.reason.code == "RoutingException::NearestPointNotFound") {
                            message = "Unable to find a road/path within "+err.reason.attemptedToleranceMts+"mts from where you clicked. Wiggle that a bit and position it nearer a way please.";
                            map.leaflet.setView(
                                err.reason.culprit,
                                13,
                                { animate: true }
                            );
                        } else if (err.reason.code == "RoutingException::ServerOverload") {
                            message = "We've analysed " + utils.largeNumberToStr(err.reason.junctionsLimitExceeded) + " junctions for you and had to to bail to protect our small server from swapping RAM. Try plotting a shorter route.";
                        } else if (err.reason.code == "RoutingException::NoRoute" ) {
                            message = "We're sorry, but we could not find a route between the points you requested.";
                        }
                    }
                    messages.error(
                        message || "We're sorry, there's been an unusual problem. Our server says :'"+err.message+"'"
                    ).enqueue();
                })
                .ensure(function() {
                    map.busy(false);
                    self.busy(false);
                }).done();
                return routed;
            }
        });
    }
    Route.prototype = new JourneyItem();
    Route.prototype.constructor = Route;



    function updateGeocodedLocation(self, latlng, toOrFromStr) {
        if (latlng instanceof L.LatLng) {
            latlng = [latlng.lat, latlng.lng]
        }
        var storedAs = self.mParams[toOrFromStr];
        if (!storedAs.latlng ||
            !(latlng[0] == storedAs.latlng[0] && latlng[1] == storedAs.latlng[1])) {
            storedAs.latlng = latlng;
            googleAPI.reverseGeocodeFind(latlng)
            .then(function(geocode) {
                storedAs.geocode = geocode;
                storedAs.latlng = latlng;
                self.invalidate();
            });
        }
    }

    Route.prototype.style = function() {
        return utils.mergeSimpleObjects(
            this.mParams.style,
            { showElevation : true,
              color : colours[0] },
            true
        );
    }

    Route.prototype.from = function(latlng) {
        if (!latlng) {
            //getter
            return this.mParams.from;
        }
        updateGeocodedLocation(this, latlng, "from");
        if (this.mRouteOnMap.from(latlng)) {
            this.mRouteOnMap.invalidate();
        }
    }

    Route.prototype.to = function(latlng) {
        if (!latlng) {
            //getter
            return this.mParams.to;
        }
        updateGeocodedLocation(this, latlng, "to");
        if (this.mRouteOnMap.to(latlng)) {
            this.mRouteOnMap.invalidate();
        }
    }

    Route.prototype.route = function() {
        this.mRouteOnMap.invalidate({reroute:true});
    }

    Route.prototype.showsElevation = function() {
        return this.mRouteOnMap.showsElevation();
    }

    Route.prototype.fillContextMenu = function(ctxMenu) {
        if (this.mRouteOnMap.from()) {
            //from set
            ctxMenu.from({
                display: "Change From"
            }).to({
                bold: true
            });
        } else {
            ctxMenu.to().from({
                bold: true
            });
        }
        return ctxMenu;
    }

    Route.prototype.name = function() {
        var result = JourneyItem.prototype.name.call(this);
        if (!result) {
            if ((this.mParams.from.geocode || this.mParams.to.geocode)) {
                var fromTo = ["",""];
                if (this.mParams.from.geocode && this.mParams.to.geocode) {
                    fromTo = googleAPI.geocodeHumanReadable(
                        [this.mParams.from.geocode,
                         this.mParams.to.geocode]
                    );
                } else if (this.mParams.from.geocode) {
                    fromTo[0] = googleAPI.geocodeHumanReadable(
                        this.mParams.from.geocode
                    );
                } else {
                    fromTo[1] = googleAPI.geocodeHumanReadable(
                        this.mParams.to.geocode
                    );
                }
                result = fromTo[0] + " -> " + fromTo[1];
            } else {
                result = "New Route";
            }
        }
        return result;
    }

    Route.prototype.distanceMts = function() {
        if (this.mRouteOnMap) {
            return this.mRouteOnMap.distanceMts();
        }
        return 0;
    }

    Route.prototype.isPending = function() {
        return this.mRouteOnMap.isPending();
    }

    Route.prototype.isUnititialised = function() {
        return this.mRouteOnMap.isUnititialised();
    }

    Route.prototype.delete = function() {
        JourneyItem.prototype.delete.call(this);
        this.mRouteOnMap.delete();
    }

    function resetDelta(self) {
        var deltaChar = "\u0394";
        var content = "";
        if (self.mPixProfile && !isNaN(self.mPixProfile.dEleMts())) {
            content = deltaChar + Math.round(self.mPixProfile.dEleMts()) + "mts";
        }
        self.mDivs.dEle.text(content);
        self.mDivs.dEle.css("margin-left", "0px");
    }


    Route.prototype.doInvalidate = function() {
        if (!this.mRouteOnMap || (!this.mRouteOnMap.from() && !this.mRouteOnMap.to())) {
            this.delete();
            return;
        }
        JourneyItem.prototype.doInvalidate.call(this);
        this.mDivs.distance.text(utils.mtsToString(this.distanceMts()));
        if (this.mPixProfile) {
            this.mPixProfile.draw();
        }
        resetDelta(this);
    }

    Route.prototype.toJSON = function() {
        return {
            meta: this.mParams,
            sections: this.mRouteOnMap.toJSON()
        };
    }

    Route.prototype.fromJSON = function(json) {
        var self = this;
        this.mCentreOnRoute = false;
        this.mParams = json.meta;
        this.show = function() { //prevent expanding accordion when deserialising
            delete self["show"];
            return when(true);
        }
        return this.mRouteOnMap.fromJSON(json.sections);
    }

    Route.prototype.busy = function(busy) {
        if (this.mDivs) {
            if (busy) {
                this.mDivs.centreIcon.addClass("hide");
                this.mDivs.busyIcon.removeClass("hide")
            } else {
                this.mDivs.busyIcon.addClass("hide");
                this.mDivs.centreIcon.removeClass("hide")
            }
        }
    }

    Route.prototype.getBounds = function() {
        return this.mRouteOnMap.getBounds();
    }

    Route.prototype.toGpx = function() {
        var result = '<rte>\n<name>'+utils.escapeXML(this.name())+'</name>\n';
        result += this.mRouteOnMap.gpxRtePts() + '\n</rte>';
        return result;
    }

    Route.prototype.render = function() {
        if (this.mDivs) {
            return when(true);
        }
        var self = this;
        return JourneyItem.prototype.render.call(
            this,
            "Route",
            {
                distance: utils.mtsToString(self.distanceMts()),
                dEle: "",
                color: this.style().color.base
            }
        )
        .then(function() { return journeyItem.templates;})
        .then(function(templates) {
            self.mDivs["profileCanvas"]   = self.mDivs.collapsiblePanel.find(".routeProfileCanvas").get(0);
            self.mDivs["highlightCanvas"] = self.mDivs.collapsiblePanel.find(".highlightProfileCanvas").get(0);
            self.mDivs["distance"]        = self.mDivs.collapsiblePanel.find(".routeDistance");
            self.mDivs["dEle"]            = self.mDivs.collapsiblePanel.find(".routeEleDelta");
            self.mDivs["changeColour"]    = self.mDivs.collapsiblePanel.find("#changeColor a");
            self.mDivs.collapsiblePanel.hover(
                function() { self.mRouteOnMap.highlight(true)},
                function() { self.mRouteOnMap.highlight(false)}
            );

            var popover = self.mDivs.name.popover({
                trigger: 'manual',
                html:    true,
                placement: 'bottom',
                title:   "route colour",
                content:  mustache.render(
                    templates.colours,
                    utils.mergeSimpleObjects(
                        colorTemplateParams(), {
                            checked : self.style().showElevation ? "checked" : ""
                        }
                    )
                )
            });
            self.mDivs.changeColour.click(function(e) {
                function hide() {
                    popover.popover("hide");
                    $('body').off('click', dismissIfClickedOutside);
                }
                function dismissIfClickedOutside(e) {
                    var popoverDiv = self.mDivs.name.parent().find(".popover");
                    if (popoverDiv.length && !$(e.target).parents(".popover").length) {
                        hide();
                    }
                }
                $('body').on('click', dismissIfClickedOutside);
                setTimeout(function() {
                    popover.popover("show");
                    var checkbox = self.mDivs.name.parent().find("input");
                    checkbox.prop('checked', !!self.style().showElevation);
                    self.mDivs.name.parent().find(".colorSwatchItem-clickable").click(function(e) {
                        hide();
                        var color = colours[parseInt($(e.target).attr("id").slice(1))];
                        self.mParams.style = utils.mergeSimpleObjects(self.mParams.style, {color : color});
                        self.mRouteOnMap.invalidate({redraw:true});
                        self.mDivs.changeColour.find(".colorSwatchItem").css("background", color.base);
                    });
                    checkbox.click(function(e) {
                        self.mParams.style = utils.mergeSimpleObjects(
                            self.mParams.style, {showElevation : $(e.target).is(":checked")}
                        );
                        self.mRouteOnMap.invalidate({redraw:true});
                    });
                }, 0);
            });
        });
    }


    return { Route : Route };

})();

module.exports = panelRoute;