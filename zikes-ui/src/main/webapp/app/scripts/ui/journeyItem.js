/**
 * scripts/ui/journeyItem.js
 *
 * Base class for Journey Items
 */

'use strict';

var when          = require("when");
var mustache      = require("mustache");
var map           = require("./map.js");
var linkifyHtml   = require('linkifyjs/html')

var journeyItem = (function() {

    var templates = when.defer();
    $.get("templates/journey.template.html", function(template, textStatus, jqXhr) {
        templates.resolve({
            Journey : $(template).filter("#journey_tmpl").html(),
            Route   : $(template).filter("#route_tmpl").html(),
            POI     : $(template).filter("#poi_tmpl").html(),
            colours : $(template).filter("#color_picker_tmpl").html()
        });
    });

    var journeyItemId = 0;
    function JourneyItem(parent, itemsDiv) {
        this.mParent   = parent;
        this.mItemsDiv = itemsDiv;
        this.mId       = "journeyItemId_"+journeyItemId++;
        this.mParams   = {
            givenName      : undefined,
            description    : ""
        }
    }

    JourneyItem.prototype.name = function() {
        return this.mParams.givenName;
    }

    JourneyItem.prototype.delete = function() {
        $( "#"+this.mId).remove();
        if (this.mParent) {
            this.mParent.deleteItem(this);
        }
    }

    JourneyItem.prototype.show = function() {
        var self = this;
        var rendered = true;
        if (!this.mDivs) {
            rendered = this.render();
        }
        return when(rendered)
        .then(function() {
            self.mDivs.collapse.collapse("show");
        });
    }

    JourneyItem.prototype.isShown = function() {
        if (this.mDivs) {
            var openPanel = this.mDivs.collapsiblePanel.find(".collapse.in");
            return openPanel.length == 1;
        }
        return false;
    }

    JourneyItem.prototype.otherItems = function(itemType, cb) {
        var self = this;
        this.mParent.forEach(itemType, function(item) {
            if (self.mId != item.mId) {
                cb(item);
            }
        });
    }

    JourneyItem.prototype.hide = function() {
        this.mDivs.collapse.collapse("hide");
    }

    JourneyItem.prototype.centre = function() {
        map.leaflet.fitBounds(
            this.getBounds(),
            {
                animate : true,
                padding : [30,30]
            }
        );
    }

    JourneyItem.prototype.invalidate = function() {
        var self = this;
        if (!this.mInvalidatePosted) {
            this.mInvalidatePosted = this.render()
            .then(function() {
                self.doInvalidate();
                delete self["mInvalidatePosted"];
                !self.mParent || self.mParent.invalidate();
            });
        }
        return this.mInvalidatePosted;
    }

    JourneyItem.prototype.doInvalidate = function() {
        if (!this.mParams.givenName) {
            this.mDivs.name.text(this.name());
        }
    }

    function removeMarkup($div) {
        return when.delay(0).then(function() {
            $div.html($div.text());
            return $div.text();
        });
    }

    JourneyItem.prototype.render = function(templateName, templateParams) {
        if (this.mDivs) {
            return when(true);
        }
        templateParams = templateParams || {};
        var self = this;
        return when(templates.promise)
        .then(function(templates) {
            templateParams["id"]          = self.mId;
            templateParams["parent"]      = "journey";
            templateParams["name"]        = self.name();
            var descriptionHtml = self.mParams.description || "description";

            self.mItemsDiv.append(mustache.render(templates[templateName], templateParams));
            var collapsiblePanel = self.mItemsDiv.find("#"+self.mId);
            self.mDivs = {
                collapsiblePanel    : collapsiblePanel,
                name                : collapsiblePanel.find(".panelHeading"),
                description         : collapsiblePanel.find(".description"),
                collapse            : collapsiblePanel.find(".collapse"),
                delete              : collapsiblePanel.find("#delete"),
                centreIcon          : collapsiblePanel.find("#centreIcon"),
                busyIcon            : collapsiblePanel.find("#busyIcon")
            }
            function closeOthers() {
                //for some reason, we need to programmatically enforce single open panel
                $("#journey .collapse.in").each(function() {
                    var collapsible = $(this);
                    if (collapsible.parent()[0] != collapsiblePanel[0]) {
                        collapsible.collapse("hide");
                    }
                });
            }
            collapsiblePanel.on("show.bs.collapse", closeOthers);
            collapsiblePanel.on("shown.bs.collapse", closeOthers);
            $('[data-toggle="tooltip"]').tooltip();
            self.mDivs.delete.click(function() {
                self.delete();
            });
            self.mDivs.centreIcon.click(function() {
                self.centre();
            });

            /**
             * Handle description field
             */
            self.mDivs.description.html(descriptionHtml);
            self.mDivs.description.focus(function() {
                if (!self.mParams.description) {
                    $(this).text("");
                }
            }).blur(function() {
                var newDescription = $(this).html();
                newDescription = newDescription == "" ? undefined : newDescription;
                if (!newDescription) {
                    $(this).text("description");
                    $(this).addClass("placeHolderText");
                } else {
                    newDescription = linkifyHtml(newDescription, {
                        attributes: {
                            contenteditable: "false"
                        },
                        className: 'zikesAnchor',
                        format: function (value, type) {
                            // value is the link, type is 'url', 'email', etc.
                            if (type === 'url' && value.length > 30) {
                                value = value.slice(0, 30) + '…';
                            }
                            return value;
                        }
                    });
                    $(this).html(newDescription);
                }
                if (self.mParams.description != newDescription) {
                    self.mParams.description = newDescription;
                    self.invalidate();
                }
            }).on("paste", function() {
                $(this).removeClass("placeHolderText");
                removeMarkup($(this));
            });


            /**
             * Handle name field
             */
            self.mDivs.name.blur(function() {
                if (self.name() != $(this).text()) {
                    //something got editted
                    if ($(this).text() == "") {
                        self.mParams.givenName = undefined;
                    } else {
                        self.mParams.givenName = $(this).text();
                    }
                    self.invalidate();
                }
            }).on('paste keyup input', function () {
                removeMarkup($(this));
            }).keypress(function (e) {
              if (e.keyCode == 10 || e.keyCode == 13) {
                e.preventDefault();
                self.mDivs.name.trigger("blur");
              }
            })
            self.show();
        });
    }

    return {
        JourneyItem : JourneyItem,
        templates   : templates.promise
    };

})();

module.exports = journeyItem;