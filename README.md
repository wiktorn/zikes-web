Zikes
================================

This application lets you create cycling tours. Better Than You Ever Imagined

## Architecture overview
Application contains two logical modules (ui + api) and 'ear' to orchestrate run/deploy flow

Since API module serves like a proxy server for a set of endpoints we include following three running modes :
1. Local (assumes local engine is running and listening port 8087)
2. Mock (serves pre loaded static responses)
3. Production (goes to production server)

Spring profile is generated dynamically on pre-compile step and according to a given environment
In a root folder you can find two predefined properties files:
1. gradle-local.properties
2. gradle-mock.properties

gradle-prod.properties file is confidential and should never be checked-in
If not explicitly specified gradle-local is going to be used by default
To specify desired mode please use './gradlew [YOUR COMMMAND HERE] -PenvironmentName=mock/local/prod'

For instance, to run zikes-api in the local mode, from zikes-api folder run:
```../gradlew appengineRun -PenvironmentName=local```
to deploy:
```../gradlew appengineUpdate -PenvironmentName=prod```

## Products
- [App Engine][1]

## Language
- [Java][2]

## APIs
- [Google Cloud Storage][3]

## Setup Instructions
1. git clone https://bitbucket.org/pturowskij/zikes-web.git
2. cd zikes-ear
3. ../gradlew appengineRun (by default starts API [localhost:7777][4] & UI [localhost:8080][5])

## Run Tests
1. cd zikes-api
2. ../gradlew test (html report can be found in /build/reports/index.html)
3. ../gradlew appengineFunctionalTest -PenvironmentName=mock

## Deploy Instructions
1. cd zikes-ear
2. ../gradlew appengineUpdate

[1]: https://developers.google.com/appengine
[2]: http://java.com/en/
[3]: https://cloud.google.com/docs/
[4]: https://localhost:7777/
[5]: https://localhost:8080/
