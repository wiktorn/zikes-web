/**
 * scripts/lib/urbanRural.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the urban/rural preference
 */

'use strict';

var urbanRural = (function() {

    var KSliderMax                  = 40;
    var KMaxLog2Magnitude           = 15;
    var KUrbanFlattenLog2Magnitude  = 6; //Within 0..2^KUrbanFlattenLog2Magnitude mts we will produce the same score
                                         //to not have two inherently urban tracks unneceserily compete

    function UrbanRuralSliderPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }

    UrbanRuralSliderPreference.prototype.masterSlideOptions = function() {
        var result = {
            label: "urbanrural",
            value: KSliderMax/2,
            min: {
                value: 1,
                label: "more rural"
            },
            max: {
                value: KSliderMax,
                label: "more urban"
            },
            step: 1
        }
        this.mMasterSliderValue = slider2Value(result.value);
        return result;
    }

    function slider2Value(masterSliderValue) {
        return (masterSliderValue-KSliderMax/2)/1.5;
    }


    UrbanRuralSliderPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        this.mMasterSliderValue = slider2Value(masterSliderValue);
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    UrbanRuralSliderPreference.prototype.render = function(panel) {}

    UrbanRuralSliderPreference.prototype.generate = function(rootJson) {
        if (this.mMasterSliderValue) {
            var result       = [];
            for (var i = 0; i <= KMaxLog2Magnitude-KUrbanFlattenLog2Magnitude; i++) {
                //this will produce linearly rising function
                result.push(
                    (1+Math.abs(this.mMasterSliderValue)*1.0*i/KMaxLog2Magnitude).toFixed(2)
                );
            }
            if (this.mMasterSliderValue < 0) {
                //more rural required, so we need to punish short routes and
                //thus need a falling linear function
                result = result.reverse();
            }
            //now prepend for the short distances
            result = new Array(KUrbanFlattenLog2Magnitude)
            .fill(result[0])
            .concat(result);

            rootJson["sectionLengthFactors"] = {
                costFactors : result
            }
        }
    }

    return {
        UrbanRuralSliderPreference: UrbanRuralSliderPreference,
        sliderGenerator : UrbanRuralSliderPreference
    }

})();

module.exports = urbanRural;