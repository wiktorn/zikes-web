/**
 * scripts/ui/map.js
 *
 * This prepares and manages the leaflet map element
 */

'use strict';

var when      = require("when");
when.delay    = require("when/delay");
                require("../external/leaflet-singleclick.js");
var zikesApi  = require("../lib/zikesRouter.js");
var googleAPI = require("../lib/google.js");
var geoip     = require("../lib/geoip.js");
var utils     = require("../lib/utils.js");
var messages  = require("./messages.js");
var ctxMenu   = require("./contextMenu.js");
var ui        = require("./ui.js");

var map = (function() {

    var mapElement = $("#map");
    var mapWrap    = $("#mapWrap")

    var initialCentre = new L.LatLng(49.977722, 14.064453);

    var highwaysById    = undefined;
    var highwaysByName  = undefined;
    var openCycleMap    = L.tileLayer(
        "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png", {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
    });
    var googleSatView = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });
    var mapboxGl = L.mapboxGL({
        style: 'mapbox://styles/mapbox/outdoors-v9',
        accessToken: 'pk.eyJ1IjoicmVtc3RlciIsImEiOiJjaXF6MnlrYXUwMDY3aTVubmxxdWN2M2htIn0.8FBrAn804OlX9QYW-FRVWA'
    });
    var baseMaps = {
        "OpenCycleMap" : openCycleMap,
        "Google Aerial": googleSatView,
        "Mapbox Vector WebGL" : mapboxGl
    };
    var mainMapLayer   = openCycleMap;
    try {
        mainMapLayer = MQ.mapLayer();
        baseMaps["MapBox"] = mainMapLayer;
    } catch (err) {} //we don't have this in the tests

    var map = L.map('map', {
      center: initialCentre,
      zoom: 5,
      layers: [mainMapLayer]
    });

    L.control.layers(baseMaps).addTo(map);
    var connectionMessage = messages.wait(
        "Contacting backend... It sometimes needs waking up. This normally takes less than 20s."
    );
    var geoIpRetrieved    = geoip.get();
    when.delay(1000).then(function() {
        connectionMessage.enqueue();
        if (!highwaysByName) {
            busy(true);
        }
    });
    var loaded = zikesApi.meta().then(function(meta) {
        busy(false);
        connectionMessage.hide();
        var coverageOverlay = [[[89, -179],[89, 179],[-89, 179],[-89, -179]]];
        highwaysByName = meta.map.highways;
        highwaysById = {};
        for (var name in highwaysByName) {
            var id = highwaysByName[name];
            highwaysById[id] = name;
        }
        coverageOverlay = coverageOverlay.concat(meta.map.coverage);
        var polygon = L.polygon(
            coverageOverlay,
            {
                color: "red",
                weight: 0,
                clickable: false
            }
        ).addTo(map);

        if (!ui.welcomeMessage.hidden() && Object.keys(utils.urlParams).length == 0) {
            return when.delay(1000)
            .then(function() {
                ui.welcomeMessage.enqueue();
            })
            .then(function() {
                return meta.map.coverage;
            });
        }
        return meta.map.coverage;
    })
    .otherwise(function() {
        connectionMessage.hide();
        messages.error(
            "We're sorry, but our routing server appears to be down :( There is nothing we can do for you right now.",
            {timeoutSec:0}
        ).enqueue();
        throw "We're doomed!"
    })
    .then(function(coverage) {
        return geoIpRetrieved.then(function(geoipData) {
            if (geoipData.country != "") {
                var currentCentre = map.getCenter();
                if (utils.pips(geoipData.latlng, coverage)) {
                    if (currentCentre.lat == initialCentre.lat && currentCentre.lon == initialCentre.lon) {
                        map.setView(
                            geoipData.latlng,
                            8,
                            { animate : true }
                        );
                        return when.delay(1200);//let the view settle
                    }
                } else {
                    messages.warn(
                        "We're sorry, but it appears, where we think you are ("+geoipData.country+"), we don't offer coverage :( We only have a small server."
                    ).enqueue();
                }
            }
        })
    });

    map.on('singleclick', function(e) {
        if (!mapElement.hasClass("busy")) {
            ctxMenu(e.latlng).default().show();
        }
    });
    map.on('dblclick', function(e) {
        map.zoomIn();
    });

    function expand() {
        if (mapWrap.hasClass("shrunk")) {
            var done = when.defer();
            mapWrap.removeClass('shrunk').animate({'margin-left':'0px'}, function() {
                map.invalidateSize();
                done.resolve();
            });
            return done.promise;
        }
        return when(true);
    }

    function shrink() {
        if (!mapWrap.hasClass("shrunk")) {
            var done = when.defer();
            mapWrap.addClass('shrunk').animate({'margin-left':'300px'}, function() {
                map.invalidateSize();
                done.resolve();
            });
            return done.promise;
        }
        return when(true);
    }

    function isShrunk() {
        return mapWrap.hasClass("shrunk");
    }

    function busy(busy) {
        if (busy) {
            mapElement.css("cursor", "wait");
        } else {
            mapElement.css("cursor", "");
        }
    }

    function initGeoSearch() {
        var searchBox = $("#geo-search-box");
        searchBox.blur(function() {
            searchBox.val('');
        });
        searchBox.focus(function() {
            searchBox.val('');
        });
        googleAPI.geocodeFindUI(searchBox[0], function(result) {
            if (result.geometry) {
                var location = new L.LatLng(
                    result.geometry.location.lat(), result.geometry.location.lng()
                );

                if (result.geometry.viewport) {
                    var bounds = new L.latLngBounds([]);
                    bounds.extend(new L.LatLng(
                        result.geometry.viewport.getNorthEast().lat(), result.geometry.viewport.getNorthEast().lng())
                    );
                    bounds.extend(new L.LatLng(
                        result.geometry.viewport.getSouthWest().lat(), result.geometry.viewport.getSouthWest().lng())
                    );
                    map.fitBounds(bounds, {animate:true});
                } else {
                    map.setView(location, 17, { animate: true });
                }
                var title = result.formatted_address;
                if (title.indexOf(result.name) === -1) {
                    title = result.name + ", " + title;
                }
                title = title.split(", ");
                title[0] = "<b>"+title[0]+"</b>";
                title = title.join(", ");
                ctxMenu(location).title(title).default().show();
            }
        });
    }
    initGeoSearch();

    return {
        expand          : expand,
        shrink          : shrink,
        isShrunk        : isShrunk,
        leaflet         : map,
        elem            : mapElement,
        busy            : busy,
        loaded          : loaded,
        rect            : function() { return mapElement[0].getBoundingClientRect();},
        highwaysByName  : function() { return highwaysByName; },
        highwaysById    : function() { return highwaysById; }
    };

})();

module.exports   = map;